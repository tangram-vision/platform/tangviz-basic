# Tangram Vision Basic

Tangram Vision Basic represents the core types and functions that make Tangram software
tick.

## Features

- `proptest-support`: Enables `arbitrary` modules within the crate. These are used for writing
property-based tests with the `proptest` crate.

## Modules

### Base

Base holds basic types that are useful across Tangram Software. This includes different
component model descriptions, mathematical necessities, etc.

### Plex

Plexes are a fundamental representation of a multi-sensor system. They describe the
modeling, registration, and synchronization relationships between unit components in a
multi-sensor system.
