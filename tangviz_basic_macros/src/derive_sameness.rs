// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! Provides a derive macro for deriving the `Sameness` trait.

use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, DeriveInput};

/// Derive macro for the Sameness trait.
///
/// This allows one to derive Sameness based on whether the members of a struct or enum are "the
/// same" according to the Sameness trait (in lexicographic order).
///
/// What this means is that for every struct or enum this is derived on, one can assume that every
/// member of that struct or enum must also return `true` when calling `a.is_same(b)`.
///
/// # Errors
///
/// This derive macro will fail on Union types. It is not possible to safely compare unions for
/// "sameness" unless they also have a union field that is `&[u8]` or some equivalent. Since we
/// cannot guarantee that such a field exists, these must be implemented manually.
pub fn derive(input: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(input as DeriveInput);
    let name = &ast.ident;

    let (impl_generics, ty_generics, where_clause) = ast.generics.split_for_impl();

    let gen = match &ast.data {
        syn::Data::Struct(s) => {
            let impl_block = match &s.fields {
                syn::Fields::Named(fields) => {
                    let token_iter = fields.named.iter().map(|f| {
                        let ident = f.ident.as_ref().unwrap();
                        quote! { self.#ident.is_same(&rhs.#ident) }
                    });

                    quote! { #(#token_iter)&&* }
                }

                syn::Fields::Unnamed(fields) => {
                    let token_iter = fields.unnamed.iter().enumerate().map(|(i, _)| {
                        quote! { self.#i.is_same(rhs.#i) }
                    });

                    quote! { #(#token_iter)&&* }
                }

                syn::Fields::Unit => {
                    quote! { true }
                }
            };

            quote! {
                impl #impl_generics Sameness for #name #ty_generics #where_clause {
                    fn is_same(&self, rhs: &Self) -> bool {
                        #impl_block
                    }
                }
            }
        }

        syn::Data::Enum(e) => {
            let variant_iter = e.variants.iter().map(|v| {
                let ident = &v.ident;

                match &v.fields {
                    syn::Fields::Named(fields) => {
                        let left_tokens = fields.named.iter().enumerate().map(|(i, f)| {
                            let ident = &f.ident;
                            let tokenized_ident = syn::Ident::new(
                                &format!("__l_{}", i),
                                proc_macro2::Span::call_site(),
                            );

                            (ident, tokenized_ident)
                        });

                        let right_tokens = fields.named.iter().enumerate().map(|(i, f)| {
                            let ident = &f.ident;
                            let tokenized_ident = syn::Ident::new(
                                &format!("__r_{}", i),
                                proc_macro2::Span::call_site(),
                            );

                            (ident, tokenized_ident)
                        });

                        let token_iter = left_tokens
                            .clone()
                            .zip(right_tokens.clone())
                            .map(|((_, left), (_, right))| {
                                quote! { #left.is_same(&#right) }
                            });

                        let left_tokens = left_tokens.map(|(ident, tokenized)| {
                            quote! { #ident: #tokenized }
                        });

                        let right_tokens = right_tokens.map(|(ident, tokenized)| {
                            quote! { #ident: #tokenized }
                        });

                        quote! {
                            (#name::#ident { #(#left_tokens),* }, #name::#ident { #(#right_tokens),* }) => {
                                #(#token_iter)&&*
                            }
                        }
                    }

                    syn::Fields::Unnamed(fields) => {
                        let left_tokens = fields.unnamed.iter().enumerate().map(|(i, _)| {
                            syn::Ident::new(
                                &format!("__l{}", i),
                                proc_macro2::Span::call_site(),
                            )
                        });

                        let right_tokens = fields.unnamed.iter().enumerate().map(|(i, _)| {
                            syn::Ident::new(
                                &format!("__r{}", i),
                                proc_macro2::Span::call_site(),
                            )
                        });

                        let token_iter = left_tokens
                            .clone()
                            .zip(right_tokens.clone())
                            .map(|(left, right)| {
                                quote! { #left.is_same(&#right) }
                            });

                        quote! {
                            (#name::#ident(#(#left_tokens),*), #name::#ident(#(#right_tokens),*)) => {
                                #(#token_iter)&&*
                            }
                        }
                    }

                    syn::Fields::Unit => {
                        quote! { (#name::#ident, #name::#ident ) => true, }
                    }
                }
            });

            quote! {
                impl #impl_generics Sameness for #name #ty_generics #where_clause {
                    fn is_same(&self, rhs: &Self) -> bool {
                        match (self, rhs) {
                            #(#variant_iter)*
                            _ => false,
                        }
                    }
                }
            }
        }

        syn::Data::Union(_) => {
            panic!("There is no safe way to automatically derive `Sameness` for a union.");
        }
    };

    gen.into()
}
