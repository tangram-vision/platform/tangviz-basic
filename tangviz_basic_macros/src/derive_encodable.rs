// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! Provides a derive macro for deriving the `Encodable` trait.

use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, DeriveInput, Field};

/// Checks if a field has the `#[no_encode]` attribute.
///
/// NOTE: you may see this predicate used in all parts of the derive macro. In effect, we are
/// asking if a fields should have been encoded, so that we can decide whether to clone it, skip
/// it, or otherwise when calculating encoded sizes and such.
fn should_encode(field: &Field) -> bool {
    !field.attrs.iter().any(|attr| {
        attr.path
            .get_ident()
            .map_or_else(|| false, |ident| ident.to_string() == "no_encode")
    })
}

/// Derive macro for the Encodable trait.
///
/// This derives the `Encodable` trait automatically for a given type, as well as produce a custom
/// decoder type (private) that will be used when calling `T::decoder()`, output as a `Box<dyn
/// Decoder<T>>`.
///
/// # Attributes
///
/// The `#[no_encode]` attribute is defined for this derive macro. If this attribute decorates a
/// field, then that field is not encoded into the final vector of parameters and is instead cloned
/// straight into its respective decoder.
///
/// # Errors
///
/// This derive macro will fail on Union types. It is not possible to safely compare unions for
/// "sameness" unless they also have a union field that is `&[u8]` or some equivalent. Since we
/// cannot guarantee that such a field exists, these must be implemented manually.
pub fn derive(input: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(input as DeriveInput);
    let name = &ast.ident;

    let (impl_generics, ty_generics, where_clause) = ast.generics.split_for_impl();

    let gen = match &ast.data {
        syn::Data::Struct(s) => {
            let decoder_name = syn::Ident::new(
                &format!("__{}StructDecoder", quote! { #name }),
                proc_macro2::Span::mixed_site(),
            );

            match &s.fields {
                syn::Fields::Named(fields) => {
                    let identifiers_iter = fields.named.iter().map(|f| {
                        let ident = f.ident.as_ref().unwrap();
                        quote! { #ident }
                    });

                    let encoded_size_token_iter = fields.named.iter().filter_map(|f| {
                        if should_encode(f) {
                            let ident = f.ident.as_ref().unwrap();
                            Some(quote! { self.#ident.encoded_size() })
                        } else {
                            None
                        }
                    });

                    let encode_block_token_iter = fields.named.iter().filter_map(|f| {
                        if should_encode(f) {
                            let ident = f.ident.as_ref().unwrap();
                            Some(quote! {
                                let mut v = self.#ident.encode();
                                encoded.append(&mut v);
                            })
                        } else {
                            None
                        }
                    });

                    let encode_decoder_token_iter = fields.named.iter().map(|f| {
                        let ident = f.ident.as_ref().unwrap();
                        let field_type = &f.ty;

                        if should_encode(f) {
                            quote! { #ident: Box<dyn Decoder<#field_type>>, }
                        } else {
                            quote! { #ident: #field_type, }
                        }
                    });

                    let encode_decoder_impl_token_iter = fields.named.iter().map(|f| {
                        let ident = f.ident.as_ref().unwrap();

                        if should_encode(f) {
                            quote! { #ident: self.#ident.decoder(), }
                        } else {
                            quote! { #ident: self.#ident.clone(), }
                        }
                    });

                    let decode_size_token_iter = fields.named.iter().filter_map(|f| {
                        let ident = f.ident.as_ref().unwrap();

                        if should_encode(f) {
                            Some(quote! { self.#ident.size() })
                        } else {
                            None
                        }
                    });

                    let decode_block_token_iter = fields.named.iter().map(|f| {
                        let ident = f.ident.as_ref().unwrap();

                        if should_encode(f) {
                            quote! {
                                let sz = self.#ident.size();
                                let #ident = self.#ident.decode(
                                    &parameters[0..sz],
                                ).unwrap();
                                let parameters = &parameters[sz..];
                            }
                        } else {
                            quote! {
                                let #ident = self.#ident.clone();
                            }
                        }
                    });

                    quote! {
                        impl #impl_generics Encodable for #name #ty_generics #where_clause {
                            fn encoded_size(&self) -> usize {
                                #(#encoded_size_token_iter)+*
                            }

                            fn decoder(&self) -> Box<dyn Decoder<#name>> {
                                Box::new(#decoder_name {
                                    #(#encode_decoder_impl_token_iter)*
                                })
                            }

                            fn encode(&self) -> Vec<f64> {
                                let mut encoded = Vec::with_capacity(self.encoded_size());
                                #(#encode_block_token_iter)*
                                encoded
                            }
                        }

                        #[doc(hidden)]
                        #[allow(non_upper_case_globals, unused_attributes, unused_qualifications)]
                        struct #decoder_name {
                            #(#encode_decoder_token_iter)*
                        }

                        impl Decoder<#name> for #decoder_name {
                            fn size(&self) -> usize {
                                #(#decode_size_token_iter)+*
                            }

                            fn decode(&self, parameters: &[f64]) -> Option<#name> {
                                if parameters.len() == self.size() {
                                    #(#decode_block_token_iter)*
                                    Some(#name { #(#identifiers_iter),* })
                                } else {
                                    None
                                }
                            }
                        }
                    }
                }

                syn::Fields::Unnamed(fields) => {
                    let encoded_size_token_iter =
                        fields.unnamed.iter().enumerate().filter_map(|(i, f)| {
                            if should_encode(f) {
                                let idx = syn::Index::from(i);
                                Some(quote! { self.#idx.encoded_size() })
                            } else {
                                None
                            }
                        });

                    let encode_block_token_iter =
                        fields.unnamed.iter().enumerate().filter_map(|(i, f)| {
                            if should_encode(f) {
                                let idx = syn::Index::from(i);
                                Some(quote! {
                                    let mut v = self.#idx.encode();
                                    encoded.append(&mut v);
                                })
                            } else {
                                None
                            }
                        });

                    let encode_decoder_token_iter = fields.unnamed.iter().map(|f| {
                        let field_type = &f.ty;
                        if should_encode(f) {
                            quote! { Box<dyn Decoder<#field_type>> }
                        } else {
                            quote! { #field_type }
                        }
                    });

                    let encode_decoder_impl_token_iter =
                        fields.unnamed.iter().enumerate().map(|(i, f)| {
                            let idx = syn::Index::from(i);
                            if should_encode(f) {
                                quote! { self.#idx.decoder() }
                            } else {
                                quote! { self.#idx.clone() }
                            }
                        });

                    let decode_size_token_iter =
                        fields.unnamed.iter().enumerate().filter_map(|(i, f)| {
                            if should_encode(f) {
                                let idx = syn::Index::from(i);
                                Some(quote! { self.#idx.size() })
                            } else {
                                None
                            }
                        });

                    // Unlike in the named case, we need to create some local identifier for each
                    // decoded result. These are just called `__fN` for every one of N fields.
                    let tokens = fields.unnamed.iter().enumerate().map(|(i, _)| {
                        let idx = syn::Index::from(i);
                        syn::Ident::new(
                            &format!("__f{}", quote! { #idx }),
                            proc_macro2::Span::call_site(),
                        )
                    });

                    let decode_block_token_iter =
                        fields.unnamed.iter().enumerate().zip(tokens.clone()).map(
                            |((i, f), token)| {
                                let idx = syn::Index::from(i);

                                if should_encode(f) {
                                    quote! {
                                        let sz = self.#idx.size();
                                        let #token = self.#idx.decode(
                                            &parameters[0..sz],
                                        ).unwrap();
                                        let parameters = &parameters[sz..];
                                    }
                                } else {
                                    quote! {
                                        let #token = self.#idx.clone();
                                    }
                                }
                            },
                        );

                    quote! {
                        impl #impl_generics Encodable for #name #ty_generics #where_clause {
                            fn encoded_size(&self) -> usize {
                                #(#encoded_size_token_iter)+*
                            }

                            fn decoder(&self) -> Box<dyn Decoder<#name>> {
                                Box::new(#decoder_name ( #(#encode_decoder_impl_token_iter),* ))
                            }

                            fn encode(&self) -> Vec<f64> {
                                let mut encoded = Vec::with_capacity(self.encoded_size());
                                #(#encode_block_token_iter)*
                                encoded
                            }
                        }

                        #[doc(hidden)]
                        #[allow(non_upper_case_globals, unused_attributes, unused_qualifications)]
                        struct #decoder_name (#(#encode_decoder_token_iter),*);

                        impl Decoder<#name> for #decoder_name {
                            fn size(&self) -> usize {
                                #(#decode_size_token_iter)+*
                            }

                            fn decode(&self, parameters: &[f64]) -> Option<#name> {
                                if parameters.len() == self.size() {
                                    #(#decode_block_token_iter)*
                                    Some(#name ( #(#tokens),* ))
                                } else {
                                    None
                                }
                            }
                        }
                    }
                }

                syn::Fields::Unit => {
                    // Admittedly this is kind of strange - I'm not sure why one would encode a
                    // unit struct at all (this makes more sense in an enum) but is easy enough to
                    // implement that there's no reason we should prevent it / cause a compile
                    // error because of it.

                    quote! {
                        impl #impl_generics Encodable for #name #ty_generics #where_clause {
                            fn encoded_size(&self) -> usize {
                                0
                            }

                            fn decoder(&self) -> Box<dyn Decoder<#name>> {
                                Box::new(#decoder_name)
                            }

                            fn encode(&self) -> Vec<f64> {
                                Vec::new()
                            }
                        }

                        #[doc(hidden)]
                        #[allow(non_upper_case_globals, unused_attributes, unused_qualifications)]
                        struct #decoder_name;

                        impl Decoder<#name> for #decoder_name {
                            fn size(&self) -> usize {
                                0
                            }

                            fn decode(&self, parameters: &[f64]) -> Option<#name> {
                                if parameters.len() == self.size() {
                                    Some(#name)
                                } else {
                                    None
                                }
                            }
                        }
                    }
                }
            }
        }

        syn::Data::Enum(e) => {
            // Unlike in the struct case, we can't have a single "decoder name" - we need a decoder
            // name for each of the variants.
            //
            // Fortunately this just means we push it one layer deeper, and generate a custom
            // decoder type (and encoder body) for each variant kind.

            let encoded_size_variant_iter = e.variants.iter().map(|v| {
                let variant_ident = &v.ident;

                match &v.fields {
                    syn::Fields::Named(fields) => {
                        let field_ident_iter = fields.named.iter().map(|f| {
                            let ident = f.ident.as_ref().unwrap();
                            quote! { #ident }
                        });

                        let field_encoded_size_iter = fields.named.iter().filter_map(|f| {
                            if should_encode(f) {
                                let ident = f.ident.as_ref().unwrap();
                                Some(quote! { #ident.encoded_size() })
                            } else {
                                None
                            }
                        });

                        quote! {
                            #name::#variant_ident{ #(#field_ident_iter),* } => {
                                #(#field_encoded_size_iter)+*
                            }
                        }
                    }

                    syn::Fields::Unnamed(fields) => {
                        // Unlike in the named case, we need to create some local identifier for each
                        // decoded result. These are just called `__vfN` for every one of N variant
                        // fields.
                        let field_ident_iter = fields.unnamed.iter().enumerate().map(|(i, _)| {
                            let idx = syn::Index::from(i);
                            let tokenized_ident = syn::Ident::new(
                                &format!("__vf{}", quote! { #idx }),
                                proc_macro2::Span::call_site(),
                            );
                            tokenized_ident
                        });

                        let field_encoded_size_iter = fields
                            .unnamed
                            .iter()
                            .zip(field_ident_iter.clone())
                            .filter_map(|(f, token)| {
                                if should_encode(f) {
                                    Some(quote! { #token.encoded_size() })
                                } else {
                                    None
                                }
                            });

                        quote! {
                            #name::#variant_ident( #(#field_ident_iter),* ) => {
                                #(#field_encoded_size_iter)+*
                            }
                        }
                    }

                    syn::Fields::Unit => {
                        quote! {
                            #name::#variant_ident => 0,
                        }
                    }
                }
            });

            let encode_block_iter = e.variants.iter().map(|v| {
                let variant_ident = &v.ident;

                match &v.fields {
                    syn::Fields::Named(fields) => {
                        let field_ident_iter = fields.named.iter().map(|f| {
                            let ident = f.ident.as_ref().unwrap();
                            quote! { #ident }
                        });

                        let field_encode_iter = fields.named.iter().filter_map(|f| {
                            if should_encode(f) {
                                let ident = f.ident.as_ref().unwrap();
                                Some(quote! {
                                    let mut v = #ident.encode();
                                    encoded.append(&mut v);
                                })
                            } else {
                                None
                            }
                        });

                        quote! {
                            #name::#variant_ident{ #(#field_ident_iter),* } => {
                                let mut encoded = Vec::with_capacity(self.encoded_size());
                                #(#field_encode_iter)*
                                encoded
                            }
                        }
                    }

                    syn::Fields::Unnamed(fields) => {
                        // Unlike in the named case, we need to create some local identifier for each
                        // decoded result. These are just called `__vfN` for every one of N variant
                        // fields.
                        let field_ident_iter = fields.unnamed.iter().enumerate().map(|(i, _)| {
                            let idx = syn::Index::from(i);
                            let tokenized_ident = syn::Ident::new(
                                &format!("__vf{}", quote! { #idx }),
                                proc_macro2::Span::call_site(),
                            );
                            tokenized_ident
                        });

                        let field_encode_iter = fields
                            .unnamed
                            .iter()
                            .zip(field_ident_iter.clone())
                            .filter_map(|(f, token)| {
                                if should_encode(f) {
                                    Some(quote! {
                                        let mut v = #token.encode();
                                        encoded.append(&mut v);
                                    })
                                } else {
                                    None
                                }
                            });

                        quote! {
                            #name::#variant_ident( #(#field_ident_iter),* ) => {
                                let mut encoded = Vec::with_capacity(self.encoded_size());
                                #(#field_encode_iter)*
                                encoded
                            }
                        }
                    }

                    syn::Fields::Unit => {
                        quote! {
                            #name::#variant_ident => Vec::new(),
                        }
                    }
                }
            });

            let decoder_block_iter = e.variants.iter().map(|v| {
                let variant_ident = &v.ident;
                let decoder_name = syn::Ident::new(
                    &format!(
                        "__{}{}EnumVariantDecoder",
                        quote! { #name },
                        quote! { #variant_ident },
                    ),
                    proc_macro2::Span::mixed_site(),
                );

                match &v.fields {
                    syn::Fields::Named(fields) => {
                        let field_ident_iter = fields.named.iter().map(|f| {
                            let ident = f.ident.as_ref().unwrap();
                            quote! { #ident }
                        });

                        let field_decode_iter = fields.named.iter().map(|f| {
                            let ident = f.ident.as_ref().unwrap();
                            if should_encode(f) {
                                quote! {
                                    #ident: #ident.decoder(),
                                }
                            } else {
                                quote! {
                                    #ident: #ident.clone(),
                                }
                            }
                        });

                        quote! {
                            #name::#variant_ident{ #(#field_ident_iter),* } => {
                                Box::new(#decoder_name {
                                    #(#field_decode_iter)*
                                })
                            }
                        }
                    }

                    syn::Fields::Unnamed(fields) => {
                        // Unlike in the named case, we need to create some local identifier for each
                        // decoded result. These are just called `__vfN` for every one of N variant
                        // fields.
                        let field_ident_iter = fields.unnamed.iter().enumerate().map(|(i, _)| {
                            let idx = syn::Index::from(i);
                            let tokenized_ident = syn::Ident::new(
                                &format!("__vf{}", quote! { #idx }),
                                proc_macro2::Span::call_site(),
                            );
                            tokenized_ident
                        });

                        let field_decode_iter =
                            fields.unnamed.iter().zip(field_ident_iter.clone()).map(
                                |(f, token)| {
                                    if should_encode(f) {
                                        quote! { #token.decoder() }
                                    } else {
                                        quote! { #token.clone() }
                                    }
                                },
                            );

                        quote! {
                            #name::#variant_ident( #(#field_ident_iter),* ) => {
                                Box::new(
                                    #decoder_name( #(#field_decode_iter),* ),
                                )
                            }
                        }
                    }

                    syn::Fields::Unit => {
                        quote! {
                            #name::#variant_ident => Box::new(#decoder_name),
                        }
                    }
                }
            });

            let decoder_variant_impls = e.variants.iter().map(|v| {
                let variant_ident = &v.ident;
                let decoder_name = syn::Ident::new(
                    &format!(
                        "__{}{}EnumVariantDecoder",
                        quote! { #name },
                        quote! { #variant_ident },
                    ),
                    proc_macro2::Span::mixed_site(),
                );

                match &v.fields {
                    syn::Fields::Named(fields) => {
                        let decoder_fields_iter = fields.named.iter().map(|f| {
                            let ident = f.ident.as_ref().unwrap();
                            let field_type = &f.ty;

                            if should_encode(f) {
                                quote! { #ident: Box<dyn Decoder<#field_type>> }
                            } else {
                                quote! { #ident: #field_type }
                            }
                        });

                        let decoder_size_iter = fields.named.iter().filter_map(|f| {
                            if should_encode(f) {
                                let ident = f.ident.as_ref().unwrap();
                                Some(quote! { self.#ident.size() })
                            } else {
                                None
                            }
                        });

                        let decoder_block_impl_iter = fields.named.iter().map(|f| {
                            let ident = f.ident.as_ref().unwrap();

                            if should_encode(f) {
                                quote! {
                                    let sz = self.#ident.size();
                                    let #ident = self.#ident.decode(
                                        &parameters[0..sz],
                                    ).unwrap();
                                    let parameters = &parameters[sz..];
                                }
                            } else {
                                quote! {
                                    let #ident = self.#ident.clone();
                                }
                            }
                        });

                        let identifier_iter = fields.named.iter().map(|f| {
                            let ident = f.ident.as_ref().unwrap();
                            quote! { #ident }
                        });

                        quote! {
                            #[doc(hidden)]
                            #[allow(non_upper_case_globals, unused_attributes, unused_qualifications)]
                            struct #decoder_name {
                                #(#decoder_fields_iter),*
                            }

                            impl Decoder<#name> for #decoder_name {
                                fn size(&self) -> usize {
                                    #(#decoder_size_iter)+*
                                }

                                fn decode(&self, parameters: &[f64]) -> Option<#name> {
                                    if parameters.len() == self.size() {
                                        #(#decoder_block_impl_iter)*
                                        Some(#name::#variant_ident {
                                            #(#identifier_iter),*
                                        })
                                    } else {
                                        None
                                    }
                                }
                            }
                        }
                    }

                    syn::Fields::Unnamed(fields) => {
                        let field_ident_iter = fields.unnamed.iter().enumerate().map(|(i, _)| {
                            let idx = syn::Index::from(i);
                            let tokenized_ident = syn::Ident::new(
                                &format!("__vf{}", quote! { #idx }),
                                proc_macro2::Span::call_site(),
                            );
                            tokenized_ident
                        });

                        let decoder_types_iter = fields.unnamed.iter().map(|f| {
                            let field_type = &f.ty;

                            if should_encode(f) {
                                quote! { Box<dyn Decoder<#field_type>> }
                            } else {
                                quote! { #field_type }
                            }
                        });

                        let decoder_size_iter =
                            fields.unnamed.iter().enumerate().filter_map(|(i, f)| {
                                if should_encode(f) {
                                    let idx = syn::Index::from(i);
                                    Some(quote! { self.#idx.size() })
                                } else {
                                    None
                                }
                            });

                        let decoder_block_impl_iter = fields
                            .unnamed
                            .iter()
                            .enumerate()
                            .zip(field_ident_iter.clone())
                            .map(|((i, f), token)| {
                                let idx = syn::Index::from(i);
                                if should_encode(f) {
                                    quote! {
                                        let sz = self.#idx.size();
                                        let #token = self.#idx.decode(
                                            &parameters[0..sz],
                                        ).unwrap();
                                        let parameters = &parameters[sz..];
                                    }
                                } else {
                                    quote! { let #token = self.#idx.clone(); }
                                }
                            });

                        quote! {
                            #[doc(hidden)]
                            #[allow(non_upper_case_globals, unused_attributes, unused_qualifications)]
                            struct #decoder_name (#(#decoder_types_iter),*);

                            impl Decoder<#name> for #decoder_name {
                                fn size(&self) -> usize {
                                    #(#decoder_size_iter)+*
                                }

                                fn decode(&self, parameters: &[f64]) -> Option<#name> {
                                    if parameters.len() == self.size() {
                                        #(#decoder_block_impl_iter)*
                                        Some(#name::#variant_ident (#(#field_ident_iter),* ))
                                    } else {
                                        None
                                    }
                                }
                            }
                        }
                    }

                    syn::Fields::Unit => {
                        quote! {
                            #[doc(hidden)]
                            #[allow(non_upper_case_globals, unused_attributes, unused_qualifications)]
                            struct #decoder_name;

                            impl Decoder<#name> for #decoder_name {
                                fn size(&self) -> usize {
                                    0
                                }

                                fn decode(&self, parameters: &[f64]) -> Option<#name> {
                                    if parameters.len() == self.size() {
                                        Some(#name::#variant_ident)
                                    } else {
                                        None
                                    }
                                }
                            }
                        }
                    }
                }
            });

            quote! {
                impl #impl_generics Encodable for #name #ty_generics #where_clause {
                    fn encoded_size(&self) -> usize {
                        match self {
                            #(#encoded_size_variant_iter)*
                        }
                    }

                    fn decoder(&self) -> Box<dyn Decoder<#name>> {
                        match self {
                            #(#decoder_block_iter)*
                        }
                    }

                    fn encode(&self) -> Vec<f64> {
                        match self {
                            #(#encode_block_iter)*
                        }
                    }
                }

                #(#decoder_variant_impls)*
            }
        }

        syn::Data::Union(_) => {
            panic!("There is no safe way to automatically derive `Encodable` for a union.");
        }
    };

    gen.into()
}
