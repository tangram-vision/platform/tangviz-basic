// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! Provides a derive macro for deriving the `PlexComponent` trait.

use proc_macro::TokenStream;
use quote::quote;

/// Derive macro for creating plex components.
///
/// This attribute is used to implement the `PlexComponent` trait. The `PlexComponent` trait, which
/// is in the default crate (this crate is specifically for procedural macros), specifies the
/// interfaces that every component type must implement.
pub fn derive(input: TokenStream) -> TokenStream {
    // These ty_ variables are used when checking if the expected type signature for member
    // variables are correct.
    //
    // Unfortunately they can't be const, but this is the easiest way to be able to do a type-check
    // comparison on field tokens in the struct.
    let ty_qualified_uuid = syn::parse::<syn::Type>(TokenStream::from(quote!(uuid::Uuid))).unwrap();
    let ty_uuid = syn::parse::<syn::Type>(TokenStream::from(quote!(Uuid))).unwrap();
    let ty_qualified_string =
        syn::parse::<syn::Type>(TokenStream::from(quote!(std::string::String))).unwrap();
    let ty_string = syn::parse::<syn::Type>(TokenStream::from(quote!(String))).unwrap();

    let ast = syn::parse::<syn::DeriveInput>(input).unwrap();
    let name = &ast.ident;

    let fields = match &ast.data {
        syn::Data::Struct(data) => &data.fields,
        syn::Data::Enum(_) | syn::Data::Union(_) => {
            panic!("#[derive(PlexComponent)] is only valid for structs with named fields.",);
        }
    };

    match &fields {
        syn::Fields::Named(named_fields) => {
            let mut has_uuid = false;
            let mut has_root_uuid = false;
            let mut has_name = false;

            for field in &named_fields.named {
                let identifier = field.ident.as_ref().unwrap().to_string();

                match identifier.as_str() {
                    "uuid" => {
                        if field.ty != ty_uuid && field.ty != ty_qualified_uuid {
                            panic!("`uuid` member in {} must be of type `uuid::Uuid`.", name);
                        }

                        has_uuid = true;
                    }
                    "root_uuid" => {
                        if field.ty != ty_uuid && field.ty != ty_qualified_uuid {
                            panic!(
                                "`root_uuid` member in {} must be of type `uuid::Uuid`.",
                                name
                            );
                        }
                        has_root_uuid = true;
                    }
                    "name" => {
                        if field.ty != ty_string && field.ty != ty_qualified_string {
                            panic!("`name` member in {} must be of type `String`.", name);
                        }
                        has_name = true;
                    }
                    _ => (),
                }
            }

            if !has_uuid {
                panic!("PlexComponents require a struct member (type Uuid) with identifier `uuid`");
            }

            if !has_root_uuid {
                panic!("PlexComponents require a struct member (type Uuid) with identifier `root_uuid`");
            }

            if !has_name {
                panic!(
                    "PlexComponents require a struct member (type String) with identifier `name`"
                );
            }
        }

        syn::Fields::Unit => {
            panic!(
                "#[derive(PlexComponent)] is only valid for structs with named fields. Found: Unit struct",
            );
        }

        syn::Fields::Unnamed(_) => {
            panic!(
                "#[derive(PlexComponent)] is only valid for structs with named fields. Found: Tuple-like struct",
            );
        }
    };

    let (impl_generics, ty_generics, where_clause) = ast.generics.split_for_impl();

    let gen = quote! {
        impl #impl_generics PlexComponent for #name #ty_generics #where_clause {
            fn uuid(&self) -> &uuid::Uuid {
                &self.uuid
            }

            fn root_uuid(&self) -> &uuid::Uuid {
                &self.root_uuid
            }

            fn name(&self) -> &str {
                &self.name
            }
        }
    };

    gen.into()
}
