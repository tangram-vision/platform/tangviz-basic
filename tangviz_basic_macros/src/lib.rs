// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! This crate defines a set of procedural macros used in the default crate.

use proc_macro::TokenStream;

mod derive_encodable;
mod derive_plex_component;
mod derive_sameness;

/// Derive macro for `PlexComponent` trait.
///
/// This attribute is used to implement the `PlexComponent` trait. The `PlexComponent` trait, which
/// is in the default crate (this crate is specifically for procedural macros), specifies the
/// interfaces that every component type must implement.
#[proc_macro_derive(PlexComponent)]
pub fn derive_plex_component(input: TokenStream) -> TokenStream {
    derive_plex_component::derive(input)
}

/// Derive macro for `Sameness` trait.
///
/// This allows one to derive Sameness based on whether the members of a struct or enum are "the
/// same" according to the Sameness trait (in lexicographic order).
///
/// What this means is that for every struct or enum this is derived on, one can assume that every
/// member of that struct or enum must also return `true` when calling `a.is_same(b)`.
///
/// # Errors
///
/// This derive macro will fail on Union types. It is not possible to safely compare unions for
/// "sameness" unless they also have a union field that is `&[u8]` or some equivalent. Since we
/// cannot guarantee that such a field exists, these must be implemented manually.
#[proc_macro_derive(Sameness)]
pub fn derive_sameness(input: TokenStream) -> TokenStream {
    derive_sameness::derive(input)
}

/// Derive macro for `Encodable` trait.
///
/// This derives the `Encodable` trait automatically for a given type, as well as produce a custom
/// decoder type (private) that will be used when calling `T::decoder()`, output as a `Box<dyn
/// Decoder<T>>`.
///
/// # Attributes
///
/// The `#[no_encode]` attribute is defined for this derive macro. If this attribute decorates a
/// field, then that field is not encoded into the final vector of parameters and is instead cloned
/// straight into its respective decoder.
///
/// # Errors
///
/// This derive macro will fail on Union types. It is not possible to safely compare unions for
/// "sameness" unless they also have a union field that is `&[u8]` or some equivalent. Since we
/// cannot guarantee that such a field exists, these must be implemented manually.

#[proc_macro_derive(Encodable, attributes(no_encode))]
pub fn derive_encodable(input: TokenStream) -> TokenStream {
    derive_encodable::derive(input)
}
