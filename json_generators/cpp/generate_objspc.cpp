// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
// Sample program demonstrating how to generate your own object space file in
// C++
//
// NOTE: If you have many targets in a plane, you can use an image of said
// targets to generate the object space using
// `examples/gen_objspace_markers.rs` For now, we'll just make a few by hand.

#include "objectspace.h"
#include <fstream>
#include <nlohmann/json.hpp>

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//    Remove None Values
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Thanks to:
// https://github.com/nlohmann/json/issues/995
static void recurseAndFilterNullValues(nlohmann::json &jsonObject) {
  if (!jsonObject.is_object() && !jsonObject.is_array()) {
    return;
  }
  std::vector<nlohmann::json::object_t::key_type> keysToRemove;
  for (auto &it : jsonObject.items()) {
    if (it.value().is_null()) {
      keysToRemove.push_back(it.key());
      continue;
    }
    recurseAndFilterNullValues(it.value());
  }
  for (auto key : keysToRemove) {
    jsonObject.erase(key);
  }
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//    Checkerboard/Markerboard/AprilGrid
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// The code below constructs object space based off a checkerboard.
// Note that Markerboards and AprilGrids follow the same Detector-Descriptor
// convention. No Descriptor is needed for these formats.
void create_checkerboard_objspc() {
  // In meters
  double checker_length = 0.05;
  // the number of _inner_ corners on the checkerboard
  int64_t width = 5;
  int64_t height = 9;
  // In meters^2
  // Order is [x: left-right, y: up-down, z: forward-back]
  std::vector<double> variances{1e-6, 1e-6, 1e-6};

  tangviz::Checkerboard checkerboard{checker_length, height, variances, width};
  tangviz::Objectspace objspc_checkerboard{tangviz::Camera{
      nullptr, // No descriptor needed
      tangviz::Detector{
          nullptr, std::make_shared<tangviz::Checkerboard>(checkerboard)}}};

  nlohmann::json j;
  nlohmann::to_json(j, objspc_checkerboard);
  // Important; validation will fail if null values are present
  recurseAndFilterNullValues(j);

  // Generate the object space file into our build folder
  std::ofstream checkerboard_file;
  checkerboard_file.open("checkerboardcpp.json");
  checkerboard_file << j.dump();
  checkerboard_file.close();
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//    Markers
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// 0-----1  4-----5
// | id0 |  | id1 |  with a distance of 0.05m between them in X
// |     |  |     |
// 3-----2  7-----6

void create_markers_objspc() {
  // Example: using 5x5 ArUco tags, each 0.025m a side
  tangviz::MarkerboardMarkerDictionary marker_dict =
      tangviz::MarkerboardMarkerDictionary::ARUCO5_X5_250;
  double marker_length = 0.025;
  tangviz::Markers markers{marker_dict, marker_length};

  tangviz::Detector detector{nullptr, // not an aprilgrid...
                             nullptr, // not a checkerboard...
                             nullptr, // not a markerboard...
                             std::make_shared<tangviz::Markers>(markers)};

  // Markers take targets as their descriptor. This matches the IDs of the
  // markers with the object space. The Platform uses corners as features, so
  // there are 4 coordinates per target.

  std::vector<tangviz::Target> targets;

  // In meters^2
  // Order is [x: left-right, y: up-down, z: forward-back]
  // We'll make these the same for every point, just for demonstration purposes.
  std::vector<double> variances{1e-6, 1e-6, 1e-6};

  // We order these top-bottom --> left-right.
  // This matches right corner with the double-loop logic below.
  std::vector<uint16_t> corner_array{0, 3, 1, 2};

  int marker0_id = 0;
  int marker1_id = 1;
  for (int x = 0; x < 2; x++) {
    for (int y = 0; y < 2; y++) {
      int current_idx = (x * 2) + y;
      int64_t corner_id_marker0 = (marker0_id * 4) + corner_array[current_idx];
      int64_t corner_id_marker1 = (marker1_id * 4) + corner_array[current_idx];

      // Corner from id0
      targets.push_back(
          tangviz::Target{std::vector<double>{x * 0.025, y * 0.025, 0.0},
                          corner_id_marker0, variances});
      // Corner from id1
      targets.push_back(
          tangviz::Target{std::vector<double>{x * 0.025 + 0.05, y * 0.025, 0.0},
                          corner_id_marker1, variances});
    }
  }

  tangviz::Objectspace objspc_markers{tangviz::Camera{
      std::make_shared<tangviz::Descriptor>(tangviz::Descriptor{targets}),
      detector}};

  nlohmann::json j;
  nlohmann::to_json(j, objspc_markers);
  // Important; validation will fail if null values are present
  recurseAndFilterNullValues(j);

  // Generate the object space file into our build folder
  std::ofstream checkerboard_file;
  checkerboard_file.open("markerscpp.json");
  checkerboard_file << j.dump();
  checkerboard_file.close();
}

int main() {
  create_checkerboard_objspc();
  create_markers_objspc();
  return 0;
}