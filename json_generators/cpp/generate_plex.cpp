// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
// Sample program demonstrating how to generate your own Plex file in C++

#include "plex.h"
#include <Eigen/Core>
#include <fstream>
#include <iostream>
#include <nlohmann/json.hpp>
#include <time.h>
#include <uuid.h>

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//    Remove None Values
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Thanks to:
// https://github.com/nlohmann/json/issues/995
static void recurseAndFilterNullValues(nlohmann::json &jsonObject) {
  if (!jsonObject.is_object() && !jsonObject.is_array()) {
    return;
  }
  std::vector<nlohmann::json::object_t::key_type> keysToRemove;
  for (auto &it : jsonObject.items()) {
    if (it.value().is_null()) {
      keysToRemove.push_back(it.key());
      continue;
    }
    recurseAndFilterNullValues(it.value());
  }
  for (auto key : keysToRemove) {
    jsonObject.erase(key);
  }
}

const uuids::uuid generate_uuid_v4() {
  std::random_device rd;
  auto seed_data = std::array<int, 6>{};
  std::generate(std::begin(seed_data), std::end(seed_data), std::ref(rd));
  std::seed_seq seq(std::begin(seed_data), std::end(seed_data));
  std::ranlux48_base generator(seq);

  uuids::basic_uuid_random_generator<std::ranlux48_base> gen(&generator);
  return gen();
}

int main() {
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //     Constants
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  uint32_t width = 640;
  uint32_t height = 480;
  double cx = width / 2;
  double cy = height / 2;
  double radius = sqrt(cx * cx + cy * cy);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //     Camera Alpha - Narrow FOV
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  double field_of_view = 90 * M_PI / 180;
  double f = radius / tan(field_of_view / 2.0);

  tangviz::Pinhole c0_pinhole{cx, cy, f};
  tangviz::Projection c0_projection{c0_pinhole};
  std::vector<double> c0_proj_covariance = std::vector<double>{
      100.0, // standard deviation of f
      25.0,  // standard deviation of cx
      25.0   // standard deviation of cy
  };

  tangviz::BrownConrady distortion_bc{
      0.0, 0.0, 0.0, 0.0, 0.0,
  };
  tangviz::Distortion c0_distortion{
      std::make_shared<tangviz::BrownConrady>(distortion_bc),
      nullptr // No Kannala-Brandt here
  };
  std::vector<double> c0_dist_covariance = std::vector<double>{
      1.0, // standard deviation of k1
      1.0, // standard deviation of k2
      1.0, // standard deviation of k3
      1.0, // standard deviation of p1
      1.0, // standard deviation of p2
  };

  tangviz::Affinity c0_affinity{
      std::make_shared<double>(0.0), // no scale factor
      std::make_shared<double>(0.0), // no shear factor

  };
  std::vector<double> c0_aff_covariance = std::vector<double>{
      0.5, // standard deviation of a1
      0.5  // standard deviation of a2
  };

  tangviz::Intrinsics c0_intrinsics{
      std::make_shared<tangviz::Affinity>(c0_affinity),
      std::make_shared<tangviz::Distortion>(c0_distortion), height,
      c0_projection, width};

  // Covariance is always ordered
  // - Projection
  // - Distortion
  // - Affinity
  std::vector<double> covariance_vec{};
  covariance_vec.insert(covariance_vec.end(), c0_proj_covariance.begin(),
                        c0_proj_covariance.end());
  covariance_vec.insert(covariance_vec.end(), c0_dist_covariance.begin(),
                        c0_dist_covariance.end());
  covariance_vec.insert(covariance_vec.end(), c0_aff_covariance.begin(),
                        c0_aff_covariance.end());
  Eigen::VectorXd c0_covariance_diag =
      Eigen::Map<Eigen::VectorXd>(covariance_vec.data(), covariance_vec.size());

  // Convert this from std_dev to variance by squaring it
  Eigen::MatrixXd c0_covariance =
      c0_covariance_diag.array().square().matrix().asDiagonal();

  // A plex takes covariance in the format
  //   [[flattened covariance matrix], matrix len, matrix len]
  // This is a quirk of nalgebra's serialization (a Rust mathematics crate)
  Eigen::Map<const Eigen::RowVectorXd> cov_map(c0_covariance.data(),
                                               c0_covariance.size());
  std::vector<double> cov_data =
      std::vector<double>(cov_map.data(), cov_map.data() + cov_map.size());
  std::vector<tangviz::CovarianceElement> c0_covariance_formatted{
      cov_data, c0_covariance.size(), c0_covariance.size()};

  // Plex uses UUID v4
  uuids::uuid const c0_uuid = generate_uuid_v4();
  std::string c0_uuid_str = uuids::to_string<char>(c0_uuid);
  std::string c0_name = "alpha";
  float c0_pixel_pitch = 1.0;

  tangviz::Camera c0_cam{
      c0_covariance_formatted, c0_intrinsics, c0_name,
      c0_pixel_pitch,          c0_uuid_str,   c0_uuid_str,
  };

  tangviz::Component c0{c0_cam};

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~y
  //     Camera Beta - Wide FOV
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  double field_of_view_1 = 120 * M_PI / 180;
  f = radius / tan(field_of_view / 2.0);

  tangviz::Pinhole c1_pinhole{cx, cy, f};
  tangviz::Projection c1_projection{c1_pinhole};
  std::vector<double> c1_proj_covariance = std::vector<double>{
      100.0, // standard deviation of f
      25.0,  // standard deviation of cx
      25.0   // standard deviation of cy
  };

  tangviz::KannalaBrandt distortion_kb{0.0, 0.0, 0.0, 0.0};
  tangviz::Distortion c1_distortion{
      nullptr, // No Brown-Conrady here
      std::make_shared<tangviz::KannalaBrandt>(distortion_kb)};
  std::vector<double> c1_dist_covariance = std::vector<double>{
      1.0, // standard deviation of k1
      1.0, // standard deviation of k2
      1.0, // standard deviation of k3
      1.0, // standard deviation of k4
  };

  tangviz::Intrinsics c1_intrinsics{
      nullptr, // No Affinity in this camera...
      std::make_shared<tangviz::Distortion>(c1_distortion), height,
      c1_projection, width};

  std::vector<double> covariance_vec_1{};
  covariance_vec_1.insert(covariance_vec_1.end(), c1_proj_covariance.begin(),
                          c1_proj_covariance.end());
  covariance_vec_1.insert(covariance_vec_1.end(), c1_dist_covariance.begin(),
                          c1_dist_covariance.end());
  Eigen::VectorXd c1_covariance_diag = Eigen::Map<Eigen::VectorXd>(
      covariance_vec_1.data(), covariance_vec_1.size());
  Eigen::MatrixXd c1_covariance =
      c1_covariance_diag.array().square().matrix().asDiagonal();
  Eigen::Map<const Eigen::RowVectorXd> cov_map_1(c1_covariance.data(),
                                                 c1_covariance.size());
  std::vector<double> cov_data_1 = std::vector<double>(
      cov_map_1.data(), cov_map_1.data() + cov_map_1.size());
  std::vector<tangviz::CovarianceElement> c1_covariance_formatted{
      cov_data_1, c1_covariance.size(), c1_covariance.size()};

  uuids::uuid const c1_uuid = generate_uuid_v4();
  std::string c1_uuid_str = uuids::to_string<char>(c1_uuid);
  std::string c1_name = "beta";
  float c1_pixel_pitch = 1.0;

  // root uuid points to Alpha
  tangviz::Camera c1_cam{
      c1_covariance_formatted, c1_intrinsics, c1_name,
      c1_pixel_pitch,          c0_uuid_str,   c1_uuid_str,
  };

  tangviz::Component c1{c1_cam};

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //     Camera Alpha < -> Camera Beta Spatial Constraint
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  std::string to_component = c0_uuid_str;
  std::string from_component = c1_uuid_str;
  // Rotation as a quaternion
  // Format: [x, y, z, w]
  std::vector<double> rotation{0.0, 0.0, 0.0, 1.0};
  // translation in meters
  // convention: (+X: right, +Y: down, +Z: forward)
  std::vector<double> translation{0.02, 0.001, 0.0};
  tangviz::Extrinsics extrinsics{rotation, translation};

  // extrinsics covariance is currently in se3 space. This will change soon.
  // For now, these are fine defaults.
  // - translation standard deviation of 15m (basically, we know nothing)
  // - rotation standard deviation of 15 degrees
  tangviz::CovarianceClass extrinsics_covariance{
      std::vector<double>{209.2163483383551,   0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                          226.15189405951685,  0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                          239.8072670298675,   0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                          0.05042861912254395, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                          0.08564778378373221, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                          0.05042861912254395}};

  tangviz::SpatialConstraint spatial_constraint{
      extrinsics_covariance, extrinsics, from_component, to_component};

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //     Camera Alpha < -> Camera Beta Temporal Constraint
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  // No offset or skew here
  tangviz::Synchronization sync{0, 0};
  // Convert resolution to nanoseconds
  uint8_t c0_fps = 30;
  int64_t resolution = int64_t(((1 / c0_fps) / 2) * 1e9);
  tangviz::TemporalConstraint temporal_constraint{from_component, resolution,
                                                  sync, to_component};

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //     Put our Plex Together!
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  std::vector<tangviz::Component> components{c0, c1};
  int64_t creation_timestamp = int64_t(time(NULL) * 1e9);
  tangviz::Plex plex{
      components, creation_timestamp,
      std::vector<tangviz::SpatialConstraint>{spatial_constraint},
      std::vector<tangviz::TemporalConstraint>{temporal_constraint},
      uuids::to_string<char>(generate_uuid_v4())};

  nlohmann::json j;
  nlohmann::to_json(j, plex);
  // Important; validation will fail if null values are present
  recurseAndFilterNullValues(j);

  // Generate the plex file into our build folder
  std::ofstream plex_file;
  plex_file.open("plexcpp.json");
  plex_file << j.dump();
  plex_file.close();
  return 0;
}
