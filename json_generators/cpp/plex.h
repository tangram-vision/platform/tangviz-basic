// Copyright(c) 2022 Tangram Robotics Inc.- All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
// Generated C++ interface for creating plex objects and JSON

#pragma once

#include <nlohmann/json.hpp>
#include <variant>

#ifndef NLOHMANN_OPT_HELPER
#define NLOHMANN_OPT_HELPER
namespace nlohmann {
template <typename T> struct adl_serializer<std::shared_ptr<T>> {
  static void to_json(json &j, const std::shared_ptr<T> &opt) {
    if (!opt)
      j = nullptr;
    else
      j = *opt;
  }

  static std::shared_ptr<T> from_json(const json &j) {
    if (j.is_null())
      return std::unique_ptr<T>();
    else
      return std::unique_ptr<T>(new T(j.get<T>()));
  }
};
} // namespace nlohmann
#endif

namespace tangviz {
using nlohmann::json;

inline json get_untyped(const json &j, const char *property) {
  if (j.find(property) != j.end()) {
    return j.at(property).get<json>();
  }
  return json();
}

inline json get_untyped(const json &j, std::string property) {
  return get_untyped(j, property.data());
}

template <typename T>
inline std::shared_ptr<T> get_optional(const json &j, const char *property) {
  if (j.find(property) != j.end()) {
    return j.at(property).get<std::shared_ptr<T>>();
  }
  return std::shared_ptr<T>();
}

template <typename T>
inline std::shared_ptr<T> get_optional(const json &j, std::string property) {
  return get_optional<T>(j, property.data());
}

using CovarianceElement = std::variant<std::vector<double>, int64_t>;

struct Affinity {
  std::shared_ptr<double> a1;
  std::shared_ptr<double> a2;
};

struct BrownConrady {
  double k1;
  double k2;
  double k3;
  double p1;
  double p2;
};

struct KannalaBrandt {
  double k1;
  double k2;
  double k3;
  double k4;
};

struct Distortion {
  std::shared_ptr<BrownConrady> brown_conrady;
  std::shared_ptr<KannalaBrandt> kannala_brandt;
};

struct Pinhole {
  double cx;
  double cy;
  double f;
};

struct Projection {
  Pinhole pinhole;
};

struct Intrinsics {
  std::shared_ptr<Affinity> affinity;
  std::shared_ptr<Distortion> distortion;
  int64_t height;
  Projection projection;
  int64_t width;
};

struct Camera {
  std::vector<CovarianceElement> covariance;
  Intrinsics intrinsics;
  std::string name;
  double pixel_pitch;
  std::string root_uuid;
  std::string uuid;
};

struct Component {
  Camera camera;
};

struct CovarianceClass {
  std::vector<double> raw_se3;
};

struct Extrinsics {
  std::vector<double> rotation;
  std::vector<double> translation;
};

struct SpatialConstraint {
  CovarianceClass covariance;
  Extrinsics extrinsics;
  std::string from;
  std::string to;
};

struct Synchronization {
  int64_t offset;
  int64_t skew;
};

struct TemporalConstraint {
  std::string from;
  int64_t resolution;
  Synchronization synchronization;
  std::string to;
};

struct Plex {
  std::vector<Component> components;
  int64_t creation_timestamp;
  std::vector<SpatialConstraint> spatial_constraints;
  std::vector<TemporalConstraint> temporal_constraints;
  std::string uuid;
};
} // namespace tangviz

namespace nlohmann {

void from_json(const json &j, tangviz::Affinity &x);
void to_json(json &j, const tangviz::Affinity &x);

void from_json(const json &j, tangviz::BrownConrady &x);
void to_json(json &j, const tangviz::BrownConrady &x);

void from_json(const json &j, tangviz::KannalaBrandt &x);
void to_json(json &j, const tangviz::KannalaBrandt &x);

void from_json(const json &j, tangviz::Distortion &x);
void to_json(json &j, const tangviz::Distortion &x);

void from_json(const json &j, tangviz::Pinhole &x);
void to_json(json &j, const tangviz::Pinhole &x);

void from_json(const json &j, tangviz::Projection &x);
void to_json(json &j, const tangviz::Projection &x);

void from_json(const json &j, tangviz::Intrinsics &x);
void to_json(json &j, const tangviz::Intrinsics &x);

void from_json(const json &j, tangviz::Camera &x);
void to_json(json &j, const tangviz::Camera &x);

void from_json(const json &j, tangviz::Component &x);
void to_json(json &j, const tangviz::Component &x);

void from_json(const json &j, tangviz::CovarianceClass &x);
void to_json(json &j, const tangviz::CovarianceClass &x);

void from_json(const json &j, tangviz::Extrinsics &x);
void to_json(json &j, const tangviz::Extrinsics &x);

void from_json(const json &j, tangviz::SpatialConstraint &x);
void to_json(json &j, const tangviz::SpatialConstraint &x);

void from_json(const json &j, tangviz::Synchronization &x);
void to_json(json &j, const tangviz::Synchronization &x);

void from_json(const json &j, tangviz::TemporalConstraint &x);
void to_json(json &j, const tangviz::TemporalConstraint &x);

void from_json(const json &j, tangviz::Plex &x);
void to_json(json &j, const tangviz::Plex &x);

void from_json(const json &j, std::variant<std::vector<double>, int64_t> &x);
void to_json(json &j, const std::variant<std::vector<double>, int64_t> &x);

inline void from_json(const json &j, tangviz::Affinity &x) {
  x.a1 = tangviz::get_optional<double>(j, "a1");
  x.a2 = tangviz::get_optional<double>(j, "a2");
}

inline void to_json(json &j, const tangviz::Affinity &x) {
  j = json::object();
  j["a1"] = x.a1;
  j["a2"] = x.a2;
}

inline void from_json(const json &j, tangviz::BrownConrady &x) {
  x.k1 = j.at("k1").get<double>();
  x.k2 = j.at("k2").get<double>();
  x.k3 = j.at("k3").get<double>();
  x.p1 = j.at("p1").get<double>();
  x.p2 = j.at("p2").get<double>();
}

inline void to_json(json &j, const tangviz::BrownConrady &x) {
  j = json::object();
  j["k1"] = x.k1;
  j["k2"] = x.k2;
  j["k3"] = x.k3;
  j["p1"] = x.p1;
  j["p2"] = x.p2;
}

inline void from_json(const json &j, tangviz::KannalaBrandt &x) {
  x.k1 = j.at("k1").get<double>();
  x.k2 = j.at("k2").get<double>();
  x.k3 = j.at("k3").get<double>();
  x.k4 = j.at("k4").get<double>();
}

inline void to_json(json &j, const tangviz::KannalaBrandt &x) {
  j = json::object();
  j["k1"] = x.k1;
  j["k2"] = x.k2;
  j["k3"] = x.k3;
  j["k4"] = x.k4;
}

inline void from_json(const json &j, tangviz::Distortion &x) {
  x.brown_conrady =
      tangviz::get_optional<tangviz::BrownConrady>(j, "brown_conrady");
  x.kannala_brandt =
      tangviz::get_optional<tangviz::KannalaBrandt>(j, "kannala_brandt");
}

inline void to_json(json &j, const tangviz::Distortion &x) {
  j = json::object();
  j["brown_conrady"] = x.brown_conrady;
  j["kannala_brandt"] = x.kannala_brandt;
}

inline void from_json(const json &j, tangviz::Pinhole &x) {
  x.cx = j.at("cx").get<double>();
  x.cy = j.at("cy").get<double>();
  x.f = j.at("f").get<double>();
}

inline void to_json(json &j, const tangviz::Pinhole &x) {
  j = json::object();
  j["cx"] = x.cx;
  j["cy"] = x.cy;
  j["f"] = x.f;
}

inline void from_json(const json &j, tangviz::Projection &x) {
  x.pinhole = j.at("pinhole").get<tangviz::Pinhole>();
}

inline void to_json(json &j, const tangviz::Projection &x) {
  j = json::object();
  j["pinhole"] = x.pinhole;
}

inline void from_json(const json &j, tangviz::Intrinsics &x) {
  x.affinity = tangviz::get_optional<tangviz::Affinity>(j, "affinity");
  x.distortion = tangviz::get_optional<tangviz::Distortion>(j, "distortion");
  x.height = j.at("height").get<int64_t>();
  x.projection = j.at("projection").get<tangviz::Projection>();
  x.width = j.at("width").get<int64_t>();
}

inline void to_json(json &j, const tangviz::Intrinsics &x) {
  j = json::object();
  j["affinity"] = x.affinity;
  j["distortion"] = x.distortion;
  j["height"] = x.height;
  j["projection"] = x.projection;
  j["width"] = x.width;
}

inline void from_json(const json &j, tangviz::Camera &x) {
  x.covariance =
      j.at("covariance").get<std::vector<tangviz::CovarianceElement>>();
  x.intrinsics = j.at("intrinsics").get<tangviz::Intrinsics>();
  x.name = j.at("name").get<std::string>();
  x.pixel_pitch = j.at("pixel_pitch").get<double>();
  x.root_uuid = j.at("root_uuid").get<std::string>();
  x.uuid = j.at("uuid").get<std::string>();
}

inline void to_json(json &j, const tangviz::Camera &x) {
  j = json::object();
  j["covariance"] = x.covariance;
  j["intrinsics"] = x.intrinsics;
  j["name"] = x.name;
  j["pixel_pitch"] = x.pixel_pitch;
  j["root_uuid"] = x.root_uuid;
  j["uuid"] = x.uuid;
}

inline void from_json(const json &j, tangviz::Component &x) {
  x.camera = j.at("camera").get<tangviz::Camera>();
}

inline void to_json(json &j, const tangviz::Component &x) {
  j = json::object();
  j["camera"] = x.camera;
}

inline void from_json(const json &j, tangviz::CovarianceClass &x) {
  x.raw_se3 = j.at("raw_se3").get<std::vector<double>>();
}

inline void to_json(json &j, const tangviz::CovarianceClass &x) {
  j = json::object();
  j["raw_se3"] = x.raw_se3;
}

inline void from_json(const json &j, tangviz::Extrinsics &x) {
  x.rotation = j.at("rotation").get<std::vector<double>>();
  x.translation = j.at("translation").get<std::vector<double>>();
}

inline void to_json(json &j, const tangviz::Extrinsics &x) {
  j = json::object();
  j["rotation"] = x.rotation;
  j["translation"] = x.translation;
}

inline void from_json(const json &j, tangviz::SpatialConstraint &x) {
  x.covariance = j.at("covariance").get<tangviz::CovarianceClass>();
  x.extrinsics = j.at("extrinsics").get<tangviz::Extrinsics>();
  x.from = j.at("from").get<std::string>();
  x.to = j.at("to").get<std::string>();
}

inline void to_json(json &j, const tangviz::SpatialConstraint &x) {
  j = json::object();
  j["covariance"] = x.covariance;
  j["extrinsics"] = x.extrinsics;
  j["from"] = x.from;
  j["to"] = x.to;
}

inline void from_json(const json &j, tangviz::Synchronization &x) {
  x.offset = j.at("offset").get<int64_t>();
  x.skew = j.at("skew").get<int64_t>();
}

inline void to_json(json &j, const tangviz::Synchronization &x) {
  j = json::object();
  j["offset"] = x.offset;
  j["skew"] = x.skew;
}

inline void from_json(const json &j, tangviz::TemporalConstraint &x) {
  x.from = j.at("from").get<std::string>();
  x.resolution = j.at("resolution").get<int64_t>();
  x.synchronization = j.at("synchronization").get<tangviz::Synchronization>();
  x.to = j.at("to").get<std::string>();
}

inline void to_json(json &j, const tangviz::TemporalConstraint &x) {
  j = json::object();
  j["from"] = x.from;
  j["resolution"] = x.resolution;
  j["synchronization"] = x.synchronization;
  j["to"] = x.to;
}

inline void from_json(const json &j, tangviz::Plex &x) {
  x.components = j.at("components").get<std::vector<tangviz::Component>>();
  x.creation_timestamp = j.at("creation_timestamp").get<int64_t>();
  x.spatial_constraints = j.at("spatial_constraints")
                              .get<std::vector<tangviz::SpatialConstraint>>();
  x.temporal_constraints = j.at("temporal_constraints")
                               .get<std::vector<tangviz::TemporalConstraint>>();
  x.uuid = j.at("uuid").get<std::string>();
}

inline void to_json(json &j, const tangviz::Plex &x) {
  j = json::object();
  j["components"] = x.components;
  j["creation_timestamp"] = x.creation_timestamp;
  j["spatial_constraints"] = x.spatial_constraints;
  j["temporal_constraints"] = x.temporal_constraints;
  j["uuid"] = x.uuid;
}
inline void from_json(const json &j,
                      std::variant<std::vector<double>, int64_t> &x) {
  if (j.is_number_integer())
    x = j.get<int64_t>();
  else if (j.is_array())
    x = j.get<std::vector<double>>();
  else
    throw "Could not deserialize";
}

inline void to_json(json &j,
                    const std::variant<std::vector<double>, int64_t> &x) {
  switch (x.index()) {
  case 0:
    j = std::get<std::vector<double>>(x);
    break;
  case 1:
    j = std::get<int64_t>(x);
    break;
  default:
    throw "Input JSON does not conform to schema";
  }
}
} // namespace nlohmann
