// Copyright(c) 2022 Tangram Robotics Inc.- All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
// Generated C++ interface for creating object space JSON

#pragma once

#include <nlohmann/json.hpp>
#include <unordered_map>

#ifndef NLOHMANN_OPT_HELPER
#define NLOHMANN_OPT_HELPER
namespace nlohmann {
template <typename T> struct adl_serializer<std::shared_ptr<T>> {
  static void to_json(json &j, const std::shared_ptr<T> &opt) {
    if (!opt)
      j = nullptr;
    else
      j = *opt;
  }

  static std::shared_ptr<T> from_json(const json &j) {
    if (j.is_null())
      return std::unique_ptr<T>();
    else
      return std::unique_ptr<T>(new T(j.get<T>()));
  }
};
} // namespace nlohmann
#endif

namespace tangviz {
using nlohmann::json;

inline json get_untyped(const json &j, const char *property) {
  if (j.find(property) != j.end()) {
    return j.at(property).get<json>();
  }
  return json();
}

inline json get_untyped(const json &j, std::string property) {
  return get_untyped(j, property.data());
}

template <typename T>
inline std::shared_ptr<T> get_optional(const json &j, const char *property) {
  if (j.find(property) != j.end()) {
    return j.at(property).get<std::shared_ptr<T>>();
  }
  return std::shared_ptr<T>();
}

template <typename T>
inline std::shared_ptr<T> get_optional(const json &j, std::string property) {
  return get_optional<T>(j, property.data());
}

struct Target {
  std::vector<double> coordinates;
  int64_t id;
  std::vector<double> variances;
};

struct Descriptor {
  std::vector<Target> targets;
};

enum class AprilGridMarkerDictionary : int {
  APRILTAG16_H5,
  APRILTAG25_H9,
  APRILTAG36_H10,
  APRILTAG36_H11,
  APRILTAG_KALIBR
};

struct AprilGridClass {
  int64_t height;
  AprilGridMarkerDictionary marker_dictionary;
  double marker_length;
  double tag_spacing;
  std::vector<double> variances;
  int64_t width;
};

struct Checkerboard {
  double checker_length;
  int64_t height;
  std::vector<double> variances;
  int64_t width;
};

enum class MarkerboardMarkerDictionary : int {
  APRILTAG16_H5,
  APRILTAG25_H9,
  APRILTAG36_H10,
  APRILTAG36_H11,
  APRILTAG_KALIBR,
  ARUCO4_X4_100,
  ARUCO4_X4_1000,
  ARUCO4_X4_250,
  ARUCO4_X4_50,
  ARUCO5_X5_100,
  ARUCO5_X5_1000,
  ARUCO5_X5_250,
  ARUCO5_X5_50,
  ARUCO6_X6_100,
  ARUCO6_X6_1000,
  ARUCO6_X6_250,
  ARUCO6_X6_50,
  ARUCO7_X7_100,
  ARUCO7_X7_1000,
  ARUCO7_X7_250,
  ARUCO7_X7_50,
  ARUCO_ORIGINAL
};

struct MarkerboardClass {
  double checker_length;
  int64_t height;
  MarkerboardMarkerDictionary marker_dictionary;
  double marker_length;
  std::vector<double> variances;
  int64_t width;
};

struct Markers {
  MarkerboardMarkerDictionary marker_dictionary;
  double marker_length;
};

struct Detector {
  std::shared_ptr<AprilGridClass> april_grid;
  std::shared_ptr<Checkerboard> checkerboard;
  std::shared_ptr<MarkerboardClass> markerboard;
  std::shared_ptr<Markers> markers;
};

struct Camera {
  std::shared_ptr<Descriptor> descriptor;
  Detector detector;
};

struct Objectspace {
  Camera camera;
};
} // namespace tangviz

namespace nlohmann {
void from_json(const json &j, tangviz::Target &x);
void to_json(json &j, const tangviz::Target &x);

void from_json(const json &j, tangviz::Descriptor &x);
void to_json(json &j, const tangviz::Descriptor &x);

void from_json(const json &j, tangviz::AprilGridClass &x);
void to_json(json &j, const tangviz::AprilGridClass &x);

void from_json(const json &j, tangviz::Checkerboard &x);
void to_json(json &j, const tangviz::Checkerboard &x);

void from_json(const json &j, tangviz::MarkerboardClass &x);
void to_json(json &j, const tangviz::MarkerboardClass &x);

void from_json(const json &j, tangviz::Markers &x);
void to_json(json &j, const tangviz::Markers &x);

void from_json(const json &j, tangviz::Detector &x);
void to_json(json &j, const tangviz::Detector &x);

void from_json(const json &j, tangviz::Camera &x);
void to_json(json &j, const tangviz::Camera &x);

void from_json(const json &j, tangviz::Objectspace &x);
void to_json(json &j, const tangviz::Objectspace &x);

void from_json(const json &j, tangviz::AprilGridMarkerDictionary &x);
void to_json(json &j, const tangviz::AprilGridMarkerDictionary &x);

void from_json(const json &j, tangviz::MarkerboardMarkerDictionary &x);
void to_json(json &j, const tangviz::MarkerboardMarkerDictionary &x);

inline void from_json(const json &j, tangviz::Target &x) {
  x.coordinates = j.at("coordinates").get<std::vector<double>>();
  x.id = j.at("id").get<int64_t>();
  x.variances = j.at("variances").get<std::vector<double>>();
}

inline void to_json(json &j, const tangviz::Target &x) {
  j = json::object();
  j["coordinates"] = x.coordinates;
  j["id"] = x.id;
  j["variances"] = x.variances;
}

inline void from_json(const json &j, tangviz::Descriptor &x) {
  x.targets = j.at("targets").get<std::vector<tangviz::Target>>();
}

inline void to_json(json &j, const tangviz::Descriptor &x) {
  j = json::object();
  j["targets"] = x.targets;
}

inline void from_json(const json &j, tangviz::AprilGridClass &x) {
  x.height = j.at("height").get<int64_t>();
  x.marker_dictionary =
      j.at("marker_dictionary").get<tangviz::AprilGridMarkerDictionary>();
  x.marker_length = j.at("marker_length").get<double>();
  x.tag_spacing = j.at("tag_spacing").get<double>();
  x.variances = j.at("variances").get<std::vector<double>>();
  x.width = j.at("width").get<int64_t>();
}

inline void to_json(json &j, const tangviz::AprilGridClass &x) {
  j = json::object();
  j["height"] = x.height;
  j["marker_dictionary"] = x.marker_dictionary;
  j["marker_length"] = x.marker_length;
  j["tag_spacing"] = x.tag_spacing;
  j["variances"] = x.variances;
  j["width"] = x.width;
}

inline void from_json(const json &j, tangviz::Checkerboard &x) {
  x.checker_length = j.at("checker_length").get<double>();
  x.height = j.at("height").get<int64_t>();
  x.variances = j.at("variances").get<std::vector<double>>();
  x.width = j.at("width").get<int64_t>();
}

inline void to_json(json &j, const tangviz::Checkerboard &x) {
  j = json::object();
  j["checker_length"] = x.checker_length;
  j["height"] = x.height;
  j["variances"] = x.variances;
  j["width"] = x.width;
}

inline void from_json(const json &j, tangviz::MarkerboardClass &x) {
  x.checker_length = j.at("checker_length").get<double>();
  x.height = j.at("height").get<int64_t>();
  x.marker_dictionary =
      j.at("marker_dictionary").get<tangviz::MarkerboardMarkerDictionary>();
  x.marker_length = j.at("marker_length").get<double>();
  x.variances = j.at("variances").get<std::vector<double>>();
  x.width = j.at("width").get<int64_t>();
}

inline void to_json(json &j, const tangviz::MarkerboardClass &x) {
  j = json::object();
  j["checker_length"] = x.checker_length;
  j["height"] = x.height;
  j["marker_dictionary"] = x.marker_dictionary;
  j["marker_length"] = x.marker_length;
  j["variances"] = x.variances;
  j["width"] = x.width;
}

inline void from_json(const json &j, tangviz::Markers &x) {
  x.marker_dictionary =
      j.at("marker_dictionary").get<tangviz::MarkerboardMarkerDictionary>();
  x.marker_length = j.at("marker_length").get<double>();
}

inline void to_json(json &j, const tangviz::Markers &x) {
  j = json::object();
  j["marker_dictionary"] = x.marker_dictionary;
  j["marker_length"] = x.marker_length;
}

inline void from_json(const json &j, tangviz::Detector &x) {
  x.april_grid =
      tangviz::get_optional<tangviz::AprilGridClass>(j, "april_grid");
  x.checkerboard =
      tangviz::get_optional<tangviz::Checkerboard>(j, "checkerboard");
  x.markerboard =
      tangviz::get_optional<tangviz::MarkerboardClass>(j, "markerboard");
  x.markers = tangviz::get_optional<tangviz::Markers>(j, "markers");
}

inline void to_json(json &j, const tangviz::Detector &x) {
  j = json::object();
  j["april_grid"] = x.april_grid;
  j["checkerboard"] = x.checkerboard;
  j["markerboard"] = x.markerboard;
  j["markers"] = x.markers;
}

inline void from_json(const json &j, tangviz::Camera &x) {
  x.descriptor = tangviz::get_optional<tangviz::Descriptor>(j, "descriptor");
  x.detector = j.at("detector").get<tangviz::Detector>();
}

inline void to_json(json &j, const tangviz::Camera &x) {
  j = json::object();
  j["descriptor"] = x.descriptor;
  j["detector"] = x.detector;
}

inline void from_json(const json &j, tangviz::Objectspace &x) {
  x.camera = j.at("camera").get<tangviz::Camera>();
}

inline void to_json(json &j, const tangviz::Objectspace &x) {
  j = json::object();
  j["camera"] = x.camera;
}

inline void from_json(const json &j, tangviz::AprilGridMarkerDictionary &x) {
  if (j == "Apriltag16h5")
    x = tangviz::AprilGridMarkerDictionary::APRILTAG16_H5;
  else if (j == "Apriltag25h9")
    x = tangviz::AprilGridMarkerDictionary::APRILTAG25_H9;
  else if (j == "Apriltag36h10")
    x = tangviz::AprilGridMarkerDictionary::APRILTAG36_H10;
  else if (j == "Apriltag36h11")
    x = tangviz::AprilGridMarkerDictionary::APRILTAG36_H11;
  else if (j == "ApriltagKalibr")
    x = tangviz::AprilGridMarkerDictionary::APRILTAG_KALIBR;
  else
    throw "Input JSON does not conform to schema";
}

inline void to_json(json &j, const tangviz::AprilGridMarkerDictionary &x) {
  switch (x) {
  case tangviz::AprilGridMarkerDictionary::APRILTAG16_H5:
    j = "Apriltag16h5";
    break;
  case tangviz::AprilGridMarkerDictionary::APRILTAG25_H9:
    j = "Apriltag25h9";
    break;
  case tangviz::AprilGridMarkerDictionary::APRILTAG36_H10:
    j = "Apriltag36h10";
    break;
  case tangviz::AprilGridMarkerDictionary::APRILTAG36_H11:
    j = "Apriltag36h11";
    break;
  case tangviz::AprilGridMarkerDictionary::APRILTAG_KALIBR:
    j = "ApriltagKalibr";
    break;
  default:
    throw "This should not happen";
  }
}

inline void from_json(const json &j, tangviz::MarkerboardMarkerDictionary &x) {
  static std::unordered_map<std::string, tangviz::MarkerboardMarkerDictionary>
      enumValues{
          {"Apriltag16h5", tangviz::MarkerboardMarkerDictionary::APRILTAG16_H5},
          {"Apriltag25h9", tangviz::MarkerboardMarkerDictionary::APRILTAG25_H9},
          {"Apriltag36h10",
           tangviz::MarkerboardMarkerDictionary::APRILTAG36_H10},
          {"Apriltag36h11",
           tangviz::MarkerboardMarkerDictionary::APRILTAG36_H11},
          {"ApriltagKalibr",
           tangviz::MarkerboardMarkerDictionary::APRILTAG_KALIBR},
          {"Aruco4x4_100", tangviz::MarkerboardMarkerDictionary::ARUCO4_X4_100},
          {"Aruco4x4_1000",
           tangviz::MarkerboardMarkerDictionary::ARUCO4_X4_1000},
          {"Aruco4x4_250", tangviz::MarkerboardMarkerDictionary::ARUCO4_X4_250},
          {"Aruco4x4_50", tangviz::MarkerboardMarkerDictionary::ARUCO4_X4_50},
          {"Aruco5x5_100", tangviz::MarkerboardMarkerDictionary::ARUCO5_X5_100},
          {"Aruco5x5_1000",
           tangviz::MarkerboardMarkerDictionary::ARUCO5_X5_1000},
          {"Aruco5x5_250", tangviz::MarkerboardMarkerDictionary::ARUCO5_X5_250},
          {"Aruco5x5_50", tangviz::MarkerboardMarkerDictionary::ARUCO5_X5_50},
          {"Aruco6x6_100", tangviz::MarkerboardMarkerDictionary::ARUCO6_X6_100},
          {"Aruco6x6_1000",
           tangviz::MarkerboardMarkerDictionary::ARUCO6_X6_1000},
          {"Aruco6x6_250", tangviz::MarkerboardMarkerDictionary::ARUCO6_X6_250},
          {"Aruco6x6_50", tangviz::MarkerboardMarkerDictionary::ARUCO6_X6_50},
          {"Aruco7x7_100", tangviz::MarkerboardMarkerDictionary::ARUCO7_X7_100},
          {"Aruco7x7_1000",
           tangviz::MarkerboardMarkerDictionary::ARUCO7_X7_1000},
          {"Aruco7x7_250", tangviz::MarkerboardMarkerDictionary::ARUCO7_X7_250},
          {"Aruco7x7_50", tangviz::MarkerboardMarkerDictionary::ARUCO7_X7_50},
          {"ArucoOriginal",
           tangviz::MarkerboardMarkerDictionary::ARUCO_ORIGINAL},
      };
  auto iter = enumValues.find(j.get<std::string>());
  if (iter != enumValues.end()) {
    x = iter->second;
  }
}

inline void to_json(json &j, const tangviz::MarkerboardMarkerDictionary &x) {
  switch (x) {
  case tangviz::MarkerboardMarkerDictionary::APRILTAG16_H5:
    j = "Apriltag16h5";
    break;
  case tangviz::MarkerboardMarkerDictionary::APRILTAG25_H9:
    j = "Apriltag25h9";
    break;
  case tangviz::MarkerboardMarkerDictionary::APRILTAG36_H10:
    j = "Apriltag36h10";
    break;
  case tangviz::MarkerboardMarkerDictionary::APRILTAG36_H11:
    j = "Apriltag36h11";
    break;
  case tangviz::MarkerboardMarkerDictionary::APRILTAG_KALIBR:
    j = "ApriltagKalibr";
    break;
  case tangviz::MarkerboardMarkerDictionary::ARUCO4_X4_100:
    j = "Aruco4x4_100";
    break;
  case tangviz::MarkerboardMarkerDictionary::ARUCO4_X4_1000:
    j = "Aruco4x4_1000";
    break;
  case tangviz::MarkerboardMarkerDictionary::ARUCO4_X4_250:
    j = "Aruco4x4_250";
    break;
  case tangviz::MarkerboardMarkerDictionary::ARUCO4_X4_50:
    j = "Aruco4x4_50";
    break;
  case tangviz::MarkerboardMarkerDictionary::ARUCO5_X5_100:
    j = "Aruco5x5_100";
    break;
  case tangviz::MarkerboardMarkerDictionary::ARUCO5_X5_1000:
    j = "Aruco5x5_1000";
    break;
  case tangviz::MarkerboardMarkerDictionary::ARUCO5_X5_250:
    j = "Aruco5x5_250";
    break;
  case tangviz::MarkerboardMarkerDictionary::ARUCO5_X5_50:
    j = "Aruco5x5_50";
    break;
  case tangviz::MarkerboardMarkerDictionary::ARUCO6_X6_100:
    j = "Aruco6x6_100";
    break;
  case tangviz::MarkerboardMarkerDictionary::ARUCO6_X6_1000:
    j = "Aruco6x6_1000";
    break;
  case tangviz::MarkerboardMarkerDictionary::ARUCO6_X6_250:
    j = "Aruco6x6_250";
    break;
  case tangviz::MarkerboardMarkerDictionary::ARUCO6_X6_50:
    j = "Aruco6x6_50";
    break;
  case tangviz::MarkerboardMarkerDictionary::ARUCO7_X7_100:
    j = "Aruco7x7_100";
    break;
  case tangviz::MarkerboardMarkerDictionary::ARUCO7_X7_1000:
    j = "Aruco7x7_1000";
    break;
  case tangviz::MarkerboardMarkerDictionary::ARUCO7_X7_250:
    j = "Aruco7x7_250";
    break;
  case tangviz::MarkerboardMarkerDictionary::ARUCO7_X7_50:
    j = "Aruco7x7_50";
    break;
  case tangviz::MarkerboardMarkerDictionary::ARUCO_ORIGINAL:
    j = "ArucoOriginal";
    break;
  default:
    throw "This should not happen";
  }
}
} // namespace nlohmann
