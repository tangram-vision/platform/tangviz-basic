// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! This program is used to generate the JSON serializers in other target languages. It uses
//! [quicktype](https://github.com/quicktype/quicktype) under the hood to accomplish this. Since
//! quicktype is pretty good at what it does, we just have to create a verbose enough object space,
//! and it will take care of the rest.
//!
//! quicktype command line arguments:
//!
//! ```shell
//! quicktype --src markerboard.json --src checkerboard.json \
//!     --src aprilgrid.json --src markers.json -o ObjectSpace.json -l schema
//! ```

use anyhow::Result;
use serde_json::json;
use tangviz_basic::base::object_space::{
    Descriptor, Detector, DetectorDescriptor, ObjectSpaceConfig, Target,
};

fn main() -> Result<()> {
    let usize_var = 1_usize;
    let f64_var = 1.3_f64;

    // Checkerboards
    let checkerboards = ObjectSpaceConfig {
        camera: DetectorDescriptor {
            detector: Detector::Checkerboard {
                width: usize_var,
                height: usize_var,
                checker_length: f64_var,
                variances: [f64_var, f64_var, f64_var],
            },
            descriptor: Descriptor::DetectorDefined,
        },
    };

    // Markerboards
    let markerboards = ObjectSpaceConfig {
        camera: DetectorDescriptor {
            detector: Detector::Markerboard {
                width: usize_var,
                height: usize_var,
                checker_length: f64_var,
                marker_length: f64_var,
                marker_dictionary: String::from("Apriltag36h11"), // has to be valid string
                variances: [f64_var, f64_var, f64_var],
            },
            descriptor: Descriptor::DetectorDefined,
        },
    };

    // AprilGrid
    let aprilgrid = ObjectSpaceConfig {
        camera: DetectorDescriptor {
            detector: Detector::AprilGrid {
                width: usize_var,
                height: usize_var,
                marker_length: f64_var,
                tag_spacing: f64_var,
                marker_dictionary: String::from("Apriltag36h10"), // has to be valid string
                variances: [f64_var, f64_var, f64_var],
            },
            descriptor: Descriptor::DetectorDefined,
        },
    };

    // Markers
    let markers = ObjectSpaceConfig {
        camera: DetectorDescriptor {
            detector: Detector::Markers {
                marker_length: f64_var,
                marker_dictionary: String::from("Aruco4x4_50"), // has to be valid string
            },
            descriptor: Descriptor::TargetList {
                targets: vec![
                    Target {
                        id: usize_var,
                        coordinates: [f64_var, f64_var, f64_var],
                        variances: [f64_var, f64_var, f64_var],
                    },
                    Target {
                        id: usize_var,
                        coordinates: [f64_var, f64_var, f64_var],
                        variances: [f64_var, f64_var, f64_var],
                    },
                ],
            },
        },
    };

    std::fs::write("checkerboard.json", json!(&checkerboards).to_string())
        .expect("Unable to write file");
    std::fs::write("markerboard.json", json!(&markerboards).to_string())
        .expect("Unable to write file");
    std::fs::write("markers.json", json!(&markers).to_string()).expect("Unable to write file");
    std::fs::write("aprilgrid.json", json!(&aprilgrid).to_string()).expect("Unable to write file");
    Ok(())
}
