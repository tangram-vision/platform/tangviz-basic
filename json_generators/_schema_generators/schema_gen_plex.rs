// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! This program is used to generate the JSON serializers in other target languages. It uses
//! [quicktype](https://github.com/quicktype/quicktype) under the hood to accomplish this. Since
//! quicktype is pretty good at what it does, we just have to create a verbose enough Plex, and it
//! will take care of the rest.

use anyhow::Result;
use tangviz_basic::{
    base::{
        AffinityModel, CameraIntrinsics, DistortionModel, ExtrinsicCovariance, Extrinsics,
        ProjectionModel,
    },
    nalgebra::{DMatrix, DVector, Isometry3, Matrix6, Translation3, UnitQuaternion, Vector6},
    plex::{
        Camera, Component, PlexBuilder, SpatialConstraint, Synchronization, TemporalConstraint,
    },
};
use uuid::Uuid;

fn main() -> Result<()> {
    let mut builder = PlexBuilder::new();

    // ~~~~~~~~~~~~
    // lengths for all of our vectors
    // used to do some modulo math later
    let mut comp_vec_len = vec![];

    let val_usize = 1920_usize;
    let val_uint64 = 100_u64;
    let val_int64 = 600_i64;
    let val_float = 90.0_f64;
    let val_string: &str = "tangram_vision_cam";

    // ~~~~~~~~~~~~
    // All options for plex building:
    // - Components
    //   - Camera
    //     - Intrinsics
    //       - Projection (1)
    //         - Pinhole
    //       - Distortion (2)
    //         - No Distortion
    //         - Brown-Conrady
    //         - Kannala-Brandt
    //       - Affinity (4)
    //         - No Affinity
    //         - Scale
    //         - Shear
    //         - ScaleAndShear
    // ==> 4 total component scenarios
    //
    // - Spatial Constraints
    //   - Isometry
    //   - SE3
    //   - Euler Angles
    // ==> 3 total spatial scenarios (soon)
    //
    // - Temporal Constraints
    //   - One way to construct sync + resolution
    // ==> 1 total temporal scenarios
    // ~~~~~~~~~~~~

    ////////
    // Projection
    let projection_vec = vec![ProjectionModel::Pinhole {
        f: val_float,
        cx: val_float,
        cy: val_float,
    }];
    comp_vec_len.push(projection_vec.len());

    ////////
    // Distortion
    let distortion_vec = vec![
        DistortionModel::NoDistortion,
        DistortionModel::BrownConrady {
            k1: val_float,
            k2: val_float,
            k3: val_float,
            p1: val_float,
            p2: val_float,
        },
        DistortionModel::KannalaBrandt {
            k1: val_float,
            k2: val_float,
            k3: val_float,
            k4: val_float,
        },
    ];
    comp_vec_len.push(distortion_vec.len());

    ////////
    // Affinity

    let affinity_vec = vec![
        AffinityModel::NoAffinity,
        AffinityModel::Scale { a1: val_float },
        AffinityModel::Shear { a2: val_float },
        AffinityModel::ScaleAndShear {
            a1: val_float,
            a2: val_float,
        },
    ];
    comp_vec_len.push(affinity_vec.len());

    ////////
    // Covariance for our cameras

    let covariance_vec = vec![
        // proj + no dist + no aff
        DMatrix::<f64>::from_diagonal(&DVector::from_iterator(
            3,
            vec![val_float; 3].into_iter().map(|x: f64| x.powi(2)),
        )),
        // proj + bc + scale
        DMatrix::<f64>::from_diagonal(&DVector::from_iterator(
            9,
            vec![val_float; 9].into_iter().map(|x: f64| x.powi(2)),
        )),
        // proj + kb + shear
        DMatrix::<f64>::from_diagonal(&DVector::from_iterator(
            8,
            vec![val_float; 8].into_iter().map(|x: f64| x.powi(2)),
        )),
        // proj + no dist + scale_and_shear
        DMatrix::<f64>::from_diagonal(&DVector::from_iterator(
            5,
            vec![val_float; 5].into_iter().map(|x: f64| x.powi(2)),
        )),
    ];
    comp_vec_len.push(covariance_vec.len());

    // Make all possible combos of cameras
    let max_len = comp_vec_len.iter().max().unwrap();
    let mut val_uuids = vec![];
    for cam in 0..*max_len {
        let new_uuid = Uuid::new_v4();
        val_uuids.push(new_uuid);
        let component = Component::Camera(Camera::new(
            new_uuid,
            new_uuid,
            val_string.to_string(),
            CameraIntrinsics {
                projection: projection_vec
                    .get(cam % projection_vec.len())
                    .unwrap()
                    .clone(),
                distortion: distortion_vec
                    .get(cam % distortion_vec.len())
                    .unwrap()
                    .clone(),
                affinity: affinity_vec.get(cam % affinity_vec.len()).unwrap().clone(),
                width: val_usize,
                height: val_usize,
            },
            covariance_vec
                .get(cam % covariance_vec.len())
                .unwrap()
                .clone(),
            val_float,
        ));
        builder.add_component(component)?;
    }

    // ~~~~~~~~~~~~
    // Spatial Constraints
    let extrinsics = Extrinsics::from(Isometry3::from_parts(
        Translation3::new(val_float, val_float, val_float),
        UnitQuaternion::identity(),
    ));
    let std_devs = Extrinsics::from(Isometry3::from_parts(
        Translation3::new(val_float, val_float, val_float),
        UnitQuaternion::from_euler_angles(val_float, val_float, val_float),
    ))
    .to_se3();
    let extrinsic_covariance = ExtrinsicCovariance::from(Matrix6::from_diagonal(
        &Vector6::from_iterator(std_devs.into_iter().map(|v| v.powi(2))),
    ));
    let space = SpatialConstraint {
        extrinsics,
        covariance: extrinsic_covariance,
        from: *val_uuids.get(0).unwrap(),
        to: *val_uuids.get(1).unwrap(),
    };
    builder.add_spatial_constraint(space)?;

    // ~~~~~~~~~~~~
    // Temporal Constraints

    let time = TemporalConstraint {
        synchronization: Synchronization {
            offset: val_int64,
            skew: val_int64,
        },
        resolution: val_uint64,
        from: *val_uuids.get(0).unwrap(),
        to: *val_uuids.get(1).unwrap(),
    };
    builder.add_temporal_constraint(time)?;

    // ~~~~~~~~~~~~
    // Output the complete plex into JSON form
    let plex = builder.build();
    let plex_json = serde_json::to_string(&plex)?;

    std::fs::write("plex.json", plex_json).expect("Unable to write file");

    Ok(())
}
