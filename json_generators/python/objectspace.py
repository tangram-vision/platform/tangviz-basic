# Copyright(c) 2022 Tangram Robotics Inc. - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# ----------------------------
# Generated python interface for creating object space JSON

from enum import Enum
from typing import Any, Callable, List, Optional, Type, TypeVar, cast

T = TypeVar("T")
EnumT = TypeVar("EnumT", bound=Enum)


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


def from_float(x: Any) -> float:
    assert isinstance(x, (float, int)) and not isinstance(x, bool)
    return float(x)


def from_int(x: Any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


def to_float(x: Any) -> float:
    assert isinstance(x, float)
    return x


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


def to_enum(c: Type[EnumT], x: Any) -> EnumT:
    assert isinstance(x, c)
    return x.value


def from_none(x: Any) -> Any:
    assert x is None
    return x


def from_union(fs, x):
    for f in fs:
        try:
            return f(x)
        except:
            pass
    assert False


class Target:
    coordinates: List[float]
    id: int
    variances: List[float]

    def __init__(self, coordinates: List[float], id: int, variances: List[float]) -> None:
        self.coordinates = coordinates
        self.id = id
        self.variances = variances

    @staticmethod
    def from_dict(obj: Any) -> "Target":
        assert isinstance(obj, dict)
        coordinates = from_list(from_float, obj.get("coordinates"))
        id = from_int(obj.get("id"))
        variances = from_list(from_float, obj.get("variances"))
        return Target(coordinates, id, variances)

    def to_dict(self) -> dict:
        result: dict = {}
        result["coordinates"] = from_list(to_float, self.coordinates)
        result["id"] = from_int(self.id)
        result["variances"] = from_list(to_float, self.variances)
        return result


class Descriptor:
    targets: List[Target]

    def __init__(self, targets: List[Target]) -> None:
        self.targets = targets

    @staticmethod
    def from_dict(obj: Any) -> "Descriptor":
        assert isinstance(obj, dict)
        targets = from_list(Target.from_dict, obj.get("targets"))
        return Descriptor(targets)

    def to_dict(self) -> dict:
        result: dict = {}
        result["targets"] = from_list(lambda x: to_class(Target, x), self.targets)
        return result


class AprilGridMarkerDictionary(Enum):
    APRILTAG16_H5 = "Apriltag16h5"
    APRILTAG25_H9 = "Apriltag25h9"
    APRILTAG36_H10 = "Apriltag36h10"
    APRILTAG36_H11 = "Apriltag36h11"
    APRILTAG_KALIBR = "ApriltagKalibr"


class AprilGridClass:
    height: int
    marker_dictionary: AprilGridMarkerDictionary
    marker_length: float
    tag_spacing: float
    variances: List[float]
    width: int

    def __init__(
        self,
        height: int,
        marker_dictionary: AprilGridMarkerDictionary,
        marker_length: float,
        tag_spacing: float,
        variances: List[float],
        width: int,
    ) -> None:
        self.height = height
        self.marker_dictionary = marker_dictionary
        self.marker_length = marker_length
        self.tag_spacing = tag_spacing
        self.variances = variances
        self.width = width

    @staticmethod
    def from_dict(obj: Any) -> "AprilGridClass":
        assert isinstance(obj, dict)
        height = from_int(obj.get("height"))
        marker_dictionary = AprilGridMarkerDictionary(obj.get("marker_dictionary"))
        marker_length = from_float(obj.get("marker_length"))
        tag_spacing = from_float(obj.get("tag_spacing"))
        variances = from_list(from_float, obj.get("variances"))
        width = from_int(obj.get("width"))
        return AprilGridClass(
            height, marker_dictionary, marker_length, tag_spacing, variances, width
        )

    def to_dict(self) -> dict:
        result: dict = {}
        result["height"] = from_int(self.height)
        result["marker_dictionary"] = to_enum(AprilGridMarkerDictionary, self.marker_dictionary)
        result["marker_length"] = to_float(self.marker_length)
        result["tag_spacing"] = to_float(self.tag_spacing)
        result["variances"] = from_list(to_float, self.variances)
        result["width"] = from_int(self.width)
        return result


class Checkerboard:
    checker_length: float
    height: int
    variances: List[float]
    width: int

    def __init__(
        self, checker_length: float, height: int, variances: List[float], width: int
    ) -> None:
        self.checker_length = checker_length
        self.height = height
        self.variances = variances
        self.width = width

    @staticmethod
    def from_dict(obj: Any) -> "Checkerboard":
        assert isinstance(obj, dict)
        checker_length = from_float(obj.get("checker_length"))
        height = from_int(obj.get("height"))
        variances = from_list(from_float, obj.get("variances"))
        width = from_int(obj.get("width"))
        return Checkerboard(checker_length, height, variances, width)

    def to_dict(self) -> dict:
        result: dict = {}
        result["checker_length"] = to_float(self.checker_length)
        result["height"] = from_int(self.height)
        result["variances"] = from_list(to_float, self.variances)
        result["width"] = from_int(self.width)
        return result


class MarkerboardMarkerDictionary(Enum):
    APRILTAG16_H5 = "Apriltag16h5"
    APRILTAG25_H9 = "Apriltag25h9"
    APRILTAG36_H10 = "Apriltag36h10"
    APRILTAG36_H11 = "Apriltag36h11"
    APRILTAG_KALIBR = "ApriltagKalibr"
    ARUCO4_X4_100 = "Aruco4x4_100"
    ARUCO4_X4_1000 = "Aruco4x4_1000"
    ARUCO4_X4_250 = "Aruco4x4_250"
    ARUCO4_X4_50 = "Aruco4x4_50"
    ARUCO5_X5_100 = "Aruco5x5_100"
    ARUCO5_X5_1000 = "Aruco5x5_1000"
    ARUCO5_X5_250 = "Aruco5x5_250"
    ARUCO5_X5_50 = "Aruco5x5_50"
    ARUCO6_X6_100 = "Aruco6x6_100"
    ARUCO6_X6_1000 = "Aruco6x6_1000"
    ARUCO6_X6_250 = "Aruco6x6_250"
    ARUCO6_X6_50 = "Aruco6x6_50"
    ARUCO7_X7_100 = "Aruco7x7_100"
    ARUCO7_X7_1000 = "Aruco7x7_1000"
    ARUCO7_X7_250 = "Aruco7x7_250"
    ARUCO7_X7_50 = "Aruco7x7_50"
    ARUCO_ORIGINAL = "ArucoOriginal"


class MarkerboardClass:
    checker_length: float
    height: int
    marker_dictionary: MarkerboardMarkerDictionary
    marker_length: float
    variances: List[float]
    width: int

    def __init__(
        self,
        checker_length: float,
        height: int,
        marker_dictionary: MarkerboardMarkerDictionary,
        marker_length: float,
        variances: List[float],
        width: int,
    ) -> None:
        self.checker_length = checker_length
        self.height = height
        self.marker_dictionary = marker_dictionary
        self.marker_length = marker_length
        self.variances = variances
        self.width = width

    @staticmethod
    def from_dict(obj: Any) -> "MarkerboardClass":
        assert isinstance(obj, dict)
        checker_length = from_float(obj.get("checker_length"))
        height = from_int(obj.get("height"))
        marker_dictionary = MarkerboardMarkerDictionary(obj.get("marker_dictionary"))
        marker_length = from_float(obj.get("marker_length"))
        variances = from_list(from_float, obj.get("variances"))
        width = from_int(obj.get("width"))
        return MarkerboardClass(
            checker_length, height, marker_dictionary, marker_length, variances, width
        )

    def to_dict(self) -> dict:
        result: dict = {}
        result["checker_length"] = to_float(self.checker_length)
        result["height"] = from_int(self.height)
        result["marker_dictionary"] = to_enum(MarkerboardMarkerDictionary, self.marker_dictionary)
        result["marker_length"] = to_float(self.marker_length)
        result["variances"] = from_list(to_float, self.variances)
        result["width"] = from_int(self.width)
        return result


class Markers:
    marker_dictionary: MarkerboardMarkerDictionary
    marker_length: float

    def __init__(
        self, marker_dictionary: MarkerboardMarkerDictionary, marker_length: float
    ) -> None:
        self.marker_dictionary = marker_dictionary
        self.marker_length = marker_length

    @staticmethod
    def from_dict(obj: Any) -> "Markers":
        assert isinstance(obj, dict)
        marker_dictionary = MarkerboardMarkerDictionary(obj.get("marker_dictionary"))
        marker_length = from_float(obj.get("marker_length"))
        return Markers(marker_dictionary, marker_length)

    def to_dict(self) -> dict:
        result: dict = {}
        result["marker_dictionary"] = to_enum(MarkerboardMarkerDictionary, self.marker_dictionary)
        result["marker_length"] = to_float(self.marker_length)
        return result


class Detector:
    april_grid: Optional[AprilGridClass]
    checkerboard: Optional[Checkerboard]
    markerboard: Optional[MarkerboardClass]
    markers: Optional[Markers]

    def __init__(
        self,
        april_grid: Optional[AprilGridClass],
        checkerboard: Optional[Checkerboard],
        markerboard: Optional[MarkerboardClass],
        markers: Optional[Markers],
    ) -> None:
        self.april_grid = april_grid
        self.checkerboard = checkerboard
        self.markerboard = markerboard
        self.markers = markers

    @staticmethod
    def from_dict(obj: Any) -> "Detector":
        assert isinstance(obj, dict)
        april_grid = from_union([AprilGridClass.from_dict, from_none], obj.get("april_grid"))
        checkerboard = from_union([Checkerboard.from_dict, from_none], obj.get("checkerboard"))
        markerboard = from_union([MarkerboardClass.from_dict, from_none], obj.get("markerboard"))
        markers = from_union([Markers.from_dict, from_none], obj.get("markers"))
        return Detector(april_grid, checkerboard, markerboard, markers)

    def to_dict(self) -> dict:
        result: dict = {}
        result["april_grid"] = from_union(
            [lambda x: to_class(AprilGridClass, x), from_none], self.april_grid
        )
        result["checkerboard"] = from_union(
            [lambda x: to_class(Checkerboard, x), from_none], self.checkerboard
        )
        result["markerboard"] = from_union(
            [lambda x: to_class(MarkerboardClass, x), from_none], self.markerboard
        )
        result["markers"] = from_union([lambda x: to_class(Markers, x), from_none], self.markers)
        return result


class Camera:
    descriptor: Optional[Descriptor]
    detector: Detector

    def __init__(self, descriptor: Optional[Descriptor], detector: Detector) -> None:
        self.descriptor = descriptor
        self.detector = detector

    @staticmethod
    def from_dict(obj: Any) -> "Camera":
        assert isinstance(obj, dict)
        descriptor = from_union([Descriptor.from_dict, from_none], obj.get("descriptor"))
        detector = Detector.from_dict(obj.get("detector"))
        return Camera(descriptor, detector)

    def to_dict(self) -> dict:
        result: dict = {}
        result["descriptor"] = from_union(
            [lambda x: to_class(Descriptor, x), from_none], self.descriptor
        )
        result["detector"] = to_class(Detector, self.detector)
        return result


class Objectspace:
    camera: Camera

    def __init__(self, camera: Camera) -> None:
        self.camera = camera

    @staticmethod
    def from_dict(obj: Any) -> "Objectspace":
        assert isinstance(obj, dict)
        camera = Camera.from_dict(obj.get("camera"))
        return Objectspace(camera)

    def to_dict(self) -> dict:
        result: dict = {}
        result["camera"] = to_class(Camera, self.camera)
        return result


def objectspace_from_dict(s: Any) -> Objectspace:
    return Objectspace.from_dict(s)


def objectspace_to_dict(x: Objectspace) -> Any:
    return to_class(Objectspace, x)
