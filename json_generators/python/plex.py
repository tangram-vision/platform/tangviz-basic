# Copyright(c) 2022 Tangram Robotics Inc. - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# ----------------------------
# Generated python interface for creating plex objects and JSON

from typing import Any, Callable, List, Optional, Type, TypeVar, Union, cast
from uuid import UUID

import numpy as np
import numpy.typing as npt

T = TypeVar("T")


def from_float(x: Any) -> float:
    assert isinstance(x, (float, int)) and not isinstance(x, bool)
    return float(x)


def to_float(x: Any) -> float:
    assert isinstance(x, float)
    return x


def from_none(x: Any) -> Any:
    assert x is None
    return x


def from_union(fs, x):
    for f in fs:
        try:
            return f(x)
        except:
            pass
    assert False


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


def from_int(x: Any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_array(mat: Any) -> npt.ArrayLike:
    arr = from_list(lambda x: from_union([lambda x: from_list(to_float, x), from_int], x), mat)
    assert isinstance(arr[0], list)
    assert isinstance(arr[1], int)
    assert isinstance(arr[2], int)
    return np.reshape(np.array(arr[0]), arr[1:])


def to_array(mat: npt.ArrayLike) -> List[Union[List[float], int]]:
    assert isinstance(mat, np.ndarray)
    l = [np.reshape(mat, -1).tolist()]
    l.extend(np.shape(mat))
    return l


class Affinity:
    a1: Optional[float]
    a2: Optional[float]

    def __init__(self, a1: Optional[float], a2: Optional[float]) -> None:
        self.a1 = a1
        self.a2 = a2

    @staticmethod
    def from_dict(obj: Any) -> "Affinity":
        assert isinstance(obj, dict)
        a1 = from_union([from_float, from_none], obj.get("a1"))
        a2 = from_union([from_float, from_none], obj.get("a2"))
        return Affinity(a1, a2)

    def to_dict(self) -> dict:
        result: dict = {}
        result["a1"] = from_union([to_float, from_none], self.a1)
        result["a2"] = from_union([to_float, from_none], self.a2)
        return result


class BrownConrady:
    k1: float
    k2: float
    k3: float
    p1: float
    p2: float

    def __init__(self, k1: float, k2: float, k3: float, p1: float, p2: float) -> None:
        self.k1 = k1
        self.k2 = k2
        self.k3 = k3
        self.p1 = p1
        self.p2 = p2

    @staticmethod
    def from_dict(obj: Any) -> "BrownConrady":
        assert isinstance(obj, dict)
        k1 = from_float(obj.get("k1"))
        k2 = from_float(obj.get("k2"))
        k3 = from_float(obj.get("k3"))
        p1 = from_float(obj.get("p1"))
        p2 = from_float(obj.get("p2"))
        return BrownConrady(k1, k2, k3, p1, p2)

    def to_dict(self) -> dict:
        result: dict = {}
        result["k1"] = to_float(self.k1)
        result["k2"] = to_float(self.k2)
        result["k3"] = to_float(self.k3)
        result["p1"] = to_float(self.p1)
        result["p2"] = to_float(self.p2)
        return result


class KannalaBrandt:
    k1: float
    k2: float
    k3: float
    k4: float

    def __init__(self, k1: float, k2: float, k3: float, k4: float) -> None:
        self.k1 = k1
        self.k2 = k2
        self.k3 = k3
        self.k4 = k4

    @staticmethod
    def from_dict(obj: Any) -> "KannalaBrandt":
        assert isinstance(obj, dict)
        k1 = from_float(obj.get("k1"))
        k2 = from_float(obj.get("k2"))
        k3 = from_float(obj.get("k3"))
        k4 = from_float(obj.get("k4"))
        return KannalaBrandt(k1, k2, k3, k4)

    def to_dict(self) -> dict:
        result: dict = {}
        result["k1"] = to_float(self.k1)
        result["k2"] = to_float(self.k2)
        result["k3"] = to_float(self.k3)
        result["k4"] = to_float(self.k4)
        return result


class Distortion:
    brown_conrady: Optional[BrownConrady]
    kannala_brandt: Optional[KannalaBrandt]

    def __init__(
        self,
        brown_conrady: Optional[BrownConrady],
        kannala_brandt: Optional[KannalaBrandt],
    ) -> None:
        self.brown_conrady = brown_conrady
        self.kannala_brandt = kannala_brandt

    @staticmethod
    def from_dict(obj: Any) -> "Distortion":
        assert isinstance(obj, dict)
        brown_conrady = from_union([BrownConrady.from_dict, from_none], obj.get("brown_conrady"))
        kannala_brandt = from_union([KannalaBrandt.from_dict, from_none], obj.get("kannala_brandt"))
        return Distortion(brown_conrady, kannala_brandt)

    def to_dict(self) -> dict:
        result: dict = {}
        result["brown_conrady"] = from_union(
            [lambda x: to_class(BrownConrady, x), from_none], self.brown_conrady
        )
        result["kannala_brandt"] = from_union(
            [lambda x: to_class(KannalaBrandt, x), from_none], self.kannala_brandt
        )
        return result


class Pinhole:
    cx: float
    cy: float
    f: float

    def __init__(self, cx: float, cy: float, f: float) -> None:
        self.cx = cx
        self.cy = cy
        self.f = f

    @staticmethod
    def from_dict(obj: Any) -> "Pinhole":
        assert isinstance(obj, dict)
        cx = from_float(obj.get("cx"))
        cy = from_float(obj.get("cy"))
        f = from_float(obj.get("f"))
        return Pinhole(cx, cy, f)

    def to_dict(self) -> dict:
        result: dict = {}
        result["cx"] = to_float(self.cx)
        result["cy"] = to_float(self.cy)
        result["f"] = to_float(self.f)
        return result


class Projection:
    pinhole: Pinhole

    def __init__(self, pinhole: Pinhole) -> None:
        self.pinhole = pinhole

    @staticmethod
    def from_dict(obj: Any) -> "Projection":
        assert isinstance(obj, dict)
        pinhole = Pinhole.from_dict(obj.get("pinhole"))
        return Projection(pinhole)

    def to_dict(self) -> dict:
        result: dict = {}
        result["pinhole"] = to_class(Pinhole, self.pinhole)
        return result


class Intrinsics:
    affinity: Optional[Affinity]
    distortion: Optional[Distortion]
    height: int
    projection: Projection
    width: int

    def __init__(
        self,
        affinity: Optional[Affinity],
        distortion: Optional[Distortion],
        height: int,
        projection: Projection,
        width: int,
    ) -> None:
        self.affinity = affinity
        self.distortion = distortion
        self.height = height
        self.projection = projection
        self.width = width

    @staticmethod
    def from_dict(obj: Any) -> "Intrinsics":
        assert isinstance(obj, dict)
        affinity = from_union([Affinity.from_dict, from_none], obj.get("affinity"))
        distortion = from_union([Distortion.from_dict, from_none], obj.get("distortion"))
        height = from_int(obj.get("height"))
        projection = Projection.from_dict(obj.get("projection"))
        width = from_int(obj.get("width"))
        return Intrinsics(affinity, distortion, height, projection, width)

    def to_dict(self) -> dict:
        result: dict = {}
        result["affinity"] = from_union([lambda x: to_class(Affinity, x), from_none], self.affinity)
        result["distortion"] = from_union(
            [lambda x: to_class(Distortion, x), from_none], self.distortion
        )
        result["height"] = from_int(self.height)
        result["projection"] = to_class(Projection, self.projection)
        result["width"] = from_int(self.width)
        return result


class Camera:
    covariance: npt.ArrayLike
    intrinsics: Intrinsics
    name: str
    pixel_pitch: float
    root_uuid: UUID
    uuid: UUID

    def __init__(
        self,
        covariance: npt.ArrayLike,
        intrinsics: Intrinsics,
        name: str,
        pixel_pitch: float,
        root_uuid: UUID,
        uuid: UUID,
    ) -> None:
        self.covariance = covariance
        self.intrinsics = intrinsics
        self.name = name
        self.pixel_pitch = pixel_pitch
        self.root_uuid = root_uuid
        self.uuid = uuid

    @staticmethod
    def from_dict(obj: Any) -> "Camera":
        assert isinstance(obj, dict)
        covariance = from_array(obj.get("covariance"))
        intrinsics = Intrinsics.from_dict(obj.get("intrinsics"))
        name = from_str(obj.get("name"))
        pixel_pitch = from_float(obj.get("pixel_pitch"))
        root_uuid = UUID(obj.get("root_uuid"))
        uuid = UUID(obj.get("uuid"))
        return Camera(covariance, intrinsics, name, pixel_pitch, root_uuid, uuid)

    def to_dict(self) -> dict:
        result: dict = {}
        result["covariance"] = to_array(self.covariance)
        result["intrinsics"] = to_class(Intrinsics, self.intrinsics)
        result["name"] = from_str(self.name)
        result["pixel_pitch"] = to_float(self.pixel_pitch)
        result["root_uuid"] = str(self.root_uuid)
        result["uuid"] = str(self.uuid)
        return result


class Component:
    camera: Camera

    def __init__(self, camera: Camera) -> None:
        self.camera = camera

    @staticmethod
    def from_dict(obj: Any) -> "Component":
        assert isinstance(obj, dict)
        camera = Camera.from_dict(obj.get("camera"))
        return Component(camera)

    def to_dict(self) -> dict:
        result: dict = {}
        result["camera"] = to_class(Camera, self.camera)
        return result


class CovarianceClass:
    raw_se3: npt.ArrayLike

    def __init__(self, raw_se3: npt.ArrayLike) -> None:
        assert np.shape(raw_se3) == (6, 6)
        self.raw_se3 = raw_se3

    @staticmethod
    def from_dict(obj: Any) -> "CovarianceClass":
        assert isinstance(obj, dict)
        raw_se3 = np.array(from_list(from_float, obj.get("raw_se3"))).reshape((6, 6))
        return CovarianceClass(raw_se3)

    def to_dict(self) -> dict:
        result: dict = {}
        result["raw_se3"] = from_list(to_float, self.raw_se3.reshape(-1).tolist())
        return result


class Extrinsics:
    rotation: List[float]
    translation: List[float]

    def __init__(self, rotation: List[float], translation: List[float]) -> None:
        self.rotation = rotation
        self.translation = translation

    @staticmethod
    def from_dict(obj: Any) -> "Extrinsics":
        assert isinstance(obj, dict)
        rotation = from_list(from_float, obj.get("rotation"))
        translation = from_list(from_float, obj.get("translation"))
        return Extrinsics(rotation, translation)

    def to_dict(self) -> dict:
        result: dict = {}
        result["rotation"] = from_list(to_float, self.rotation)
        result["translation"] = from_list(to_float, self.translation)
        return result


class SpatialConstraint:
    covariance: CovarianceClass
    extrinsics: Extrinsics
    spatial_constraint_from: UUID
    to: UUID

    def __init__(
        self,
        covariance: CovarianceClass,
        extrinsics: Extrinsics,
        spatial_constraint_from: UUID,
        to: UUID,
    ) -> None:
        self.covariance = covariance
        self.extrinsics = extrinsics
        self.spatial_constraint_from = spatial_constraint_from
        self.to = to

    @staticmethod
    def from_dict(obj: Any) -> "SpatialConstraint":
        assert isinstance(obj, dict)
        covariance = CovarianceClass.from_dict(obj.get("covariance"))
        extrinsics = Extrinsics.from_dict(obj.get("extrinsics"))
        spatial_constraint_from = UUID(obj.get("from"))
        to = UUID(obj.get("to"))
        return SpatialConstraint(covariance, extrinsics, spatial_constraint_from, to)

    def to_dict(self) -> dict:
        result: dict = {}
        result["covariance"] = to_class(CovarianceClass, self.covariance)
        result["extrinsics"] = to_class(Extrinsics, self.extrinsics)
        result["from"] = str(self.spatial_constraint_from)
        result["to"] = str(self.to)
        return result


class Synchronization:
    offset: int
    skew: int

    def __init__(self, offset: int, skew: int) -> None:
        self.offset = offset
        self.skew = skew

    @staticmethod
    def from_dict(obj: Any) -> "Synchronization":
        assert isinstance(obj, dict)
        offset = from_int(obj.get("offset"))
        skew = from_int(obj.get("skew"))
        return Synchronization(offset, skew)

    def to_dict(self) -> dict:
        result: dict = {}
        result["offset"] = from_int(self.offset)
        result["skew"] = from_int(self.skew)
        return result


class TemporalConstraint:
    temporal_constraint_from: UUID
    resolution: int
    synchronization: Synchronization
    to: UUID

    def __init__(
        self,
        temporal_constraint_from: UUID,
        resolution: int,
        synchronization: Synchronization,
        to: UUID,
    ) -> None:
        self.temporal_constraint_from = temporal_constraint_from
        self.resolution = resolution
        self.synchronization = synchronization
        self.to = to

    @staticmethod
    def from_dict(obj: Any) -> "TemporalConstraint":
        assert isinstance(obj, dict)
        temporal_constraint_from = UUID(obj.get("from"))
        resolution = from_int(obj.get("resolution"))
        synchronization = Synchronization.from_dict(obj.get("synchronization"))
        to = UUID(obj.get("to"))
        return TemporalConstraint(temporal_constraint_from, resolution, synchronization, to)

    def to_dict(self) -> dict:
        result: dict = {}
        result["from"] = str(self.temporal_constraint_from)
        result["resolution"] = from_int(self.resolution)
        result["synchronization"] = to_class(Synchronization, self.synchronization)
        result["to"] = str(self.to)
        return result


class Plex:
    components: List[Component]
    creation_timestamp: int
    spatial_constraints: List[SpatialConstraint]
    temporal_constraints: List[TemporalConstraint]
    uuid: UUID

    def __init__(
        self,
        components: List[Component],
        creation_timestamp: int,
        spatial_constraints: List[SpatialConstraint],
        temporal_constraints: List[TemporalConstraint],
        uuid: UUID,
    ) -> None:
        self.components = components
        self.creation_timestamp = creation_timestamp
        self.spatial_constraints = spatial_constraints
        self.temporal_constraints = temporal_constraints
        self.uuid = uuid

    @staticmethod
    def from_dict(obj: Any) -> "Plex":
        assert isinstance(obj, dict)
        components = from_list(Component.from_dict, obj.get("components"))
        creation_timestamp = from_int(obj.get("creation_timestamp"))
        spatial_constraints = from_list(SpatialConstraint.from_dict, obj.get("spatial_constraints"))
        temporal_constraints = from_list(
            TemporalConstraint.from_dict, obj.get("temporal_constraints")
        )
        uuid = UUID(obj.get("uuid"))
        return Plex(
            components,
            creation_timestamp,
            spatial_constraints,
            temporal_constraints,
            uuid,
        )

    def to_dict(self) -> dict:
        result: dict = {}
        result["components"] = from_list(lambda x: to_class(Component, x), self.components)
        result["creation_timestamp"] = from_int(self.creation_timestamp)
        result["spatial_constraints"] = from_list(
            lambda x: to_class(SpatialConstraint, x), self.spatial_constraints
        )
        result["temporal_constraints"] = from_list(
            lambda x: to_class(TemporalConstraint, x), self.temporal_constraints
        )
        result["uuid"] = str(self.uuid)
        return result


def plex_from_dict(s: Any) -> Plex:
    return Plex.from_dict(s)


def plex_to_dict(x: Plex) -> Any:
    return to_class(Plex, x)
