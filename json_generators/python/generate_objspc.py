# Copyright(c) 2022 Tangram Robotics Inc. - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# ----------------------------
# Sample program demonstrating how to generate your own object space file in
# Python
# NOTE: If you have markers, you can use an image of targets to generate the object space using
# `examples/gen_objspace_markers.rs` For now, we'll just make a few by hand.

import json

import objectspace as tvobj

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     Remove None Values
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Thanks to:
# https://stackoverflow.com/a/60124334


def clean_nones(value):
    """
    Recursively remove all None values from dictionaries and lists, and returns
    the result as a new dictionary or list.
    """
    if isinstance(value, list):
        return [clean_nones(x) for x in value if x is not None]
    elif isinstance(value, dict):
        return {key: clean_nones(val) for key, val in value.items() if val is not None}
    else:
        return value


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Checkerboard/Markerboard/AprilGrid
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# The code below constructs object space based off a checkerboard.
# Note that Markerboards and AprilGrids follow the same Detector-Descriptor
# convention. No Descriptor is needed for these formats.


def create_checkerboard_objspc():
    # In meters
    checker_length = 0.05
    # the number of _inner_ corners on the checkerboard
    width = 5
    height = 9
    # In meters ^ 2
    # Order is [x: left-right, y: up-down, z: forward-back]
    variances = [1e-6, 1e-6, 1e-6]
    checkerboard = tvobj.Checkerboard(checker_length, height, variances, width)
    objspc_checkerboard = tvobj.Objectspace(
        tvobj.Camera(None, tvobj.Detector(None, checkerboard, None, None))
    )

    formatted_objspc_dict = clean_nones(tvobj.objectspace_to_dict(objspc_checkerboard))
    checkerboard_file = open("checkerboardpy.json", "w")
    checkerboard_file.write(json.dumps(formatted_objspc_dict))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Markers
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Example: using 5x5 ArUco tags, each 0.025m a side
#
#  0-----1  4-----5
#  | id0 |  | id1 | with a distance of 0.05m between them in X
#  |     |  |     |
#  3-----2  7-----6


def create_markers_objspc():

    marker_dict = tvobj.MarkerboardMarkerDictionary.ARUCO5_X5_250
    marker_length = 0.025
    markers = tvobj.Markers(marker_dict, marker_length)

    detector = tvobj.Detector(
        None,  # not an aprilgrid...
        None,  # not a checkerboard...
        None,  # not a markerboard...
        markers,
    )

    # Markers take targets as their descriptor. This matches the IDs of the
    # markers with the object space. The Platform uses corners as features, so
    # there are 4 coordinates per target.

    targets = []

    # In meters ^ 2
    # Order is [x: left-right, y: up-down, z: forward-back]
    # We'll make these the same for every point, just for demonstration purposes.
    variances = [1e-6, 1e-6, 1e-6]

    # We order these top-bottom - -> left-right.
    # This matches right corner with the double-loop logic below.
    corner_array = [0, 3, 1, 2]

    marker0_id = 0
    marker1_id = 1
    for x in range(2):
        for y in range(2):
            current_idx = (x * 2) + y
            corner_id_marker0 = (marker0_id * 4) + corner_array[current_idx]
            corner_id_marker1 = (marker1_id * 4) + corner_array[current_idx]
            targets.append(tvobj.Target([x * 0.025, y * 0.025, 0.0], corner_id_marker0, variances))
            targets.append(
                tvobj.Target([x * 0.025 + 0.05, y * 0.025, 0.0], corner_id_marker1, variances)
            )

    objspc_markers = tvobj.Objectspace(tvobj.Camera(tvobj.Descriptor(targets), detector))

    formatted_objspc_dict = clean_nones(tvobj.objectspace_to_dict(objspc_markers))
    markers_file = open("markerspy.json", "w")
    markers_file.write(json.dumps(formatted_objspc_dict))


def main():
    create_checkerboard_objspc()
    create_markers_objspc()


if __name__ == "__main__":
    main()
