# Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# ----------------------------
# Sample program demonstrating how to generate your own Plex file in Python

import json
import time
import uuid

import numpy as np

import plex

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     Remove None Values
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Thanks to:
# https://stackoverflow.com/a/60124334


def clean_nones(value):
    """
    Recursively remove all None values from dictionaries and lists, and returns
    the result as a new dictionary or list.
    """
    if isinstance(value, list):
        return [clean_nones(x) for x in value if x is not None]
    elif isinstance(value, dict):
        return {key: clean_nones(val) for key, val in value.items() if val is not None}
    else:
        return value


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     Camera Alpha - Narrow FOV
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def construct_camera_narrow_fov(width, height, uuid, root_uuid):
    cx = width / 2
    cy = height / 2
    radius = np.sqrt(cx * cx + cy * cy)

    field_of_view = np.deg2rad(90)
    f = radius / np.tan(field_of_view / 2.0)

    c0_projection = plex.Projection(plex.Pinhole(cx, cy, f))
    c0_proj_covariance = [
        100.0,  # standard deviation of f
        25.0,  # standard deviation of cx
        25.0,  # standard deviation of cy
    ]

    c0_distortion = plex.Distortion(plex.BrownConrady(0.0, 0.0, 0.0, 0.0, 0.0), None)
    c0_dist_covariance = [
        1.0,  # standard deviation of k1
        1.0,  # standard deviation of k2
        1.0,  # standard deviation of k3
        1.0,  # standard deviation of p1
        1.0,  # standard deviation of p2
    ]

    c0_affinity = plex.Affinity(0.0, 0.0)
    c0_aff_covariance = [
        0.5,  # standard deviation of a1
        0.5,  # standard deviation of a2
    ]

    c0_intrinsics = plex.Intrinsics(c0_affinity, c0_distortion, height, c0_projection, width)

    # Covariance is always ordered
    # - Projection
    # - Distortion
    # - Affinity
    covariance_diag = c0_proj_covariance + c0_dist_covariance + c0_aff_covariance
    # Convert this from std_dev to variance by squaring it
    covariance_diag = np.square(covariance_diag)
    c0_covariance = np.zeros((len(covariance_diag), len(covariance_diag)), dtype=float)
    np.fill_diagonal(c0_covariance, covariance_diag)

    # Plex uses UUID v4
    c0_name = "alpha"
    c0_pixel_pitch = 1.0

    c0_cam = plex.Camera(c0_covariance, c0_intrinsics, c0_name, c0_pixel_pitch, root_uuid, uuid)
    c0 = plex.Component(c0_cam)
    return c0


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     Camera Beta - Wide FOV
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def construct_camera_wide_fov(width, height, uuid, root_uuid):
    cx = width / 2
    cy = height / 2
    radius = np.sqrt(cx * cx + cy * cy)

    field_of_view = np.deg2rad(120)
    f = radius / np.tan(field_of_view / 2.0)

    c1_projection = plex.Projection(plex.Pinhole(cx, cy, f))
    c1_proj_covariance = [
        100.0,  # standard deviation of f
        25.0,  # standard deviation of cx
        25.0,  # standard deviation of cy
    ]

    c1_distortion = plex.Distortion(None, plex.KannalaBrandt(0.0, 0.0, 0.0, 0.0))
    c1_dist_covariance = [
        1.0,  # standard deviation of k1
        1.0,  # standard deviation of k2
        1.0,  # standard deviation of k3
        1.0,  # standard deviation of k4
    ]

    # No affinity measured this time
    c1_intrinsics = plex.Intrinsics(None, c1_distortion, height, c1_projection, width)

    # Covariance is always ordered
    # - Projection
    # - Distortion
    # - Affinity (of which there is none)
    covariance_diag = c1_proj_covariance + c1_dist_covariance
    covariance_diag = np.square(covariance_diag)
    c1_covariance = np.zeros((len(covariance_diag), len(covariance_diag)), dtype=float)
    np.fill_diagonal(c1_covariance, covariance_diag)

    # Plex uses UUID v4
    c1_name = "beta"
    c1_pixel_pitch = 1.0

    # root uuid points to Alpha
    c1_cam = plex.Camera(c1_covariance, c1_intrinsics, c1_name, c1_pixel_pitch, root_uuid, uuid)
    c1 = plex.Component(c1_cam)

    return c1


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     Camera Alpha < -> Camera Beta Spatial Constraint
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def construct_spatial_constraint(to_uuid, from_uuid):
    # Rotation as a quaternion
    # Format: [x, y, z, w]
    rotation = [0.0, 0.0, 0.0, 1.0]
    # translation in meters
    # convention: (+X: right, +Y: down, +Z: forward)
    translation = [0.02, 0.001, 0.0]
    extrinsics = plex.Extrinsics(rotation, translation)

    # extrinsics covariance is currently in se3 space. This will change soon.
    # For now, these are fine defaults.
    # - translation standard deviation of 15m (basically, we know nothing)
    # - rotation standard deviation of 15 degrees
    extrinsics_covariance = plex.CovarianceClass(
        np.array(
            [
                [209.2163483383551, 0.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, 226.15189405951685, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 239.8072670298675, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.05042861912254395, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.08564778378373221, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 0.05042861912254395],
            ]
        )
    )

    spatial_constraint = plex.SpatialConstraint(
        extrinsics_covariance, extrinsics, from_uuid, to_uuid
    )
    return spatial_constraint


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     Camera Alpha < -> Camera Beta Temporal Constraint
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def construct_temporal_constraint(to_uuid, from_uuid):
    # No offset or skew here
    sync = plex.Synchronization(0, 0)
    # Convert resolution to nanoseconds
    c0_fps = 30
    resolution = int(((1 / c0_fps) / 2) * 1e9)
    temporal_constraint = plex.TemporalConstraint(from_uuid, resolution, sync, to_uuid)
    return temporal_constraint


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     Put our Plex Together!
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def main():
    c0_uuid = uuid.uuid4()
    c1_uuid = uuid.uuid4()
    c0 = construct_camera_narrow_fov(640, 480, c0_uuid, c0_uuid)
    c1 = construct_camera_wide_fov(1280, 960, c1_uuid, c0_uuid)
    spatial_constraint = construct_spatial_constraint(c0_uuid, c1_uuid)
    temporal_constraint = construct_temporal_constraint(c0_uuid, c1_uuid)
    components = [c0, c1]
    creation_timestamp = int(time.time() * 1e9)
    new_plex = plex.Plex(
        components,
        creation_timestamp,
        [spatial_constraint],
        [temporal_constraint],
        uuid.uuid4(),
    )

    formatted_plex_dict = clean_nones(plex.plex_to_dict(new_plex))
    with open("plexpy.json", "w") as fout:
        json.dump(formatted_plex_dict, fout, indent=4, sort_keys=True)


if __name__ == "__main__":
    main()
