# Tangram Vision Platform JSON Generators

These schemas were built for the creation and validation of JSON inputs from other programming
languages. All language interfaces have been generated from the top-level JSON schema files; the
JSON schema files, in turn, are based on the native Rust API.

## Using Quicktype

All language generators are based on the output of
[quicktype](https://github.com/quicktype/quicktype). Note, however, that the language generators
have been hand-modified to improve behavior. Install the quicktype CLI to run the below commands.

## JSON Schema

```shell
quicktype --src plex.json -o plex_schema.json -l schema
```

- The JSON Schema has been hand-tuned after being generated, in order to ensure some Rust
types and logic are preserved when using the generated code for other languages.

## C++

### Dependencies

The C++ generator will automatically download its dependencies which are all header-only.

### Build

```shell
git clone <repo>
cd <repo>/json_generators/cpp
mkdir build && cd build
cmake .. && make && make install
```

### Code Generation

```shell
quicktype --src plex.json -o plex.h -l cpp -s schema \
    --no-boost --code-format with-struct --namespace tangviz
```

### Notes

- This generator currently requires C++17. If you would like to use an earlier version of C++,
regenerate these bindings without the `--no-boost` flag. This will require dependency on
[boost](https://www.boost.org/).

## Python

```shell
pip install -r requirements.txt # For numpy
quicktype --src plex.json -o plex.py -l py -s schema
```

- Supports Python v3.5 and up. 
- Requires numpy; see `requirements.txt`.
- Make sure to prune any `None` values from your generated JSON. See `python/generate_plex.py` for
a function that does the necessary post-processing.
