# CONFIGURATION
# NOTE: `image` describes the docker image that we want this test suite
#       to run on. For many repos with more specialized deps, this is a custom
#       Tangram-hosted instance, e.g.
#       `image: 'registry.gitlab.com/tangram-vision/docker-base'`.
#       If this is the case, make sure to include SSH loading in
#       `before_script`

variables:
  CARGO_HOME: $CI_PROJECT_DIR/cargo

# For advice on caching packages, see:
# https://doc.rust-lang.org/cargo/guide/cargo-home.html#caching-the-cargo-home-in-ci
.paths: &cache_paths
  - cargo/bin/
  - cargo/registry/index/
  - cargo/registry/cache/
  - cargo/git/db/
  - target/

default:
  image: "registry.gitlab.com/tangram-vision/internal/docker-base:latest"
  cache:
    # Only pull the cache, don't push because it takes a while.
    # The build_cache job populates the cache.
    policy: pull
    paths: *cache_paths

stages:
  - test
  - doc
  - build_artifacts

##### ########## ########## ########## ########## #####
# TEST STAGE
# - [PASS REQ] runs all unit tests in the build
# - [PASS REQ] runs a lint checker over every piece of code
# - [PASS REQ] checks for copyright notice at the top of all source files
# - [PASS REQ] confirms that project builds for WASM target
test:
  stage: test
  script:
    - rustc --version
    - cargo --version
    - cargo test --verbose

lint:
  stage: test
  script:
    - rustc --version
    - cargo --version
    - cargo clippy --all-targets --all-features -- -D warnings

security-check:
  stage: test
  variables:
    # Catches and fails on non-zero status
    FF_ENABLE_BASH_EXIT_CODE_CHECK: 1
  script:
    - security_check.sh

# The tangviz-wasm crate depends on this crate to produce WASM modules (e.g. for
# plex validation in the browser), so this CI step ensures that this crate
# remains buildable for the WASM target.
build-wasm:
  stage: test
  before_script:
    - rustup target add wasm32-unknown-unknown
  script:
    - rustc --version
    - cargo --version
    - cargo check --target wasm32-unknown-unknown

##### ########## ########## ########## ########## #####
# DOC STAGE
# - [PASS OPT] checks for documentation on all relevant pieces of code
# - [PASS REQ] checks for matching README and lib.rs documentation
# - [PASS REQ] creates documentation pages
doc-check:
  stage: doc
  script:
    - cargo clippy --all-targets --all-features -- -A clippy::all -D clippy::missing_docs_in_private_items
  allow_failure: true

readme-check:
  stage: doc
  variables:
    # Catches and fails on non-zero status
    FF_ENABLE_BASH_EXIT_CODE_CHECK: 1
  script:
    - readme_check.sh --no-badges --no-title --no-indent-headings

pages:
  stage: doc
  # Run this at the same time as test and lint jobs
  # https://docs.gitlab.com/ee/ci/yaml/#needs
  needs: []
  script:
    - cargo doc --no-deps --document-private-items
    - mkdir public
    - cp -r target/doc/* public
    - LOWERCASE_PROJECT_NAME=$(echo $CI_PROJECT_NAME | awk '{print tolower($0)}') && echo "<meta http-equiv=refresh content=0;url=$LOWERCASE_PROJECT_NAME>" > public/index.html
  artifacts:
    paths:
      - public
  only:
    - main

##### ########## ########## ########## ########## #####
# CACHE STAGE
# - [PASS REQ] creates a build cache for future builds. This frees up time
#   during build cycles, and is essential when running on DigitalOcean
#   or another like service.
# - [PASS REQ] update our badges on the project page by
#   creating new SVG artifacts in the ci/ folder.
build_cache:
  stage: build_artifacts
  script:
    - rustc --version
    - cargo --version
    # Build dev-dependencies too
    - cargo test --no-run
    # du commands are for keeping an eye on cache size
    # We can remove them if we're confident the cache size is sane.
    - du -h -d3 cargo || true
    - du -h -d2 target || true

    # cargo check/clippy does weird caching things, so don't try to cache it
    # https://github.com/rust-lang/rust-clippy/issues/4612
    # - cargo clippy --all-targets --all-features -- -D warnings
  cache:
    # Push all cargo dependencies and compilation artifacts.
    policy: push
    paths: *cache_paths
  only:
    - main

build_badges:
  stage: build_artifacts
  script:
    - rustc --version
    - cargo --version
    - tangram-badge-generator -d $CI_PROJECT_DIR
  artifacts:
    paths:
      - ./ci/*.svg
    when: always
    expire_in: 4 weeks
  only:
    - main
