// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! # Tangram Vision Basic
//!
//! Tangram Vision Basic represents the core types and functions that make Tangram software
//! tick.
//!
//! ## Features
//!
//! - `proptest-support`: Enables `arbitrary` modules within the crate. These are used for writing
//! property-based tests with the `proptest` crate.
//!
//! ## Modules
//!
//! ### Base
//!
//! Base holds basic types that are useful across Tangram Software. This includes different
//! component model descriptions, mathematical necessities, etc.
//!
//! ### Plex
//!
//! Plexes are a fundamental representation of a multi-sensor system. They describe the
//! modeling, registration, and synchronization relationships between unit components in a
//! multi-sensor system.

/// Proc macros re-exported from `tangviz_basic_macros`.
pub mod macros {
    pub use tangviz_basic_macros::{Encodable, Sameness};
}

pub mod base;
pub mod plex;

pub mod encoding;
pub mod sameness;

/// Module containing the various traits exported by this crate.
pub mod prelude {
    pub use crate::encoding::*;
    pub use crate::sameness::Sameness;
}

// It makes sense for us to export this. We rely on this in our public interfaces (namely,
// SpatialConstraint), and users may not want to version match this repo if it's being used under
// the hood and they're making plexes.
pub use nalgebra;
