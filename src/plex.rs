// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! The relational graphs connecting all components through space and time.
//!
//! Plexes are relational graphs connecting all components through space and time.
//! They are undirected, acyclic graphs with Components as nodes and Relations
//! as edges. They serve to connect sensor suites in three distinct ways:
//!
//! - Modeling: Component-based. A plex is modeled _iff_ a complete model
//!   descriptor exists for every component in the graph.
//! - Registration: Relation-based. A plex is registered _iff_ for every
//!   pair of components m and n in the graph, there exists a unique series of
//!   spatial constraints that connects the pair.
//! - Synchronization: Relation-based. A plex is synchronized _iff_ for
//!   every pair of components m and n in the graph, there exists a unique
//!   series of temporal constraints that connects the pair.
//!
//! A plex is *calibrated* if it is fully modeled, registered, and synchronized.
//! See [the documentation](https://www.notion.so/tangramvision/Plexes-5dedf69dd9da40978f428ea544f6634c)
//! for more.

mod builder;
mod component;
mod plex_type;
mod relation;

pub use builder::{PlexBuilder, PlexConstructionError};
pub use component::{Anchor, Camera, Component, Lidar, PlexComponent};
pub use plex_type::{read_plex, Plex};
pub use relation::{Relation, SpatialConstraint, Synchronization, TemporalConstraint};
