// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! Types and helpers for writing tests that span the whole repo (as opposed to a single source or
//! module).

use nalgebra as na;
use std::collections::{BTreeMap, BTreeSet, HashMap, HashSet};
use uuid::Uuid;

//#region Trait declarations

/// A trait for specifying 'sameness' between two instances of a type.
pub trait Sameness {
    /// Predicate to evaluate if two types that implement Serializable are the same, i.e. that they
    /// represent the same logical data.
    ///
    /// Sameness is distinct from equality, in which two values can be the same but not equal (e.g.
    /// NaN).
    ///
    /// This is primarily useful in testing serialization more than it is in general code, as we
    /// need to be able to evaluate generally that two values are the same, even if they aren't
    /// equal.
    fn is_same(&self, rhs: &Self) -> bool;
}

//#endregion

/// Quick macro for implementing Sameness on (numeric) primitives.
///
/// This should not be used outside of the module it is defined in.
macro_rules! impl_sameness_prim {
    ($($t:ty),*) => {
        $(
            impl Sameness for $t {
                fn is_same(&self, rhs: &Self) -> bool {
                    // We use byte equality because floating point numbers are "the same" if they
                    // use the same bytes in their representation.
                    //
                    // This avoids having to compare e.g. NAN and NAN, since we really only care
                    // use this to evaluate if two floats serialized / deserialized the same
                    // round-trip.
                    self.to_ne_bytes() == rhs.to_ne_bytes()
                }
            }
        )*
    }
}

impl_sameness_prim!(i8, u8, i16, u16, i32, u32, i64, u64, i128, u128, usize, isize, f32, f64);

impl Sameness for bool {
    fn is_same(&self, rhs: &Self) -> bool {
        self == rhs
    }
}

impl Sameness for String {
    fn is_same(&self, rhs: &Self) -> bool {
        self == rhs
    }
}

impl<T, const N: usize> Sameness for [T; N]
where
    T: Sameness,
{
    fn is_same(&self, rhs: &Self) -> bool {
        self.iter().zip(rhs.iter()).all(|(l, r)| l.is_same(r))
    }
}

impl<T> Sameness for &[T]
where
    T: Sameness,
{
    fn is_same(&self, rhs: &Self) -> bool {
        self.iter().zip(rhs.iter()).all(|(l, r)| l.is_same(r))
    }
}

impl<T> Sameness for Vec<T>
where
    T: Sameness,
{
    fn is_same(&self, rhs: &Self) -> bool {
        self.iter().zip(rhs.iter()).all(|(l, r)| l.is_same(r))
    }
}

impl<K, V, S> Sameness for HashMap<K, V, S>
where
    K: Sameness,
    V: Sameness,
{
    fn is_same(&self, rhs: &Self) -> bool {
        self.iter()
            .zip(rhs.iter())
            .all(|((lk, lv), (rk, rv))| lk.is_same(rk) && lv.is_same(rv))
    }
}

impl<T, S> Sameness for HashSet<T, S>
where
    T: Sameness,
{
    fn is_same(&self, rhs: &Self) -> bool {
        self.iter().zip(rhs.iter()).all(|(l, r)| l.is_same(r))
    }
}

impl<K, V> Sameness for BTreeMap<K, V>
where
    K: Sameness,
    V: Sameness,
{
    fn is_same(&self, rhs: &Self) -> bool {
        self.iter()
            .zip(rhs.iter())
            .all(|((lk, lv), (rk, rv))| lk.is_same(rk) && lv.is_same(rv))
    }
}

impl<T> Sameness for BTreeSet<T>
where
    T: Sameness,
{
    fn is_same(&self, rhs: &Self) -> bool {
        self.iter().zip(rhs.iter()).all(|(l, r)| l.is_same(r))
    }
}

impl<T, R, C, S> Sameness for na::Matrix<T, R, C, S>
where
    T: Sameness,
    R: na::Dim,
    C: na::Dim,
    S: na::RawStorage<T, R, C>,
{
    fn is_same(&self, rhs: &Self) -> bool {
        self.nrows().is_same(&rhs.nrows())
            && self.ncols().is_same(&rhs.ncols())
            && self.iter().zip(rhs.iter()).all(|(l, r)| l.is_same(r))
    }
}

impl Sameness for Uuid {
    fn is_same(&self, rhs: &Self) -> bool {
        self.as_bytes() == rhs.as_bytes()
    }
}
