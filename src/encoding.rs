// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! Traits for encoding and decoding types into / from a `Vec<f64>` or contiguous collection of
//! `f64`s.

use nalgebra as na;

/// A trait denoting any type that can be encoded into a `Vec<f64>`.
pub trait Encodable {
    /// The expected number of elements in the final `Vec<f64>`.
    fn encoded_size(&self) -> usize;

    /// Constructs a decoder that can decode a set of encoded parameters for the specific variant
    /// of this type.
    fn decoder(&self) -> Box<dyn Decoder<Self>>;

    /// Encodes the type as a `Vec<f64>`, of size `Self::size()`, and returns that vector.
    fn encode(&self) -> Vec<f64>;
}

/// A trait denoting any type that can decode a `&[f64]` into some generic type `T`.
pub trait Decoder<T> {
    /// The expected size or number of parameters that this decoder expects to work on.
    fn size(&self) -> usize;

    /// Decodes the `parameters` into some type `T`.
    ///
    /// Returns `None` if the number of `parameters` passed in via the slice does not match the
    /// `Self::size()` of parameters to decode.
    fn decode(&self, parameters: &[f64]) -> Option<T>;
}

impl Encodable for f64 {
    fn encoded_size(&self) -> usize {
        1
    }

    fn decoder(&self) -> Box<dyn Decoder<Self>> {
        Box::new(F64Decoder)
    }

    fn encode(&self) -> Vec<f64> {
        vec![*self]
    }
}

/// Private dummy struct for decoding a single `f64`.
struct F64Decoder;

impl Decoder<f64> for F64Decoder {
    #[inline(always)]
    fn size(&self) -> usize {
        1
    }

    #[inline(always)]
    fn decode(&self, parameters: &[f64]) -> Option<f64> {
        if parameters.len() == 1 {
            Some(parameters[0])
        } else {
            None
        }
    }
}

impl<T> Encodable for Vec<T>
where
    T: Encodable + 'static,
{
    fn encoded_size(&self) -> usize {
        self.iter().fold(0, |k, t| k + t.encoded_size())
    }

    fn decoder(&self) -> Box<dyn Decoder<Self>> {
        Box::new(VecDecoder(self.iter().map(|t| t.decoder()).collect()))
    }

    fn encode(&self) -> Vec<f64> {
        // Incredibly wasteful if you start with a Vec<f64>, but little we can do here otherwise???
        //
        // I'm not sure if there's a clean way to make this _not_ work for f64 and to just return a
        // copy of the vector in that case.
        let mut encoded = Vec::with_capacity(self.encoded_size());

        for t in self.iter() {
            encoded.append(&mut t.encode());
        }

        encoded
    }
}

/// Private dummy struct for decoding a `Vec<T>`.
struct VecDecoder<T>(Vec<Box<dyn Decoder<T>>>);

impl<T> Decoder<Vec<T>> for VecDecoder<T> {
    fn size(&self) -> usize {
        self.0.iter().fold(0, |k, d| k + d.size())
    }

    fn decode(&self, parameters: &[f64]) -> Option<Vec<T>> {
        if parameters.len() == self.size() {
            let decoded = Vec::with_capacity(self.0.len());

            let (decoded, _) = self.0.iter().fold((decoded, parameters), |k, d| {
                let (mut decoded, parameters) = k;
                let t = d.decode(&parameters[0..d.size()]).unwrap();
                decoded.push(t);
                let parameters = &parameters[d.size()..];

                (decoded, parameters)
            });

            Some(decoded)
        } else {
            None
        }
    }
}

impl Encodable for na::Point3<f64> {
    #[inline(always)]
    fn encoded_size(&self) -> usize {
        3
    }

    fn decoder(&self) -> Box<dyn Decoder<Self>> {
        Box::new(Point3Decoder)
    }

    fn encode(&self) -> Vec<f64> {
        self.coords.as_slice().to_vec()
    }
}

/// Private dummy struct for decoding a `na::Point3<f64>`
struct Point3Decoder;

impl Decoder<na::Point3<f64>> for Point3Decoder {
    #[inline(always)]
    fn size(&self) -> usize {
        3
    }

    fn decode(&self, parameters: &[f64]) -> Option<na::Point3<f64>> {
        if parameters.len() == self.size() {
            Some(na::Point3::new(parameters[0], parameters[1], parameters[2]))
        } else {
            None
        }
    }
}

impl<R, C> Encodable for na::OMatrix<f64, R, C>
where
    R: na::Dim,
    C: na::Dim,
    na::DefaultAllocator: nalgebra::allocator::Allocator<f64, R, C>,
{
    fn encoded_size(&self) -> usize {
        self.len()
    }

    fn decoder(&self) -> Box<dyn Decoder<Self>> {
        Box::new(OMatrixDecoder {
            nrows: R::from_usize(self.nrows()),
            ncols: C::from_usize(self.ncols()),
        })
    }

    fn encode(&self) -> Vec<f64> {
        self.as_slice().to_vec()
    }
}

/// Private dummy struct for decoding `na::OMatrix` types.
struct OMatrixDecoder<R: na::Dim, C: na::Dim> {
    /// Number of rows in the original matrix
    nrows: R,
    /// Number of columns in the original matrix
    ncols: C,
}

impl<R, C> Decoder<na::OMatrix<f64, R, C>> for OMatrixDecoder<R, C>
where
    R: na::Dim,
    C: na::Dim,
    na::DefaultAllocator: nalgebra::allocator::Allocator<f64, R, C>,
{
    #[inline(always)]
    fn size(&self) -> usize {
        self.nrows.value() * self.ncols.value()
    }

    fn decode(&self, parameters: &[f64]) -> Option<na::OMatrix<f64, R, C>> {
        if parameters.len() == self.size() {
            Some(na::OMatrix::from_column_slice_generic(
                self.nrows, self.ncols, parameters,
            ))
        } else {
            None
        }
    }
}
