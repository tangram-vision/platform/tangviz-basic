// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! Types for describing extrinsics between two components.
//!
//! Though extrinsics are stored as an SE3 Lie group, they are (de)serialized as Isometry3
//! {position, quaternion}. This is both due to human readability (though quaternions are difficult
//! to parse for most) and native use in other programs e.g. rendering processes. There is a small
//! bit of precision lost everytime this conversion takes place as a result.

use crate::prelude::{Decoder, Encodable, Sameness};
use nalgebra::{
    Isometry3, Matrix, Matrix3, Matrix4, Matrix6, Quaternion, Rotation3, Translation3,
    UnitQuaternion, Vector6,
};
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use tangviz_basic_macros::{Encodable, Sameness};

//#region

/// Rotation in radians below which we assume there is no rotation to avoid dividing by zero.
///
/// The choice of 5 * 10^-6 is because this represents approximately 1 arc-second of rotation. We
/// only seek to incorporate rotations that are at least whole arc-seconds, so anything less than
/// this is considered to be insignificant.
const ROTATION_EPS: f64 = 5e-6;

//#endregion

//#region Type Declarations

/// Type holding extrinsic parameters (extrinsics) that represent the transformation between two
/// coordinate frames (components).
#[derive(Clone, Debug, Encodable)]
pub struct Extrinsics {
    /// Internal representation of extrinsics.
    ///
    /// We hold onto the se3 group because it's used internally for most optimizations, is more
    /// accurate, and takes less space than most conventions.
    raw_se3: Vector6<f64>,
}

impl Serialize for Extrinsics {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        self.to_isometry().serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for Extrinsics {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        Ok(Extrinsics::from(Isometry3::deserialize(deserializer)?))
    }
}

/// Comparison epsilon for sameness relation
const EPSILON: f64 = 1e-8;

impl Sameness for Extrinsics {
    fn is_same(&self, rhs: &Self) -> bool {
        // There is some loss in precision between serialization and deserialization:
        // Serialize: SE3 -> Isometry
        // Deserialize: Isometry -> SE3
        self.to_se3()
            .iter()
            .zip(rhs.to_se3().iter())
            .all(|(l, r)| (l - r).abs() < EPSILON)
    }
}

/// Type describing the covariance between extrinsics.
#[derive(Clone, Debug, Deserialize, Serialize, Sameness)]
pub struct ExtrinsicCovariance {
    /// Internal representation of the extrinsic covariance.
    ///
    /// Initially stored as a 6x6 representing the covariance of an se3 group to match the
    /// [`Extrinsics`] type.
    raw_se3: Matrix6<f64>,
}

//#endregion

//#region Extrinsics impl

impl From<&Isometry3<f64>> for Extrinsics {
    /// Constructs a set of extrinsics from an isometry.
    ///
    /// Extrinsics are stored internally with an se3 group, so this function effectively acts as
    /// the log-map between SE3 and se3.
    fn from(isometry: &Isometry3<f64>) -> Self {
        // The adjustment for sign is suggested by A Micro Lie
        // Theory's appendix to account for Quaternion double cover.
        let quat = {
            let quat = isometry.rotation;
            if quat.scalar() <= 0.0 {
                -1.0 * quat.quaternion()
            } else {
                *quat.quaternion()
            }
        };

        let sin_half_theta_squared = quat.vector().norm_squared();
        let sin_half_theta = sin_half_theta_squared.sqrt();
        let cos_half_theta = quat.scalar();

        // tan(x/2) = sin(x/2) / cos(x/2)
        // atan(tan(x/2)) = atan(sin(x/2) / cos(x/2))
        // x/2 = atan(sin(x/2) / cos(x/2))
        let half_theta = sin_half_theta.atan2(cos_half_theta);
        let theta = half_theta * 2.0;

        // This epsilon was chosen empirically and is specific to f64.
        // For numbers smaller than the epsilon, the analytical scale
        // factor formula will round to 2.0 and eventually produce NaNs
        // near the machine limit (around 1e^-307)
        let axis_scale = if theta > 1e-16 {
            theta / sin_half_theta
        } else {
            2.0
        };

        let omega = quat.vector() * axis_scale;

        let trans_coeff = if theta > 1e-7 {
            (1.0 - half_theta * cos_half_theta / sin_half_theta) / (theta * theta)
        } else {
            // from L'Hopital's rule
            4.0 / 48.0
        };
        let w_x = Matrix::cross_matrix(&omega);
        let v_inv = Matrix3::identity() - (w_x * 0.5) + (w_x * w_x * trans_coeff);

        let t = isometry.translation.vector;
        let mut raw_se3 = Vector6::zeros();
        raw_se3
            .fixed_slice_mut::<3, 1>(0, 0)
            .copy_from(&(v_inv * t));
        raw_se3.fixed_slice_mut::<3, 1>(3, 0).copy_from(&omega);

        Self { raw_se3 }
    }
}

impl From<Isometry3<f64>> for Extrinsics {
    /// Constructs a set of extrinsics from an isometry.
    ///
    /// Extrinsics are stored internally with an se3 group, so this function effectively acts as
    /// the log-map between SE3 and se3.
    fn from(isometry: Isometry3<f64>) -> Self {
        Self::from(&isometry)
    }
}

impl From<Vector6<f64>> for Extrinsics {
    /// Constructs a set of extrinsics from an existing se3 group.
    ///
    /// NOTE: We do not check to guarantee the vector itself contains valid values, so use of this
    /// is at the callers discretion, as it may not output a valid set of Extrinsics.
    fn from(se3_group: Vector6<f64>) -> Self {
        Extrinsics { raw_se3: se3_group }
    }
}

impl Extrinsics {
    /// Constructs a set of extrinsics given a translation and rotation component.
    pub fn from_parts(translation: Translation3<f64>, rotation: Rotation3<f64>) -> Self {
        let isometry = Isometry3::from_parts(translation, UnitQuaternion::from(rotation));
        Self::from(&isometry)
    }

    /// Constructs a 4x4 homogeneous transformation matrix describing the extrinsic.
    ///
    /// Can be thought of as one form of the exponential map of the se3 group describing these extrinsics.
    #[allow(clippy::many_single_char_names)]
    pub fn to_homogeneous(&self) -> Matrix4<f64> {
        let u = self.raw_se3.fixed_slice::<3, 1>(0, 0);
        let omega = self.raw_se3.fixed_slice::<3, 1>(3, 0);

        let w_x = Matrix::cross_matrix(&omega);
        let theta = omega.norm();

        let a = theta.sin() / theta;
        let b = (1.0 - theta.cos()) / theta.powi(2);
        let c = (theta - theta.sin()) / theta.powi(3);

        let mut gamma = Matrix4::zeros();

        let mut rotation = Matrix3::identity();
        let mut v = Matrix3::identity();

        if theta > ROTATION_EPS {
            rotation += (a * w_x) + (b * w_x * w_x);
            v += (b * w_x) + (c * w_x * w_x);
        }

        let translation = v * u;

        gamma.fixed_slice_mut::<3, 3>(0, 0).copy_from(&rotation);
        gamma.fixed_slice_mut::<3, 1>(0, 3).copy_from(&translation);
        gamma[(3, 3)] = 1.0;

        gamma
    }

    /// Gets a copy of the 6-vector that describes the underlying se3 group for these extrinsics.
    pub fn to_se3(&self) -> Vector6<f64> {
        self.raw_se3
    }

    /// Constructs a 3D isometry describing the extrinsic.
    ///
    /// Can be thought of as one form of the exponential map of the se3 group describing these extrinsics.
    pub fn to_isometry(&self) -> Isometry3<f64> {
        let u = self.raw_se3.fixed_slice::<3, 1>(0, 0);
        let omega = self.raw_se3.fixed_slice::<3, 1>(3, 0);
        let theta_sq = omega.norm_squared();
        let theta = theta_sq.sqrt();
        let half_theta = theta * 0.5f64;
        let half_theta_sin = half_theta.sin();
        let half_theta_cos = half_theta.cos();

        // This epsilon was chosen empirically and is specific to f64.
        // For numbers smaller than the epsilon, the analytical scale
        // factor formula will round to 0.5 and eventually produce NaNs
        // near the machine limit (around 1e^-307)
        let axis_scale = if theta > 1e-8 {
            half_theta_sin / theta
        } else {
            // From L'Hopitals rule
            0.5
        };
        let quat_axis = omega * axis_scale;

        let quat = UnitQuaternion::from_quaternion(Quaternion::new(
            half_theta_cos,
            quat_axis.x,
            quat_axis.y,
            quat_axis.z,
        ));

        // This epsilon was chosen empirically and is specific to f64.
        // The analytical formula for b will round to 0.5 starting at
        // the epsilon and eventually produce NaNs at 1e^-160
        let b = if theta > 1e-8 {
            2.0 * (half_theta_sin.powi(2)) / theta.powi(2)
        } else {
            // From L'Hopitals rule applied 2x
            0.5
        };

        // This epsilon was chosen empirically and is specific to f64.
        // The analytical formula becomes less accurate than the approximation
        // starting at the epsilon. By 1e-7, the formula has an error of 0.1 and then starts
        // unpredicably rounding to zero, producing large values, NaNs etc.
        let c = if theta > 1e-2 {
            (theta - theta.sin()) / theta.powi(3)
        } else {
            // First two terms of the Taylor series expansion of (theta - theta.sin()) / theta.powi(3)
            (1.0 / 6.0) - theta_sq / 120.0
        };
        let w_x = Matrix::cross_matrix(&omega);
        let v = Matrix3::identity() + (b * w_x) + (c * w_x * w_x);

        let trans = Translation3::from(v * u);
        Isometry3::from_parts(trans, quat)
    }

    /// Produces a new set of extrinsics that describe the inverse isometry to the current
    /// extrinsic.
    pub fn inverse(&self) -> Self {
        Self {
            raw_se3: self.raw_se3 * -1.0f64,
        }
    }

    /// Gets the adjoint matrix for this extrinsic.
    ///
    /// When applied, the adjoint transforms a tangent vector from one tangent space into the
    /// tangent space of the extrinsics that the adjoint was derived from. What this means is that
    /// we can use the adjoint matrix to compose covariances. It is also used in various jacobian
    /// expressions.
    ///
    /// See the documentation of [`ExtrinsicCovariance::compose`] for more details.
    pub fn adjoint(&self) -> Matrix6<f64> {
        let m = self.to_homogeneous();
        let rot = m.fixed_slice::<3, 3>(0, 0);

        let t_x_rot = Matrix::cross_matrix(&m.fixed_slice::<3, 1>(0, 3)) * rot;

        let mut adj = Matrix6::zeros();

        // Adj =
        // [ R [t]xR ]
        // [ 0   R   ]
        adj.fixed_slice_mut::<3, 3>(0, 0).copy_from(&rot);
        adj.fixed_slice_mut::<3, 3>(0, 3).copy_from(&t_x_rot);
        adj.fixed_slice_mut::<3, 3>(3, 3).copy_from(&rot);

        adj
    }

    /// Composes two sets of extrinsics together to get a combined transform.
    ///
    /// If you have the B &larr; A extrinsics, as well as the C &larr; B extrinsics, you can get C
    /// &larr; A by performing:
    ///
    /// ```no_run
    /// # use tangviz_basic::base::extrinsics::Extrinsics;
    /// # fn get_c_from_a(c_from_b: Extrinsics, b_from_a: Extrinsics) {
    /// let c_from_a = c_from_b.compose(&b_from_a);
    /// # }
    /// ```
    pub fn compose(&self, rhs: &Self) -> Self {
        Extrinsics::from(self.to_isometry() * rhs.to_isometry())
    }
}

//#endregion

//#region ExtrinsicCovariance impl

impl From<Matrix6<f64>> for ExtrinsicCovariance {
    fn from(raw_se3: Matrix6<f64>) -> Self {
        Self::new(raw_se3)
    }
}

impl ExtrinsicCovariance {
    /// Constructs a new set of extrinsic covariances.
    ///
    /// The covariance is a 6x6 matrix which describes covariance for an se3 group [u, w] that
    /// describes the extrinsics between two components. In this formulation, the first 3 columns
    /// and rows of the matrix correspond to `u` in se3 (translation), whereas the latter 3 columns
    /// and rows correspond to `w` in se3 (rotation).
    pub fn new(raw_se3: Matrix6<f64>) -> Self {
        Self { raw_se3 }
    }

    /// Composes the covariance of two extrinsics together to produce the covariance of the
    /// composed transform.
    ///
    /// This is derived by performing error propagation over extrinsic composition. See equation
    /// (54) in [this paper] for details on how we project the covariance of the `rhs` extrinsic
    /// into the tangent space of `extrinsics`.
    ///
    /// [this paper]: https://arxiv.org/pdf/1812.01537.pdf.
    pub fn compose(&self, extrinsics: &Extrinsics, rhs: &Self) -> Self {
        let adj = extrinsics.adjoint();

        Self {
            raw_se3: self.raw_se3 + adj * rhs.raw_se3 * adj.transpose(),
        }
    }

    /// Produces the covariance matrix for extrinsics in se3 space.
    ///
    /// Rows and columns are sorted according to [u, w], where `u` is the translation component of
    /// the se3 group, and `w` is the rotation component.
    pub fn to_se3(&self) -> Matrix6<f64> {
        self.raw_se3
    }
}

//#endregion

//#region Tests

/// Module containing proptest-specific "arbitrary" functions, which provide arbitrary / randomized
/// data for property-based testing.
#[cfg(any(test, feature = "proptest-support"))]
pub mod arbitrary {
    use super::*;
    use nalgebra as na;
    use proptest::prelude::*;
    use std::f64::consts::PI;

    // The following arb_xxx functions are to compensate for the fact that nalgebra doesn't export
    // proptest strategies / `Arbitrary` impls for these types.
    prop_compose! {
        pub fn arb_translation_with(
            x_strategy: impl Strategy<Value = f64>,
            y_strategy: impl Strategy<Value = f64>,
            z_strategy: impl Strategy<Value = f64>,
        )
            (x in x_strategy, y in y_strategy, z in z_strategy)
        -> na::Translation3<f64> {
            na::Translation3::from(na::Vector3::new(x, y, z))
        }
    }

    prop_compose! {
        pub fn arb_translation()
            (
                translation in arb_translation_with(
                    -100.0..=100.0,
                    -100.0..=100.0,
                    -100.0..=100.0
                )
            )
        -> na::Translation3<f64>
        {
            translation
        }
    }

    prop_compose! {
        pub fn arb_rotation_with(
            roll_strategy: impl Strategy<Value = f64>,
            pitch_strategy: impl Strategy<Value = f64>,
            yaw_strategy: impl Strategy<Value = f64>,
        )
            (
                roll in roll_strategy,
                pitch in pitch_strategy,
                yaw in yaw_strategy,
            )
        -> na::UnitQuaternion<f64>
        {
            na::UnitQuaternion::from_euler_angles(roll, pitch, yaw)
        }
    }

    prop_compose! {
        pub fn arb_rotation()
            (rotation in arb_rotation_with(-PI..PI, -PI..PI, -PI..PI))
        -> na::UnitQuaternion<f64>
        {
            rotation
        }
    }

    prop_compose! {
        pub fn arb_isometry3_with(
            translation_strategy: impl Strategy<Value = na::Translation3<f64>>,
            rotation_strategy: impl Strategy<Value = na::UnitQuaternion<f64>>,
        )
            (
                t in translation_strategy,
                r in rotation_strategy,
            )
        -> na::Isometry3<f64>
        {
            na::Isometry::from_parts(t, r)
        }
    }

    prop_compose! {
        pub fn arb_isometry3()
            (isometry in arb_isometry3_with(arb_translation(), arb_rotation()))
        -> na::Isometry3<f64>
        {
            isometry
        }
    }

    impl Arbitrary for Extrinsics {
        type Strategy = BoxedStrategy<Self>;
        #[allow(clippy::type_complexity)]
        type Parameters = (
            Option<std::ops::RangeInclusive<f64>>, // x translation
            Option<std::ops::RangeInclusive<f64>>, // y translation
            Option<std::ops::RangeInclusive<f64>>, // z translation
            Option<std::ops::RangeInclusive<f64>>, // x (roll) rotation
            Option<std::ops::RangeInclusive<f64>>, // y (pitch) rotation
            Option<std::ops::RangeInclusive<f64>>, // z (yaw) rotation
        );

        fn arbitrary_with(args: Self::Parameters) -> Self::Strategy {
            let (x, y, z, roll, pitch, yaw) = args;

            let x_strategy = if let Some(s) = x { s } else { -100.0..=100.0 };
            let y_strategy = if let Some(s) = y { s } else { -100.0..=100.0 };
            let z_strategy = if let Some(s) = z { s } else { -100.0..=100.0 };

            let roll_strategy = if let Some(s) = roll { s } else { -PI..=PI };
            let pitch_strategy = if let Some(s) = pitch { s } else { -PI..=PI };
            let yaw_strategy = if let Some(s) = yaw { s } else { -PI..=PI };

            arb_isometry3_with(
                arb_translation_with(x_strategy, y_strategy, z_strategy),
                arb_rotation_with(roll_strategy, pitch_strategy, yaw_strategy),
            )
            .prop_map(Extrinsics::from)
            .boxed()
        }
    }

    impl Arbitrary for ExtrinsicCovariance {
        type Strategy = BoxedStrategy<Self>;
        type Parameters = ();

        fn arbitrary_with(_args: Self::Parameters) -> Self::Strategy {
            proptest::collection::vec(
                proptest::num::f64::POSITIVE | proptest::num::f64::ZERO,
                proptest::sample::size_range(36..=36),
            )
            .prop_map(|v| Matrix6::from_iterator(v).symmetric_part().into())
            .boxed()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::prelude::*;
    use approx::assert_relative_eq;
    use proptest::prelude::*;

    proptest! {
        #[test]
        fn extrinsics_inversion(isometry in arbitrary::arb_isometry3()) {
            let extrinsics = Extrinsics::from(isometry);
            let se3_inverse = extrinsics.inverse();
            let group_inverse = isometry.inverse();
            let group_from_se3_inverse = se3_inverse.to_isometry();
            assert_relative_eq!(
                group_inverse,
                group_from_se3_inverse,
                epsilon = EPSILON,
            );
        }

        #[test]
        fn extrinsics_compose(isometry1 in arbitrary::arb_isometry3(), isometry2 in arbitrary::arb_isometry3()) {
            let extrinsics1 = Extrinsics::from(isometry1);
            let extrinsics2 = Extrinsics::from(isometry2);

            assert_relative_eq!(
                isometry1,
                extrinsics1.to_isometry(),
                epsilon = EPSILON,
            );

            assert_relative_eq!(
                isometry2,
                extrinsics2.to_isometry(),
                epsilon = EPSILON,
            );

            assert_relative_eq!(
                isometry2 * isometry1,
                extrinsics2.compose(&extrinsics1).to_isometry(),
                epsilon = EPSILON,
            );

            assert_relative_eq!(
                extrinsics2.to_isometry() * extrinsics1.to_isometry(),
                extrinsics2.compose(&extrinsics1).to_isometry(),
                epsilon = EPSILON,
            );

            assert_relative_eq!(
                extrinsics2.to_isometry() * extrinsics1.to_isometry(),
                extrinsics2.compose(&extrinsics1).to_isometry(),
                epsilon = EPSILON,
            );
        }

        #[test]
        fn representation_error_is_small(isometry in arbitrary::arb_isometry3()) {
            let extrinsics = Extrinsics::from(&isometry);

            assert_relative_eq!(extrinsics.to_isometry(), isometry, epsilon = EPSILON);

            assert_relative_eq!(
                Extrinsics::from(extrinsics.to_isometry()).to_isometry(),
                isometry,
                epsilon = EPSILON,
            );

            assert_relative_eq!(
                Extrinsics::from(Extrinsics::from(extrinsics.to_isometry()).to_isometry())
                    .to_isometry(),
                isometry,
                epsilon = EPSILON,
            );
        }

        #[test]
        fn extrinsics_ser_de_is_reflective(extrinsics in any::<Extrinsics>()) {
            // Since we SerDe to isometry, these are no longer exactly equal
            let serialized = serde_json::to_string(&extrinsics).unwrap();
            let deserialized: Extrinsics = serde_json::from_str(&serialized).unwrap();

            prop_assert!(extrinsics.is_same(&deserialized));
        }

        #[test]
        fn extrinsic_covariance_ser_de_is_reflective(covariance in any::<ExtrinsicCovariance>()) {
            let serialized = serde_json::to_string(&covariance).unwrap();
            let deserialized = serde_json::from_str(&serialized).unwrap();

            prop_assert!(covariance.is_same(&deserialized));
        }
    }
}

//#endregion
