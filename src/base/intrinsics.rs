// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! Types for describing common component models & intrinsics sets.

mod camera_intrinsics;
mod lidar_intrinsics;

pub use camera_intrinsics::{AffinityModel, CameraIntrinsics, DistortionModel, ProjectionModel};
pub use lidar_intrinsics::{
    LidarAltitudeModel, LidarAzimuthModel, LidarIntrinsics, LidarRangeModel,
};
