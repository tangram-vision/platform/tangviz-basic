// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! Type for describing coordinates in an image.

use nalgebra as na;

//#region Type declarations

/// Describes an image-plane coordinate.
///
/// Coordinate Frame:
///
/// - Origin: Top-left corner of top-left pixel
/// - +X: Right (along columns)
/// - +Y: Down (along rows)
/// - +Z: Into the plane
#[derive(Clone, Debug, PartialEq)]
pub struct MeasuredImagePoint(pub na::geometry::Point<f64, 2>);

impl MeasuredImagePoint {
    /// Create a new MeasuredImagePoint    
    pub fn new(x: f64, y: f64) -> Self {
        Self(na::geometry::Point::<f64, 2>::new(x, y))
    }
    /// Retrieve x component
    pub fn x(&self) -> f64 {
        self.0.x
    }
    /// Retrieve y component
    pub fn y(&self) -> f64 {
        self.0.y
    }
}

/// Describes the variance / covariance of an image-plane coordinate.
///
/// Like the measured point itself, this is always in reference to the image-plane's local
/// coordinate frame. Units are likewise not specified here, as that should be described in the
/// image measurement metadata.
#[derive(Clone, Debug, PartialEq)]
pub struct MeasuredImagePointVariance {
    /// Variance of the horizontal coordinate of the point.
    ///
    /// One should expect that this value is always positive, as variance is standard deviation
    /// **squared**.
    pub x_variance: f64,

    /// Variance of the vertical coordinate of the point.
    ///
    /// One should expect that this value is always positive, as variance is standard deviation
    /// **squared**.
    pub y_variance: f64,

    /// Covariance between the horizontal and vertical coordinate of the point.
    pub xy_covariance: f64,
}

/// Describes an image-plane coordinate that has had principal point / distortion / affinity model
/// corrections applied to it.
///
/// Coordinate Frame:
///
/// - Origin: Principal Point
/// - +X: Right
/// - +Y: Down
/// - +Z: Into the plane
#[derive(Clone, Debug, PartialEq)]
pub struct CorrectedImagePoint(pub na::geometry::Point<f64, 2>);

impl CorrectedImagePoint {
    /// Create a new CorrectedImagePoint        
    pub fn new(x: f64, y: f64) -> Self {
        Self(na::geometry::Point::<f64, 2>::new(x, y))
    }
    /// Retrieve x component
    pub fn x(&self) -> f64 {
        self.0.x
    }
    /// Retrieve y component
    pub fn y(&self) -> f64 {
        self.0.y
    }
}

//#endregion
