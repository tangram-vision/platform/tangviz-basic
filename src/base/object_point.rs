// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------

//! Type for describing coordinates of real-world objects.

use crate::prelude::{Decoder, Encodable};
use nalgebra as na;
use tangviz_basic_macros::Encodable;

//#region Type declarations

/// Type describing a point in object-space.
///
/// This point is always in reference to the world coordinate frame. The world frame may be defined
/// differently according to the problem that you're tackling, so be warned that the type makes no
/// guarantees about the frame-of-reference at all, and the user is expected to guarantee that
/// their world-frame is consistent.
#[derive(Clone, Debug, Encodable)]
pub struct ObjectPoint(pub na::geometry::Point<f64, 3>);

/// Type describing the variances of a point in object-space.
///
/// Like object-space points themselves, these are always in reference to the world coordinate
/// frame.
///
/// Note that covariances are left out here because object-space points typically are not defined
/// in a way that a covariance between the X and Y coordinate would make any physical sense.
#[derive(Clone, Debug)]
pub struct ObjectPointVariance {
    /// Variance of the point along the world-frame X-axis.
    ///
    /// One should expect that this value is always positive, as variance is standard deviation
    /// **squared**.
    pub x_variance: f64,

    /// Variance of the point along the world-frame Y-axis.
    ///
    /// One should expect that this value is always positive, as variance is standard deviation
    /// **squared**.
    pub y_variance: f64,

    /// Variance of the point along the world-frame Z-axis.
    ///
    /// One should expect that this value is always positive, as variance is standard deviation
    /// **squared**.
    pub z_variance: f64,
}

//#endregion
