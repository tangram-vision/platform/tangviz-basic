// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! Types for describing LiDAR intrinsics.

use crate::prelude::{Decoder, Encodable, Sameness};
use approx::{AbsDiffEq, RelativeEq};
use serde::{Deserialize, Serialize};
use std::fmt::Display;
use tangviz_basic_macros::{Encodable, Sameness};

//#region Type declarations

/// Type for modeling the intrinsics of a LiDAR device.
#[derive(Clone, Debug, Encodable, Deserialize, Serialize, PartialEq, Sameness)]
pub struct LidarIntrinsics {
    /// Denotes the intrinsic model for acquiring range.
    pub range: LidarRangeModel,

    /// Denotes the intrinsic model for the azimuth (horizontal angle) of a point.
    pub azimuth: LidarAzimuthModel,

    /// Denotes the intrinsic model for the altitude (vertical angle) of a point.
    pub altitude: LidarAltitudeModel,
}

/// Sub-model for describing range-based aberrations along a beam path.
#[derive(Clone, Debug, Encodable, Deserialize, Serialize, PartialEq, Sameness)]
pub enum LidarRangeModel {
    /// No range model applied.
    NoRange,

    /// The range of the LiDAR consists of a fixed number of beams (e.g. Velodyne VLP-16 has 16
    /// beams), and each of these beams has a range offset correction to the "standard" model for
    /// that LiDAR component.
    PerBeamOffset(Vec<f64>),
}

/// Sub-model for describing azimuthal (horizontal angle) based aberrations along a beam path.
#[derive(Clone, Debug, Encodable, Deserialize, Serialize, PartialEq, Sameness)]
pub enum LidarAzimuthModel {
    /// No azimuth model applied.
    NoAzimuth,

    /// The azimuth of the LiDAR consists of a fixed number of beams (e.g. Velodyne VLP-16 has 16
    /// beams), and each of these beams has an azimuthal offset correction to the "standard" model
    /// for that LiDAR component.
    PerBeamOffset(Vec<f64>),
}

/// Sub-model for describing altitudinal (vertical angle) based aberrations along a beam path.
#[derive(Clone, Debug, Encodable, Deserialize, Serialize, PartialEq, Sameness)]
pub enum LidarAltitudeModel {
    /// No altitude model applied.
    NoAltitude,

    /// The altitude of the LiDAR consists of a fixed number of beams (e.g. Velodyne VLP-16 has 16
    /// beams), and each of these beams has an altitudinal offset correction to the "standard"
    /// model for that LiDAR component.
    PerBeamOffset(Vec<f64>),
}

//#endregion

//#region LidarRangeModel impl

impl Display for LidarRangeModel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            LidarRangeModel::NoRange => {
                write!(f, "No Range Model")
            }
            LidarRangeModel::PerBeamOffset(xs) => {
                write!(f, "PerBeamOffset({:?})", xs)
            }
        }
    }
}

impl AbsDiffEq for LidarRangeModel {
    type Epsilon = f64;

    #[inline]
    fn default_epsilon() -> Self::Epsilon {
        f64::EPSILON
    }

    fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
        match (self, other) {
            (LidarRangeModel::NoRange, LidarRangeModel::NoRange) => true,

            (LidarRangeModel::PerBeamOffset(xs), LidarRangeModel::PerBeamOffset(ys)) => {
                xs.iter().zip(ys).all(|(x, y)| x.abs_diff_eq(y, epsilon))
            }

            _ => false,
        }
    }
}

impl RelativeEq for LidarRangeModel {
    #[inline]
    fn default_max_relative() -> Self::Epsilon {
        Self::default_epsilon()
    }

    fn relative_eq(
        &self,
        other: &Self,
        epsilon: Self::Epsilon,
        max_relative: Self::Epsilon,
    ) -> bool {
        match (self, other) {
            (LidarRangeModel::NoRange, LidarRangeModel::NoRange) => true,

            (LidarRangeModel::PerBeamOffset(xs), LidarRangeModel::PerBeamOffset(ys)) => xs
                .iter()
                .zip(ys)
                .all(|(x, y)| x.relative_eq(y, epsilon, max_relative)),

            _ => false,
        }
    }
}

//#endregion

//#region LidarAzimuthModel impl

impl Display for LidarAzimuthModel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            LidarAzimuthModel::NoAzimuth => {
                write!(f, "No Azimuth Model")
            }
            LidarAzimuthModel::PerBeamOffset(xs) => {
                write!(f, "PerBeamOffset({:?})", xs)
            }
        }
    }
}

impl AbsDiffEq for LidarAzimuthModel {
    type Epsilon = f64;

    #[inline]
    fn default_epsilon() -> Self::Epsilon {
        f64::EPSILON
    }

    fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
        match (self, other) {
            (LidarAzimuthModel::NoAzimuth, LidarAzimuthModel::NoAzimuth) => true,

            (LidarAzimuthModel::PerBeamOffset(xs), LidarAzimuthModel::PerBeamOffset(ys)) => {
                xs.iter().zip(ys).all(|(x, y)| x.abs_diff_eq(y, epsilon))
            }

            _ => false,
        }
    }
}

impl RelativeEq for LidarAzimuthModel {
    #[inline]
    fn default_max_relative() -> Self::Epsilon {
        Self::default_epsilon()
    }

    fn relative_eq(
        &self,
        other: &Self,
        epsilon: Self::Epsilon,
        max_relative: Self::Epsilon,
    ) -> bool {
        match (self, other) {
            (LidarAzimuthModel::NoAzimuth, LidarAzimuthModel::NoAzimuth) => true,

            (LidarAzimuthModel::PerBeamOffset(xs), LidarAzimuthModel::PerBeamOffset(ys)) => xs
                .iter()
                .zip(ys)
                .all(|(x, y)| x.relative_eq(y, epsilon, max_relative)),

            _ => false,
        }
    }
}

//#endregion

//#region LidarAltitudeModel impl

impl Display for LidarAltitudeModel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            LidarAltitudeModel::NoAltitude => {
                write!(f, "No Altitude Model")
            }
            LidarAltitudeModel::PerBeamOffset(xs) => {
                write!(f, "PerBeamOffset({:?})", xs)
            }
        }
    }
}

impl AbsDiffEq for LidarAltitudeModel {
    type Epsilon = f64;

    #[inline]
    fn default_epsilon() -> Self::Epsilon {
        f64::EPSILON
    }

    fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
        match (self, other) {
            (LidarAltitudeModel::NoAltitude, LidarAltitudeModel::NoAltitude) => true,

            (LidarAltitudeModel::PerBeamOffset(xs), LidarAltitudeModel::PerBeamOffset(ys)) => {
                xs.iter().zip(ys).all(|(x, y)| x.abs_diff_eq(y, epsilon))
            }

            _ => false,
        }
    }
}

impl RelativeEq for LidarAltitudeModel {
    #[inline]
    fn default_max_relative() -> Self::Epsilon {
        Self::default_epsilon()
    }

    fn relative_eq(
        &self,
        other: &Self,
        epsilon: Self::Epsilon,
        max_relative: Self::Epsilon,
    ) -> bool {
        match (self, other) {
            (LidarAltitudeModel::NoAltitude, LidarAltitudeModel::NoAltitude) => true,

            (LidarAltitudeModel::PerBeamOffset(xs), LidarAltitudeModel::PerBeamOffset(ys)) => xs
                .iter()
                .zip(ys)
                .all(|(x, y)| x.relative_eq(y, epsilon, max_relative)),

            _ => false,
        }
    }
}

//#endregion

//#region LidarIntrinsics impl

impl Display for LidarIntrinsics {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "LidarIntrinsics {{ range: {}, azimuth: {}, altitude: {} }}",
            self.range, self.azimuth, self.altitude
        )
    }
}

impl AbsDiffEq for LidarIntrinsics {
    type Epsilon = f64;

    #[inline]
    fn default_epsilon() -> Self::Epsilon {
        f64::EPSILON
    }

    fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
        self.range.abs_diff_eq(&other.range, epsilon)
            && self.azimuth.abs_diff_eq(&other.azimuth, epsilon)
            && self.altitude.abs_diff_eq(&other.altitude, epsilon)
    }
}

impl RelativeEq for LidarIntrinsics {
    #[inline]
    fn default_max_relative() -> Self::Epsilon {
        Self::default_epsilon()
    }

    fn relative_eq(
        &self,
        other: &Self,
        epsilon: Self::Epsilon,
        max_relative: Self::Epsilon,
    ) -> bool {
        self.range.relative_eq(&other.range, epsilon, max_relative)
            && self
                .azimuth
                .relative_eq(&other.azimuth, epsilon, max_relative)
            && self
                .altitude
                .relative_eq(&other.altitude, epsilon, max_relative)
    }
}

//#endregion

//#region Tests

/// Module containing proptest-specific "arbitrary" functions, which provide arbitrary / randomized
/// data for property-based testing.
#[cfg(any(test, feature = "proptest-support"))]
pub mod arbitrary {
    use super::*;
    use proptest::prelude::*;

    impl Arbitrary for LidarRangeModel {
        type Strategy = BoxedStrategy<Self>;
        type Parameters = usize;

        fn arbitrary_with(len: Self::Parameters) -> Self::Strategy {
            if len == 0 {
                Just(LidarRangeModel::NoRange).boxed()
            } else {
                prop_oneof![
                    Just(LidarRangeModel::NoRange),
                    any_with::<Vec<f64>>((proptest::sample::size_range(len..=len), ()))
                        .prop_map(LidarRangeModel::PerBeamOffset),
                ]
                .boxed()
            }
        }
    }

    impl Arbitrary for LidarAzimuthModel {
        type Strategy = BoxedStrategy<Self>;
        type Parameters = usize;

        fn arbitrary_with(len: Self::Parameters) -> Self::Strategy {
            if len == 0 {
                Just(LidarAzimuthModel::NoAzimuth).boxed()
            } else {
                prop_oneof![
                    Just(LidarAzimuthModel::NoAzimuth),
                    any_with::<Vec<f64>>((proptest::sample::size_range(len..=len), ()))
                        .prop_map(LidarAzimuthModel::PerBeamOffset),
                ]
                .boxed()
            }
        }
    }

    impl Arbitrary for LidarAltitudeModel {
        type Strategy = BoxedStrategy<Self>;
        type Parameters = usize;

        fn arbitrary_with(len: Self::Parameters) -> Self::Strategy {
            if len == 0 {
                Just(LidarAltitudeModel::NoAltitude).boxed()
            } else {
                prop_oneof![
                    Just(LidarAltitudeModel::NoAltitude),
                    any_with::<Vec<f64>>((proptest::sample::size_range(len..=len), ()))
                        .prop_map(LidarAltitudeModel::PerBeamOffset),
                ]
                .boxed()
            }
        }
    }

    impl Arbitrary for LidarIntrinsics {
        type Strategy = BoxedStrategy<Self>;
        type Parameters = usize;

        fn arbitrary_with(len: Self::Parameters) -> Self::Strategy {
            any_with::<(LidarRangeModel, LidarAzimuthModel, LidarAltitudeModel)>((len, len, len))
                .prop_map(|(range, azimuth, altitude)| LidarIntrinsics {
                    range,
                    azimuth,
                    altitude,
                })
                .boxed()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;

    proptest! {
        #[test]
        fn range_model_ser_de_is_reflective(range in any_with::<LidarRangeModel>(32)) {
            let serialized = serde_json::to_string(&range).unwrap();
            let deserialized = serde_json::from_str(&serialized).unwrap();

            prop_assert!(range.is_same(&deserialized));
        }

        #[test]
        fn azimuth_model_ser_de_is_reflective(azimuth in any_with::<LidarAzimuthModel>(32)) {
            let serialized = serde_json::to_string(&azimuth).unwrap();
            let deserialized = serde_json::from_str(&serialized).unwrap();

            prop_assert!(azimuth.is_same(&deserialized));
        }

        #[test]
        fn altitude_model_ser_de_is_reflective(altitude in any_with::<LidarAltitudeModel>(32)) {
            let serialized = serde_json::to_string(&altitude).unwrap();
            let deserialized = serde_json::from_str(&serialized).unwrap();

            prop_assert!(altitude.is_same(&deserialized));
        }

        #[test]
        fn lidar_intrinsics_ser_de_is_reflective(intrinsics in any_with::<LidarIntrinsics>(32)) {
            let serialized = serde_json::to_string(&intrinsics).unwrap();
            let deserialized = serde_json::from_str(&serialized).unwrap();

            prop_assert!(intrinsics.is_same(&deserialized));
        }
    }
}

//#endregion
