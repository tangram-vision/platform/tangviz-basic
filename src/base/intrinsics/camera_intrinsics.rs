// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! Types for describing a camera's intrinsics.

use crate::{
    base::{CorrectedImagePoint, MeasuredImagePoint},
    prelude::{Decoder, Encodable, Sameness},
};
use approx::{AbsDiffEq, RelativeEq};
use nalgebra as na;
use serde::{Deserialize, Serialize};
use std::{cmp::PartialEq, fmt::Display};
use tangviz_basic_macros::{Encodable, Sameness};

#[cfg(any(test, feature = "proptest-support"))]
use proptest_derive::Arbitrary;

//#region Type declarations

/// Lens projection model of the camera.
///
/// The projection model of the camera describes how light (traveling in straight rays) reflects
/// off an object, passes through the "lens" or principal point of the camera, and then is traced
/// through that principal centre back onto the image plane (in other words, how light is projected through
/// the lens onto the image).
#[cfg_attr(any(test, feature = "proptest-support"), derive(Arbitrary))]
#[derive(Clone, Debug, Encodable, Deserialize, Serialize, PartialEq, Sameness)]
#[serde(rename_all = "snake_case")]
pub enum ProjectionModel {
    /// Standard pinhole projection model.
    ///
    /// Consists of a single focal length, and principal point offsets. This projection model
    /// matches the following collinearity equations:
    ///
    /// ```text
    /// [ x ] = [ f * Xc / Zc ] + [ cx ]
    /// [ y ] = [ f * Yc / Zc ] + [ cy ]
    /// ```
    ///
    /// Where `x` and `y` are image plane coordinates (column and row) and `Xc` / `Yc` / `Zc` are
    /// object-space coordinates transformed by the camera's extrinsics.
    Pinhole {
        /// Principal distance for this camera.
        f: f64,

        /// Principal point offset along the horizontal direction.
        cx: f64,

        /// Principal point offset along the vertical direction.
        cy: f64,
    },
}

/// Distortions characterized by lens effects.
///
/// Examples can include radial / tangential components of light refraction through lenses. Some
/// lens models (e.g. Kannala-Brandt) do not differentiate between radial and tangential components
/// as lenses are modeled as general conics and corrections are applied in a unified manner.
#[cfg_attr(any(test, feature = "proptest-support"), derive(Arbitrary))]
#[derive(Clone, Debug, Encodable, Deserialize, Serialize, PartialEq, Sameness)]
#[serde(rename_all = "snake_case")]
pub enum DistortionModel {
    /// No distortion model applied.
    ///
    /// Should only be used to describe pre-rectified images.
    NoDistortion,

    /// The "complete" Brown-Conrady distortion correction model.
    ///
    /// This model covers both radial and tangential distortion of light between the lens and image
    /// plane. Tangential distortion is sometimes referred to as decentering distortion, because
    /// tangential effects are modeled as tangential shifts from the principal point due to lens
    /// assembly misalignment.
    ///
    /// The Brown-Conrady model uses a separate set of coefficients for both radial
    /// (k-coefficients) and tangential (p-coefficients) distortions. These coefficients
    /// approximate a Taylor-series expansion of the radial and tangential components of
    /// semi-spherical lens distortion. The approximation of this Taylor series assumes some amount
    /// of "small-angle approximation." This assumption of small angle approximation often breaks
    /// down when dealing with lenses that have extreme distortions at the periphery (e.g. fish-eye
    /// or ultra-wide angle lenses). However, the model is fairly robust for standard view lenses
    /// and is resilient to distortion overfitting when used on standard view lenses.
    ///
    /// See the [original Conrady paper](https://doi.org/10.1093%2Fmnras%2F79.5.384) for a more
    /// thorough description of the model, or the [original Brown
    /// paper](http://www.close-range.com/docs/Decentering_Distortion_of_Lenses_Brown_1966_may_444-462.pdf).
    ///
    /// While we do advertise this as the "complete" Brown-Conrady model, it is only complete
    /// insofar as it considers the most significant parametric coefficients seen in most cameras.
    /// As far as additional parameters go, one could have [k1 kN] for some arbitrarily large N, or
    /// [p1 pM] for some arbitrarily large M. In practice, however, one should find that the
    /// provided model below suffices for most standard lens types.
    BrownConrady {
        /// First order radial distortion correction coefficient.
        k1: f64,

        /// Second order radial distortion correction coefficient.
        k2: f64,

        /// Third order radial distortion correction coefficient.
        ///
        /// In most cases, this coefficient should probably go unused. If you do find yourself
        /// utilizing higher order components for radial lens distortion, there is a good chance
        /// you want to use the Kannala-Brandt model instead.
        k3: f64,

        /// First order tangential (decentering) distortion correction coefficient.
        p1: f64,

        /// Second order tangential (decentering) distortion correction coefficient.
        p2: f64,
    },

    /// The complete Kannala-Brandt distortion correction model.
    ///
    /// This model bundles radial and tangential distortions in a combined form, treating the final
    /// correction as a series of coefficients describing a singular conic. Because we are fitting
    /// a full conic, there is no "small-angle approximation" in the underlying model, which makes
    /// this model a better fit for fish-eye, wide, or ultra-wide angle lenses. However, this model
    /// is less applicable to standard view lenses, as it can be subject to overfitting of the
    /// underlying distortion. This typically demonstrates an artefact of trying to fit a broader
    /// conic to a small line segment of noisy data.
    ///
    /// See the [original
    /// paper](http://close-range.com/docs/A_GENERIC_CAMERA_MODEL_AND_CALIBRATION_METHOD_Kannala-Brandt_pdf697.pdf)
    /// for more details.
    ///
    /// While we do advertise this as the "complete" Kannala-Brandt model, it is only complete
    /// insofar as it considers the most significant parametric coefficients used for most
    /// wide and ultra-wide angle lens configurations. While arbitrarily large numbers of
    /// coefficients could be used to describe the conical model of the lens, in practice the
    /// provided model below suffices for the majority of lenses.
    KannalaBrandt {
        /// First order KB distortion correction coefficient.
        k1: f64,

        /// Second order KB distortion correction coefficient.
        k2: f64,

        /// Third order KB distortion correction coefficient.
        k3: f64,

        /// Fourth order KB distortion correction coefficient.
        k4: f64,
    },
}

impl Default for DistortionModel {
    fn default() -> Self {
        DistortionModel::NoDistortion
    }
}

impl DistortionModel {
    /// Check whether this instance of DistortionModel is NoDistortion. Used in serialization logic.
    fn is_no_distortion(&self) -> bool {
        matches!(self, DistortionModel::NoDistortion)
    }
}

/// Distortions characterized as affine transformations (scale and non-orthogonality) in the image
/// plane.
///
/// These appear as either scale or shear effects, and are applied exclusively to the x-axis.
#[cfg_attr(any(test, feature = "proptest-support"), derive(Arbitrary))]
#[derive(Clone, Debug, Encodable, Deserialize, Serialize, PartialEq, Sameness)]
#[serde(rename_all = "snake_case")]
#[serde(untagged)]
pub enum AffinityModel {
    /// No affine distortions.
    NoAffinity,

    /// Affine scale and non-orthogonality in the image plane.
    ///
    /// Models both scale and shear effects in the image plane.
    ///
    /// ```text
    /// x_corrected = x + a1 * x + a2 * y;
    /// y_corrected = y;
    /// ```
    ///
    ScaleAndShear {
        /// The scale factor.
        a1: f64,
        /// The shear factor.
        a2: f64,
    },

    /// Affine scale difference along the x-axis.
    ///
    /// This is modeled as a scale difference in pixel pitch between the x and y axes in image
    /// space. However, this value is specifically meant to be applied directly as a correction to
    /// the x-coordinate in image space.
    ///
    /// ```text
    /// x_corrected = x + a1 * x;
    /// y_corrected = y;
    /// ```
    ///
    Scale {
        /// The scale factor.
        a1: f64,
    },

    /// Non-orthogonality in the image plane.
    ///
    /// This models image shear, which is a distortion artefact that appears as if the image plane
    /// is shaped not as a rectangle or square (with orthogonal corners) but rather a rhombus,
    /// which has corner angles that are not 90 degrees. Like scale, this is only applied to the
    /// x-coordinate in image space.
    ///
    /// ```text
    /// x_corrected = x + a2 * y;
    /// y_corrected = y;
    /// ```
    ///
    Shear {
        /// The shear factor.
        a2: f64,
    },
}

impl Default for AffinityModel {
    fn default() -> Self {
        AffinityModel::NoAffinity
    }
}

impl AffinityModel {
    /// Check whether this instance of AffinityModel is NoAffinity. Used in serialization logic.
    fn is_no_affinity(&self) -> bool {
        matches!(self, AffinityModel::NoAffinity)
    }
}

/// Type for modeling the intrinsics (interior orientation) of a camera.
#[cfg_attr(any(test, feature = "proptest-support"), derive(Arbitrary))]
#[derive(Clone, Debug, Encodable, Deserialize, Serialize, PartialEq, Sameness)]
pub struct CameraIntrinsics {
    /// Model parameters holding the appropriate projection model parameters to use for this
    /// camera.
    pub projection: ProjectionModel,

    /// Model parameters holding the appropriate radial / decentering coefficients to use for this
    /// camera.
    #[serde(default)]
    #[serde(skip_serializing_if = "DistortionModel::is_no_distortion")]
    pub distortion: DistortionModel,

    /// Model parameters holding the appropriate affine / non-orthogonality coefficients to use for
    /// this camera.
    #[serde(default)]
    #[serde(skip_serializing_if = "AffinityModel::is_no_affinity")]
    pub affinity: AffinityModel,

    /// The width of the image format from the camera that these intrinsics were calibrated for.
    ///
    /// In units of pixels.
    #[no_encode]
    pub width: usize,

    /// The height of the image format from the camera that these intrinsics were calibrated for.
    ///
    /// In units of pixels.
    #[no_encode]
    pub height: usize,
}

//#endregion

//#region ProjectionModel impl

impl Display for ProjectionModel {
    fn fmt(&self, file: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ProjectionModel::Pinhole { f, cx, cy } => {
                write!(file, "Pinhole {{ f: {}, cx: {}, cy: {} }}", f, cx, cy)
            }
        }
    }
}

impl AbsDiffEq for ProjectionModel {
    type Epsilon = f64;

    #[inline]
    fn default_epsilon() -> Self::Epsilon {
        f64::EPSILON
    }

    fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
        match (self, other) {
            (
                ProjectionModel::Pinhole {
                    f: lf,
                    cx: lcx,
                    cy: lcy,
                },
                ProjectionModel::Pinhole {
                    f: rf,
                    cx: rcx,
                    cy: rcy,
                },
            ) => {
                lf.abs_diff_eq(rf, epsilon)
                    && lcx.abs_diff_eq(rcx, epsilon)
                    && lcy.abs_diff_eq(rcy, epsilon)
            }
        }
    }
}

impl RelativeEq for ProjectionModel {
    #[inline]
    fn default_max_relative() -> Self::Epsilon {
        Self::default_epsilon()
    }

    fn relative_eq(
        &self,
        other: &Self,
        epsilon: Self::Epsilon,
        max_relative: Self::Epsilon,
    ) -> bool {
        match (self, other) {
            (
                ProjectionModel::Pinhole {
                    f: lf,
                    cx: lcx,
                    cy: lcy,
                },
                ProjectionModel::Pinhole {
                    f: rf,
                    cx: rcx,
                    cy: rcy,
                },
            ) => {
                lf.relative_eq(rf, epsilon, max_relative)
                    && lcx.relative_eq(rcx, epsilon, max_relative)
                    && lcy.relative_eq(rcy, epsilon, max_relative)
            }
        }
    }
}

//#endregion

//#region DistortionModel impl

impl Display for DistortionModel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            DistortionModel::NoDistortion => {
                write!(f, "None")
            }
            DistortionModel::BrownConrady { k1, k2, k3, p1, p2 } => {
                write!(
                    f,
                    "BC {{ k1: {}, k2: {}, k3: {}, p1: {}, p2: {} }}",
                    k1, k2, k3, p1, p2
                )
            }
            DistortionModel::KannalaBrandt { k1, k2, k3, k4 } => {
                write!(f, "KB {{ k1: {}, k2: {}, k3: {}, k4: {} }}", k1, k2, k3, k4)
            }
        }
    }
}

impl AbsDiffEq for DistortionModel {
    type Epsilon = f64;

    #[inline]
    fn default_epsilon() -> Self::Epsilon {
        f64::EPSILON
    }

    fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
        match (self, other) {
            (DistortionModel::NoDistortion, DistortionModel::NoDistortion) => true,

            (
                DistortionModel::BrownConrady {
                    k1: lk1,
                    k2: lk2,
                    k3: lk3,
                    p1: lp1,
                    p2: lp2,
                },
                DistortionModel::BrownConrady {
                    k1: rk1,
                    k2: rk2,
                    k3: rk3,
                    p1: rp1,
                    p2: rp2,
                },
            ) => {
                lk1.abs_diff_eq(rk1, epsilon)
                    && lk2.abs_diff_eq(rk2, epsilon)
                    && lk3.abs_diff_eq(rk3, epsilon)
                    && lp1.abs_diff_eq(rp1, epsilon)
                    && lp2.abs_diff_eq(rp2, epsilon)
            }

            (
                DistortionModel::KannalaBrandt {
                    k1: lk1,
                    k2: lk2,
                    k3: lk3,
                    k4: lk4,
                },
                DistortionModel::KannalaBrandt {
                    k1: rk1,
                    k2: rk2,
                    k3: rk3,
                    k4: rk4,
                },
            ) => {
                lk1.abs_diff_eq(rk1, epsilon)
                    && lk2.abs_diff_eq(rk2, epsilon)
                    && lk3.abs_diff_eq(rk3, epsilon)
                    && lk4.abs_diff_eq(rk4, epsilon)
            }

            _ => false,
        }
    }
}

impl RelativeEq for DistortionModel {
    #[inline]
    fn default_max_relative() -> Self::Epsilon {
        Self::default_epsilon()
    }

    fn relative_eq(
        &self,
        other: &Self,
        epsilon: Self::Epsilon,
        max_relative: Self::Epsilon,
    ) -> bool {
        match (self, other) {
            (DistortionModel::NoDistortion, DistortionModel::NoDistortion) => true,

            (
                DistortionModel::BrownConrady {
                    k1: lk1,
                    k2: lk2,
                    k3: lk3,
                    p1: lp1,
                    p2: lp2,
                },
                DistortionModel::BrownConrady {
                    k1: rk1,
                    k2: rk2,
                    k3: rk3,
                    p1: rp1,
                    p2: rp2,
                },
            ) => {
                lk1.relative_eq(rk1, epsilon, max_relative)
                    && lk2.relative_eq(rk2, epsilon, max_relative)
                    && lk3.relative_eq(rk3, epsilon, max_relative)
                    && lp1.relative_eq(rp1, epsilon, max_relative)
                    && lp2.relative_eq(rp2, epsilon, max_relative)
            }

            (
                DistortionModel::KannalaBrandt {
                    k1: lk1,
                    k2: lk2,
                    k3: lk3,
                    k4: lk4,
                },
                DistortionModel::KannalaBrandt {
                    k1: rk1,
                    k2: rk2,
                    k3: rk3,
                    k4: rk4,
                },
            ) => {
                lk1.relative_eq(rk1, epsilon, max_relative)
                    && lk2.relative_eq(rk2, epsilon, max_relative)
                    && lk3.relative_eq(rk3, epsilon, max_relative)
                    && lk4.relative_eq(rk4, epsilon, max_relative)
            }

            _ => false,
        }
    }
}

//#endregion

//#region AffinityModel impl

impl Display for AffinityModel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            AffinityModel::NoAffinity => {
                write!(f, "None")
            }
            AffinityModel::Scale { a1 } => {
                write!(f, "Scale {{ a1: {} }}", a1)
            }
            AffinityModel::Shear { a2 } => {
                write!(f, "Shear {{ a2: {} }}", a2)
            }
            AffinityModel::ScaleAndShear { a1, a2 } => {
                write!(f, "Full {{ a1: {}, a2: {} }}", a1, a2)
            }
        }
    }
}

impl AbsDiffEq for AffinityModel {
    type Epsilon = f64;

    #[inline]
    fn default_epsilon() -> Self::Epsilon {
        f64::EPSILON
    }

    fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
        match (self, other) {
            (AffinityModel::NoAffinity, AffinityModel::NoAffinity) => true,

            (AffinityModel::Scale { a1: la1 }, AffinityModel::Scale { a1: ra1 }) => {
                la1.abs_diff_eq(ra1, epsilon)
            }

            (AffinityModel::Shear { a2: la2 }, AffinityModel::Shear { a2: ra2 }) => {
                la2.abs_diff_eq(ra2, epsilon)
            }

            (
                AffinityModel::ScaleAndShear { a1: la1, a2: la2 },
                AffinityModel::ScaleAndShear { a1: ra1, a2: ra2 },
            ) => la1.abs_diff_eq(ra1, epsilon) && la2.abs_diff_eq(ra2, epsilon),

            _ => false,
        }
    }
}

impl RelativeEq for AffinityModel {
    #[inline]
    fn default_max_relative() -> Self::Epsilon {
        Self::default_epsilon()
    }

    fn relative_eq(
        &self,
        other: &Self,
        epsilon: Self::Epsilon,
        max_relative: Self::Epsilon,
    ) -> bool {
        match (self, other) {
            (AffinityModel::NoAffinity, AffinityModel::NoAffinity) => true,

            (AffinityModel::Scale { a1: la1 }, AffinityModel::Scale { a1: ra1 }) => {
                la1.relative_eq(ra1, epsilon, max_relative)
            }

            (AffinityModel::Shear { a2: la2 }, AffinityModel::Shear { a2: ra2 }) => {
                la2.relative_eq(ra2, epsilon, max_relative)
            }

            (
                AffinityModel::ScaleAndShear { a1: la1, a2: la2 },
                AffinityModel::ScaleAndShear { a1: ra1, a2: ra2 },
            ) => {
                la1.relative_eq(ra1, epsilon, max_relative)
                    && la2.relative_eq(ra2, epsilon, max_relative)
            }

            _ => false,
        }
    }
}

//#endregion

//#region CameraIntrinsics impl

impl Display for CameraIntrinsics {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "CameraIntrinsics {{ projection: {}, distortion: {}, affinity: {}, width: {}, height: {} }}",
            self.projection,
            self.distortion,
            self.affinity,
            self.width,
            self.height,
        )
    }
}

impl AbsDiffEq for CameraIntrinsics {
    type Epsilon = f64;

    #[inline]
    fn default_epsilon() -> Self::Epsilon {
        f64::EPSILON
    }

    fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
        self.projection.abs_diff_eq(&other.projection, epsilon)
            && self.distortion.abs_diff_eq(&other.distortion, epsilon)
            && self.affinity.abs_diff_eq(&other.affinity, epsilon)
            && self.width.abs_diff_eq(&other.width, 0)
            && self.height.abs_diff_eq(&other.width, 0)
    }
}

impl RelativeEq for CameraIntrinsics {
    #[inline]
    fn default_max_relative() -> Self::Epsilon {
        Self::default_epsilon()
    }

    fn relative_eq(
        &self,
        other: &Self,
        epsilon: Self::Epsilon,
        max_relative: Self::Epsilon,
    ) -> bool {
        self.projection
            .relative_eq(&other.projection, epsilon, max_relative)
            && self
                .distortion
                .relative_eq(&other.distortion, epsilon, max_relative)
            && self
                .affinity
                .relative_eq(&other.affinity, epsilon, max_relative)
            && self.width.abs_diff_eq(&other.width, 0)
            && self.height.abs_diff_eq(&other.width, 0)
    }
}

impl CameraIntrinsics {
    /// Calculates the correction in x and y along the image plane for a measured point in that
    /// image plane.
    ///
    /// The input point is meant to be a point that has not had any corrections applied to it. This
    /// means no principal point offset, distortions, or affinity.
    ///
    /// NOTE: This does not produce the final point coordinate of the corrected point. For that,
    /// you'll want to call [`CameraIntrinsics::apply_correction`].
    pub fn correction(&self, image_point: &MeasuredImagePoint) -> na::Vector2<f64> {
        let (cx, cy) = match self.projection {
            ProjectionModel::Pinhole { f: _, cx, cy } => (cx, cy),
        };

        let mut x_corr = cx;
        let mut y_corr = cy;

        let (x_orig, y_orig) = (image_point.x(), image_point.y());

        // u and v are the point coordinates corrected for principal point offset.
        let u = x_orig - cx;
        let v = y_orig - cy;
        let u2 = u * u;
        let v2 = v * v;

        match self.distortion {
            DistortionModel::NoDistortion => (),
            DistortionModel::BrownConrady { k1, k2, k3, p1, p2 } => {
                let r2 = u2 + v2;
                let r4 = r2 * r2;
                let r6 = r4 * r2;

                x_corr += u * ((k1 * r2) + (k2 * r4) + (k3 * r6))
                    + (p1 * (r2 + 2.0 * u2))
                    + (2.0 * p2 * u * v);
                y_corr += v * ((k1 * r2) + (k2 * r4) + (k3 * r6))
                    + (p2 * (r2 + 2.0 * v2))
                    + (2.0 * p1 * u * v);
            }
            DistortionModel::KannalaBrandt { k1, k2, k3, k4 } => {
                let r = (u2 + v2).sqrt();

                let theta = match self.projection {
                    ProjectionModel::Pinhole { f, cx: _, cy: _ } => r.atan2(f),
                };

                let theta2 = theta * theta;
                let theta4 = theta2 * theta2;
                let theta6 = theta4 * theta2;

                // u / r is cos(psi), from the original paper
                x_corr += u / r * theta * (k1 + k2 * theta2 + k3 * theta4 + k4 * theta6);
                // v / r is sin(psi), from the original paper
                y_corr += v / r * theta * (k1 + k2 * theta2 + k3 * theta4 + k4 * theta6);
            }
        }

        match self.affinity {
            AffinityModel::NoAffinity => (),
            AffinityModel::Scale { a1 } => {
                x_corr += a1 * u;
            }
            AffinityModel::Shear { a2 } => {
                x_corr += a2 * v;
            }
            AffinityModel::ScaleAndShear { a1, a2 } => {
                x_corr += a1 * u + a2 * v;
            }
        }

        na::Vector2::new(x_corr, y_corr)
    }

    /// Applies the principal point offset, as well as distortion and affinity corrections to a
    /// measured point.
    ///
    /// The input point is meant to be a point that has not had any corrections applied to it. This
    /// means no principal point offset, distortions, or affinity.
    ///
    /// Analogous to "undistorting" a given point. We use the term "correction" here because:
    ///
    /// 1. Undistort isn't a real word.
    /// 2. The corrections applied here are beyond just distortion coefficients, and incorporate
    ///    principal point offset and affinity modeling as well.
    ///
    /// NOTE: This does not produce the corrections themselves. If you want those, you're probably
    /// looking for [`CameraIntrinsics::correction`].
    pub fn apply_correction(&self, image_point: &MeasuredImagePoint) -> CorrectedImagePoint {
        let correction = self.correction(image_point);

        CorrectedImagePoint::new(
            image_point.x() - correction.x,
            image_point.y() - correction.y,
        )
    }

    /// Computes the Jacobian of the intrinsics distortion correction at a given (uncorrected)
    /// image point. Uncorrected points are in pixel space and are thus expected to be centered
    /// around some principal point {Cx, Cy} and not the origin.
    fn jacobian_corr_dist_wrt_pt(&self, pt: &na::Point2<f64>) -> na::Matrix2<f64> {
        let (f, cx, cy) = match self.projection {
            ProjectionModel::Pinhole { f, cx, cy } => (f, cx, cy),
        };

        match self.distortion {
            DistortionModel::NoDistortion => na::Matrix2::<f64>::zeros(),
            DistortionModel::BrownConrady { k1, k2, k3, p1, p2 } => {
                let uc = pt.x - cx;
                let vc = pt.y - cy;
                let uc2 = uc * uc;
                let vc2 = vc * vc;
                let ucvc = uc * vc;
                let r2 = uc2 + vc2;
                let r4 = r2 * r2;
                let r6 = r4 * r2;

                let rad_coeff1 = r2 * k1 + r4 * k2 + r6 * k3;
                let rad_coeff2 = 2.0 * r2 * k2 + 3.0 * r4 * k3 + k1;
                let tan_coeff1 = 6.0 * p1 * uc + 2.0 * p2 * vc;
                let tan_coeff2 = 6.0 * p1 * vc + 2.0 * p2 * uc;
                let tan_coeff3 = 2.0 * p1 * uc + 2.0 * p2 * vc;
                let tan_coeff4 = 2.0 * p2 * uc + 2.0 * p1 * vc;

                na::Matrix2::<f64>::new(
                    rad_coeff1 + 2.0 * uc2 * rad_coeff2 + tan_coeff1,
                    2.0 * ucvc * rad_coeff2 + tan_coeff4,
                    2.0 * ucvc * rad_coeff2 + tan_coeff3,
                    rad_coeff1 + 2.0 * vc2 * rad_coeff2 + tan_coeff2,
                )
            }
            DistortionModel::KannalaBrandt { k1, k2, k3, k4 } => {
                // This was generated using SymPy with common subexpression
                // elimination
                let x0 = cx - pt.x;
                let x1 = x0.powi(2);
                let x2 = cy - pt.y;
                let x3 = x2.powi(2);
                let x4 = x1 + x3;
                let x5 = x4.powf(5.0 / 2.0);
                let x6 = f.powi(2) + x4;
                let x7 = x4.sqrt().atan2(f);
                let x8 = x7.powi(2);
                let x9 = x7.powi(4);
                let x10 = k1 + k2 * x8 + k3 * x9 + k4 * x7.powi(6);
                let x11 = x10 * x6 * x7;
                let x12 = x11 * x5;
                let x13 = x4.powf(3.0 / 2.0);
                let x14 = x11 * x13;
                let x15 = 2.0 * x8;
                let x16 = f * (x10 + x15 * (k2 + k3 * x15 + 3.0 * k4 * x9));
                let x17 = x16 * x4.powi(2);
                let x18 = 1.0 / x6;
                let x19 = x18 / x4.powi(3);
                let x20 = x0 * x18 * x2 * (-x11 * x4 + x13 * x16) / x5;

                na::Matrix2::<f64>::new(
                    x19 * (-x1 * x14 + x1 * x17 + x12),
                    x20,
                    x20,
                    x19 * (x12 - x14 * x3 + x17 * x3),
                )
            }
        }
    }

    /// Computes the Jacobian of the intrinsics correction at a given (uncorrected)
    /// image point. Uncorrected points are in pixel space and are thus expected to be centered
    /// around some principal point [Cx, Cy] and not the origin.
    fn jacobian_corr_wrt_pt(&self, pt: &na::Point2<f64>) -> na::Matrix2<f64> {
        // Principal Point correction is not a function of pt
        let jacobian_corr_dist_wrt_pt = self.jacobian_corr_dist_wrt_pt(pt);
        let jacobian_corr_aff_wrt_pt = match self.affinity {
            AffinityModel::NoAffinity => na::Matrix2::<f64>::zeros(),
            AffinityModel::Scale { a1 } => na::Matrix2::<f64>::new(a1, 0.0, 0.0, 0.0),
            AffinityModel::Shear { a2 } => na::Matrix2::<f64>::new(0.0, a2, 0.0, 0.0),
            AffinityModel::ScaleAndShear { a1, a2 } => na::Matrix2::<f64>::new(a1, a2, 0.0, 0.0),
        };

        jacobian_corr_dist_wrt_pt + jacobian_corr_aff_wrt_pt
    }

    /// Computes the projection of a given 3D point in camera coordinates into the camera plane
    /// using this CameraIntrinsic. That is, this function will compute the uncorrected image
    /// point. The collinearity equation states the following relationship:
    /// ```text
    ///     [f X / Z] = [u] - correction([u]
    ///     [f Y / Z]   [v]              [v])
    /// ```
    /// where:
    /// ```text
    ///     f: focal length
    ///     [X Y Z]: 3D point in camera coordinates
    ///     [u, v]: corresponding image point (usually in pixels)
    /// ```
    ///
    /// Given the intrinsics and 3D point, it's useful to be able to compute the image point.
    /// This allows one to simulate the image formation process described by the intrinsics.
    /// However, solving for [u, v] isn't possible in closed form. This function uses
    /// Newton's method to approximate [u, v] iteratively. Newton's method doesn't guarantee
    /// convergence and this function may fail on extreme intrinsics parameters, returning `None`
    ///
    /// If the resulting point is outside the range of the intrinsics ([0, W) X [0, H)) this
    /// function, returns `None`.
    ///
    /// Parameters:
    /// * `self`: The camera intrinsics
    /// * `pt_cam`: The 3D point in camera coordinates
    /// * `tolerance`: The threshold below which changes in the norm of the answer will cause
    ///                iterations to cease. This has the same unit as indicated by focal length,
    ///                which is usually pixels. Set the tolerance according to the needs of the
    ///                application. For example, if you need to estimate the projected point to
    ///                within 1/1000th of a pixel, set the tolerance to 1e-3.
    /// * `max_iter_count`: The maximum number of iterations to try. Exceeding the maximum without
    ///                     having converged will cause this function to return `None`. Smaller
    ///                     tolerances and great distortions may require more iterations to
    ///                     successfully compute an estimate.
    /// Returns:
    /// * The image point if estimated successfully, `None` otherwise.
    pub fn compute_projected_point(
        &self,
        pt_cam: &na::Point3<f64>,
        tolerance: f64,
        max_iter_count: i64,
    ) -> Option<na::Point2<f64>> {
        if pt_cam.z <= 0.0 {
            return None;
        }
        let (f, cx, cy) = match self.projection {
            ProjectionModel::Pinhole { f, cx, cy } => (f, cx, cy),
        };
        let ideal = na::Point2::<f64>::new(f * pt_cam.x / pt_cam.z, f * pt_cam.y / pt_cam.z);
        let mut guess =
            na::Point2::<f64>::new(f * pt_cam.x / pt_cam.z + cx, f * pt_cam.y / pt_cam.z + cy);

        let mut count = 0;
        loop {
            // r(int, X, pt) = fX/Z - (pt - corr(pt)) = fXY/Z + corr(pt) - pt
            let res = ideal - self.apply_correction(&MeasuredImagePoint(guess)).0;
            // d r / d pt = d corr(pt) / d pt - d pt / d pt
            let j = self.jacobian_corr_wrt_pt(&guess) - na::Matrix2::<f64>::identity();

            // Newton Iteration:
            // x_n = x_n-1 - Jf^-1 * f(x_n-1)
            let new_guess = guess - j.try_inverse()? * res;
            let diff = (new_guess - guess).norm();

            guess = new_guess;
            count += 1;

            if diff < tolerance {
                break;
            }
            if count > max_iter_count {
                return None;
            }
        }

        // Check bounds
        if guess.x >= 0.0
            && guess.x < self.width as f64
            && guess.y >= 0.0
            && guess.y < self.height as f64
        {
            Some(guess)
        } else {
            None
        }
    }
}

//#endregion

#[cfg(test)]
mod tests {
    use super::*;
    use crate::prelude::*;
    use proptest::prelude::*;

    proptest! {
        #[test]
        fn projection_model_ser_de_is_reflective(projection in any::<ProjectionModel>()) {
            let serialized = serde_json::to_string(&projection).unwrap();
            let deserialized = serde_json::from_str(&serialized).unwrap();

            prop_assert!(projection.is_same(&deserialized));
        }

        #[test]
        fn distortion_model_ser_de_is_reflective(distortion in any::<DistortionModel>()) {
            let serialized = serde_json::to_string(&distortion).unwrap();
            let deserialized = serde_json::from_str(&serialized).unwrap();

            prop_assert!(distortion.is_same(&deserialized));
        }

        #[test]
        fn affinity_model_ser_de_is_reflective(affinity in any::<AffinityModel>()) {
            let serialized = serde_json::to_string(&affinity).unwrap();
            let deserialized = serde_json::from_str(&serialized).unwrap();

            prop_assert!(affinity.is_same(&deserialized));
        }

        #[test]
        fn camera_intrinsics_ser_de_is_reflective(intrinsics in any::<CameraIntrinsics>()) {
            let serialized = serde_json::to_string(&intrinsics).unwrap();
            let deserialized = serde_json::from_str(&serialized).unwrap();

            prop_assert!(intrinsics.is_same(&deserialized));
        }
    }

    #[test]
    fn compute_projected_point() {
        // This is a relatively extreme tolerance level for stress testing the function
        let count = 50;
        let tol = 1e-8;

        let pt_cam = na::Point3::<f64>::new(-0.4, -0.35, 1.0);
        let f = 500.0;
        let cx = 320.0;
        let cy = 240.0;
        let pt_ideal = na::Point2::<f64>::new(f * pt_cam.x / pt_cam.z, f * pt_cam.y / pt_cam.z);
        let proj = ProjectionModel::Pinhole { f, cx, cy };
        let affinity = AffinityModel::ScaleAndShear { a1: 0.02, a2: 0.05 };

        // No Dist
        {
            let nodist_int = CameraIntrinsics {
                projection: proj.clone(),
                affinity: affinity.clone(),
                distortion: DistortionModel::NoDistortion {},
                width: 640,
                height: 480,
            };

            let projected = nodist_int
                .compute_projected_point(&pt_cam, tol, count)
                .unwrap();
            let correction = nodist_int.correction(&MeasuredImagePoint(projected));
            let corrected = pt_ideal - (projected - correction);
            assert!(corrected.norm() < tol)
        }

        // BC Dist
        {
            let bc_int = CameraIntrinsics {
                projection: proj.clone(),
                affinity: affinity.clone(),
                distortion: DistortionModel::BrownConrady {
                    k1: 8e-13,
                    k2: -1e-14,
                    k3: 9e-17,
                    p1: 1e-7,
                    p2: -1e-7,
                },
                width: 640,
                height: 480,
            };

            let projected = bc_int.compute_projected_point(&pt_cam, tol, count).unwrap();
            let correction = bc_int.correction(&MeasuredImagePoint(projected));
            let corrected = pt_ideal - (projected - correction);
            assert!(corrected.norm() < tol)
        }

        // KB Dist
        {
            let kb_int = CameraIntrinsics {
                projection: proj.clone(),
                affinity: affinity.clone(),
                distortion: DistortionModel::KannalaBrandt {
                    k1: 1e2,
                    k2: 2e2,
                    k3: 4e3,
                    k4: -8e4,
                },
                width: 640,
                height: 480,
            };

            let projected = kb_int.compute_projected_point(&pt_cam, tol, count).unwrap();
            let correction = kb_int.correction(&MeasuredImagePoint(projected));
            let corrected = pt_ideal - (projected - correction);
            assert!(corrected.norm() < tol)
        }

        // Out-of-Range is None
        {
            let nodist_int = CameraIntrinsics {
                projection: proj,
                affinity,
                distortion: DistortionModel::NoDistortion {},
                width: 640,
                height: 480,
            };

            let projected = nodist_int.compute_projected_point(
                &na::Point3::<f64>::new(-1000.0, 0.0, 1.0),
                tol,
                count,
            );
            assert!(projected.is_none());
        }
    }
}
