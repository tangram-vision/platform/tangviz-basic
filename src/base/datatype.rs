// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! Index of data types that are commonly used as observations

pub mod image_point;

pub use image_point::{CorrectedImagePoint, MeasuredImagePoint, MeasuredImagePointVariance};
