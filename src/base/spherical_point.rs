// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! Type for describing spherical coordinates of real-world (3D) objects.

use nalgebra as na;
use num_traits::{Float, Zero};
use std::convert::TryFrom;
use thiserror::Error;

/// A type for representing 3D points in a spherical coordinate frame.
#[derive(Clone, Debug)]
pub struct SphericalPoint<T>
where
    T: na::Scalar,
{
    /// The coordinates of the point.
    ///
    /// This array is sorted as `[range, azimuth, altitude]`, for all points.
    pub coords: [T; 3],
}

/// Error type for use when constructing a spherical point fails.
#[derive(Debug, Error)]
#[error("Could not construct point. Reason: {0}")]
pub struct SphericalPointConstructionError(String);

impl<T> TryFrom<&[T]> for SphericalPoint<T>
where
    T: na::Scalar,
{
    type Error = SphericalPointConstructionError;

    fn try_from(slice: &[T]) -> Result<Self, Self::Error> {
        if slice.len() == 3 {
            Ok(Self {
                coords: [slice[0].clone(), slice[1].clone(), slice[2].clone()],
            })
        } else {
            Err(SphericalPointConstructionError(String::from(
                "Slice length not equal to 3",
            )))
        }
    }
}

impl<T> SphericalPoint<T>
where
    T: na::Scalar,
{
    /// Constructs a spherical point located at the origin of its basis coordinate frame.
    pub fn origin() -> Self
    where
        T: Zero,
    {
        Self {
            coords: [T::zero(), T::zero(), T::zero()],
        }
    }

    /// Constructs a spherical point.
    pub fn new(range: T, azimuth: T, altitude: T) -> Self {
        Self {
            coords: [range, azimuth, altitude],
        }
    }

    /// Returns the length (number of dimensions) of the point.
    #[allow(clippy::len_without_is_empty)]
    pub fn len(&self) -> usize {
        3
    }

    /// Gets a copy of the range value from the spherical coordinates.
    pub fn range(&self) -> T {
        self.coords[0].clone()
    }

    /// Gets a copy of the azimuth value from the spherical coordinates.
    pub fn azimuth(&self) -> T {
        self.coords[1].clone()
    }

    /// Gets a copy of the altitude value from the spherical coordinates.
    pub fn altitude(&self) -> T {
        self.coords[2].clone()
    }

    /// Constructs a spherical point from a Cartesian point with the same basis.
    pub fn from_cartesian(cartesian: &na::Point3<T>) -> Self
    where
        T: Copy + Clone + na::RealField,
    {
        let range = cartesian.coords.norm();

        let [x, y, z] = <[T; 3]>::from(cartesian.coords);

        let altitude = (z / range).asin();
        let azimuth = y.atan2(x);

        Self {
            coords: [range, azimuth, altitude],
        }
    }

    /// Converts the point into a Cartesian frame with the same basis.
    pub fn into_cartesian(&self) -> na::Point3<T>
    where
        T: Float,
    {
        let [range, azimuth, altitude] = self.coords;

        let x = range * altitude.cos() * azimuth.cos();
        let y = range * altitude.cos() * azimuth.sin();
        let z = range * altitude.sin();

        na::Point3::new(x, y, z)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use approx::assert_relative_eq;
    use proptest::prelude::*;

    proptest! {
        #[test]
        fn to_spherical_and_back_is_reflective(
            x in -1e5_f64..1e5,
            y in -1e5_f64..1e5,
            z in -1e5_f64..1e5,
        ) {
            let point = na::Point3::new(x, y, z);
            let spherical_point = SphericalPoint::from_cartesian(&point);
            let from_spherical = spherical_point.into_cartesian();

            assert_relative_eq!(point, from_spherical, epsilon = 1e-6);
        }

        #[test]
        fn to_cartesian_and_back_is_reflective(
            range in 1e-1_f64..1e5,
            // -pi/2 to pi/2 because that's the built-in range for atan2
            //
            // The purpose here is so that we can normalize our angles and compare them directly
            // according to the correct signage / orientation.
            //
            // Otherwise we'd have an ambiguity for points in one of the 8 octets of the sphere -
            // where either azimuth or altitude have a negative cosine. We are unable, generally
            // speaking, to disambiguate this since there are always two pairs of angles that
            // produce the same point in a spherical system, and any convention we pick for that
            // would be just that -- a convention, and isn't necessary for the definition of a
            // spherical point.
            azimuth in -(std::f64::consts::PI)/2.0..std::f64::consts::PI/2.0,
            altitude in -(std::f64::consts::PI)/2.0..std::f64::consts::PI/2.0,
        ) {
            let point = SphericalPoint::new(range, azimuth, altitude);
            let cartesian_point = point.into_cartesian();
            let from_cartesian = SphericalPoint::from_cartesian(&cartesian_point);

            let [r1, z1, a1] = point.coords;
            let [r2, z2, a2] = from_cartesian.coords;

            // Normalize angular ranges back within -pi/2 to pi/2
            let z1 = f64::atan2(z1.sin(), z1.cos());
            let z2 = f64::atan2(z2.sin(), z2.cos());

            let a1 = f64::atan2(a1.sin(), a1.cos());
            let a2 = f64::atan2(a2.sin(), a2.cos());

            assert_relative_eq!(r1, r2, epsilon = 1e-6);
            assert_relative_eq!(z1, z2, epsilon = 1e-9);
            assert_relative_eq!(a1, a2, epsilon = 1e-9);
        }
    }
}
