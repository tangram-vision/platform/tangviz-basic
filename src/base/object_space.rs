// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! A module to parse an object space json file for use in the Tangram Vision calibration system.

use super::{ObjectPoint, ObjectPointVariance};
use std::{collections::HashMap, fs::read_to_string, path::Path};

use anyhow::Result;
use conv::ValueFrom;
use nalgebra::{vector, Point3};
use serde::{Deserialize, Serialize};
use strum::IntoEnumIterator;

/// A type representing the possible object-space configurations.
///
/// Configurations comprise a detector-descriptor pairing for each component type within the
/// system. This means that cameras will have a distinct detector / descriptor pairing from e.g.
/// LiDAR components.
///
/// At the present time, only cameras are currently supported.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct ObjectSpaceConfig {
    /// Configuration for camera components.
    pub camera: DetectorDescriptor,
}

impl ObjectSpaceConfig {
    /// Returns a mapping of object space ID to an object space point and variance. The ordering
    /// of object spaces points w.r.t. depends on the conventions established by the individual
    /// Detector/Descriptor pairings. The behavior of this function is such that the a one-to-one
    /// correspondence can be formed between object space points returned here and image space
    /// points returned by a detector (when e.g. called using `Checkmate`). See `Detector` docs
    /// for more details about ordering convention.
    pub fn object_space_map(&self) -> Result<HashMap<usize, (ObjectPoint, ObjectPointVariance)>> {
        let mut object_space = HashMap::new();

        match &self.camera.descriptor {
            Descriptor::DetectorDefined => match self.camera.detector {
                Detector::Checkerboard {
                    width,
                    height,
                    checker_length,
                    variances,
                }
                | Detector::Markerboard {
                    width,
                    height,
                    checker_length,
                    variances,
                    ..
                } => {
                    // Plain checker boards start at the top left and move right and
                    // then down. Charuco Markerboards start at the bottom left and
                    // move right then up.
                    let y_iter: Box<dyn ExactSizeIterator<Item = usize>> =
                        match self.camera.detector {
                            Detector::Checkerboard { .. } => Box::new(0..height),
                            Detector::Markerboard { .. } => Box::new((0..height).rev()),
                            _ => unreachable!(),
                        };

                    // Populate our board with row-wise coordinates
                    let mut id = 0;
                    for y in y_iter {
                        for x in 0..width {
                            let x_as_f64 = f64::value_from(x)?;
                            let y_as_f64 = f64::value_from(y)?;
                            object_space.insert(
                                id,
                                (
                                    ObjectPoint(Point3::new(
                                        x_as_f64 * checker_length,
                                        y_as_f64 * checker_length,
                                        0.0,
                                    )),
                                    ObjectPointVariance {
                                        x_variance: variances[0],
                                        y_variance: variances[1],
                                        z_variance: variances[2],
                                    },
                                ),
                            );
                            id += 1;
                        }
                    }
                }
                Detector::AprilGrid {
                    width,
                    height,
                    marker_length,
                    tag_spacing,
                    variances,
                    ..
                } => {
                    let marker_step = marker_length * (1.0 + tag_spacing);
                    let mut id = 0;
                    // Start at the bottom row and move up
                    for marker_y_idx in (0..height).rev() {
                        // Start at the left and move right
                        for marker_x_idx in 0..width {
                            // Corner order is CW starting in the bottom right
                            for corner_off in [
                                vector![1.0, 1.0],
                                vector![0.0, 1.0],
                                vector![0.0, 0.0],
                                vector![1.0, 0.0],
                            ]
                            .iter()
                            {
                                let marker_center = vector![
                                    marker_x_idx as f64 * marker_step,
                                    marker_y_idx as f64 * marker_step
                                ];
                                let marker = marker_center + corner_off * marker_length;
                                object_space.insert(
                                    id,
                                    (
                                        ObjectPoint(Point3::new(marker.x, marker.y, 0.0)),
                                        ObjectPointVariance {
                                            x_variance: variances[0],
                                            y_variance: variances[1],
                                            z_variance: variances[2],
                                        },
                                    ),
                                );
                                id += 1;
                            }
                        }
                    }
                }
                _ => {
                    return Err(anyhow::anyhow!(
                    "Descriptor has an incompatible Detector. Cannot parse object space properly."
                ))
                }
            },
            Descriptor::TargetList { targets } => {
                for target in targets.iter() {
                    object_space.insert(
                        target.id,
                        (
                            ObjectPoint(Point3::new(
                                target.coordinates[0],
                                target.coordinates[1],
                                target.coordinates[2],
                            )),
                            ObjectPointVariance {
                                x_variance: target.variances[0],
                                y_variance: target.variances[1],
                                z_variance: target.variances[2],
                            },
                        ),
                    );
                }
            }
        };

        Ok(object_space)
    }
}

/// A type representing the detector-descriptor pairing for a camera.
///
/// Not every variant of detector and descriptor is guaranteed to be semantically valid when paired
/// together.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct DetectorDescriptor {
    /// The detector to use on observations from the parent component type.
    pub detector: Detector,

    /// The descriptor to define the object-space we are observing in observations with the
    /// detector.
    #[serde(default)]
    pub descriptor: Descriptor,
}

/// A type describing the possible detectors that can be used on component observations, and their
/// parameters.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum Detector {
    /// Detector for a checkerboard within a camera image.
    ///
    /// Valid descriptors are:
    ///
    /// - `"detector_defined"`
    Checkerboard {
        /// Number of checker squares horizontally on the board.
        width: usize,
        /// Number of checker squares vertically on the board.
        height: usize,
        /// Size of one edge of a checker square, in metres.
        checker_length: f64,
        /// The variances (X/Y/Z) of object-space points, in metres^2.
        variances: [f64; 3],
    },

    /// Detector made of a combination of checkerboard and fiducial markers. An example of this kind
    /// of target is OpenCV's ChAruco board.
    ///
    /// Valid descriptors are:
    ///
    /// - `"detector_defined"`
    Markerboard {
        /// Number of checker squares horizontally on the board.
        width: usize,
        /// Number of checker squares vertically on the board.
        height: usize,
        /// Size of one edge of a checker square, in metres.
        checker_length: f64,
        /// Size of one edge of the ArUco markers in the board.
        ///
        /// Should be smaller than `edge_length`.
        marker_length: f64,
        /// The dictionary that the Aruco is derived from. See [SupportedMarkerDictionary] for the list
        /// of valid strings (derived from the enum names).
        marker_dictionary: String,
        /// The variances (X/Y/Z) of object-space points, in metres^2.
        variances: [f64; 3],
    },

    /// Detector for an AprilGrid. AprilGrids are checkerboard-based targets which use AprilTags
    /// as fiducials which come from the Kalibr project. The layout and specification of AprilGrids
    /// is distinct from a markerboard despite some similarity. We don't currently allow for
    /// custom ID orders. We assume AprilTag IDs in the grid start at zero and count upward
    /// monotonically.
    ///
    /// The corresponding object space convention is as follows. AprilTag IDs start at 0 in the
    /// lower right corner and increase as you go left-to-right, then upward. Each tag has four
    /// corners whose object space IDs start at 4 * the AprilTag ID and then increase as you go
    /// clockwise starting in the lower left corner. This convention differs from the corner
    /// ordering the `Markers` detector because the tags in an AprilGrid are rotated 180deg
    /// relative to the canonical OpenCV convention. The order is illustrated below:
    ///
    /// 10---11 14---15
    /// | id2 | | id3 |
    /// |     | |     |
    /// 9-----8 13---12
    /// 2-----3 6-----7
    /// | id0 | | id1 |
    /// |     | |     |
    /// 1-----0 5-----4
    ///
    /// Valid descriptors are:
    ///
    /// - `"detector_defined"`
    AprilGrid {
        /// Number of AprilTags horizontally on the board.
        width: usize,
        /// Number of AprilTags vertically on the board.
        height: usize,
        /// Metric size of one edge of the AprilTags in the board.
        marker_length: f64,
        /// The space between the tags in fraction of the edge size [0..1] (kalibr default: 0.3),
        tag_spacing: f64,
        /// The dictionary that the AprilTag is derived from. See [SupportedMarkerDictionary] for the list
        /// of valid strings (derived from the enum names). Only April dictionaries are valid.
        marker_dictionary: String,
        /// The variances (X/Y/Z) of object-space points, in metres^2.
        variances: [f64; 3],
    },

    /// Detector for a target made of fiducial markers.
    ///
    /// Markers are described by their corners, not their centers. This is an important distinction
    /// to make when creating object space files from marker dictionaries; failure to use corners
    /// over center coordinates will result in failed detections.These markers don't have to
    /// be in any particular order or configuration; the only requirements here is that they
    /// are all the same size and from the same dictionary.
    ///
    /// OpenCV always reports corners in the following order:
    ///
    /// 0-----1
    /// |  _  |
    /// |     |
    /// 3-----2
    ///
    /// This allows us to assign unique IDs to every Target (e.g. corner) as follows:
    ///
    /// target_id = marker_id * 4 + corner_id
    ///
    /// Valid descriptors for Markers are:
    ///
    /// - `"target_list"`
    Markers {
        /// The real-world length of an individual fiducial marker, in metres.
        marker_length: f64,
        /// The dictionary or dictionary that the fiducial markers are derived from. See
        /// [SupportedMarkerDictionary] for the list of valid dictionaries.
        marker_dictionary: String,
    },
}

/// List of supported AprilTag dictionary variants
#[derive(
    Debug,
    strum_macros::Display,
    strum_macros::EnumString,
    strum_macros::EnumIter,
    strum_macros::AsRefStr,
    strum_macros::IntoStaticStr,
)]
pub enum SupportedMarkerDictionary {
    /// 4x4 bit Aruco containing 50 markers.
    Aruco4x4_50,
    /// 4x4 bit Aruco containing 100 markers.
    Aruco4x4_100,
    /// 4x4 bit Aruco containing 250 markers.
    Aruco4x4_250,
    /// 4x4 bit Aruco containing 1000 markers.
    Aruco4x4_1000,
    /// 5x5 bit Aruco containing 50 markers.
    Aruco5x5_50,
    /// 5x5 bit Aruco containing 100 markers.
    Aruco5x5_100,
    /// 5x5 bit Aruco containing 250 markers.
    Aruco5x5_250,
    /// 5x5 bit Aruco containing 1000 markers.
    Aruco5x5_1000,
    /// 6x6 bit Aruco containing 50 markers.
    Aruco6x6_50,
    /// 6x6 bit Aruco containing 100 markers.
    Aruco6x6_100,
    /// 6x6 bit Aruco containing 250 markers.
    Aruco6x6_250,
    /// 6x6 bit Aruco containing 1000 markers.
    Aruco6x6_1000,
    /// 7x7 bit Aruco containing 50 markers.
    Aruco7x7_50,
    /// 7x7 bit Aruco containing 100 markers.
    Aruco7x7_100,
    /// 7x7 bit Aruco containing 250 markers.
    Aruco7x7_250,
    /// 7x7 bit Aruco containing 1000 markers.
    Aruco7x7_1000,
    /// 5x5 bit Aruco containing the original generated marker library.
    ArucoOriginal,
    /// 4x4 bit Apriltag containing 20 markers. Mminimum hamming dist between any two codes is 5.
    Apriltag16h5,
    /// 5x5 bit Apriltag containing 35 markers. Mminimum hamming dist between any two codes is 9.
    Apriltag25h9,
    /// 6x6 bit Apriltag containing 2320 markers. Mminimum hamming dist between any two codes is
    /// 10.
    Apriltag36h10,
    /// 6x6 bit Apriltag containing 587 markers. Mminimum hamming dist between any two codes is
    /// 11.
    Apriltag36h11,
    /// 6x6 bit Apriltag containing 587 markers. Mminimum hamming dist between any two codes is 11.
    /// Every marker has a border width of 2; this is the only main differentiator between Kalibr
    /// and other Apriltag types.
    ApriltagKalibr,
}

/// A target describing a point in 3D space.
///
/// To be used within certain descriptors.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct Target {
    /// The target's unique identifier.
    pub id: usize,

    /// The Cartesian coordinates (X/Y/Z) representing the target's position.
    pub coordinates: [f64; 3],

    /// The variances associated with the Cartesian coordinates (X/Y/Z) representing the
    /// uncertainty in the target's position.
    pub variances: [f64; 3],
}

/// A type describing the possible descriptors for the object-space detected in an image.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
#[serde(untagged)]
pub enum Descriptor {
    /// The descriptor is to be defined in terms of the detector and its parameters.
    DetectorDefined,

    /// The descriptor is to be defined in terms of a list of identified points & variances.
    TargetList {
        /// The list of targets (id / coords / variances) that describe the object-space that the
        /// detector detects.
        targets: Vec<Target>,
    },
}

impl Default for Descriptor {
    fn default() -> Self {
        Descriptor::DetectorDefined
    }
}

/// A function to read in the object space config from a JSON file at the given path.
pub fn read_object_space_config<P>(osc_path: P) -> Result<ObjectSpaceConfig>
where
    P: AsRef<Path>,
{
    let config = serde_json::from_str::<ObjectSpaceConfig>(&read_to_string(osc_path)?)?;

    validate_object_space_config(config)
}

/// A function to run extra (unrelated to deserialization) validation on object space configs.
pub fn validate_object_space_config(config: ObjectSpaceConfig) -> Result<ObjectSpaceConfig> {
    match &config.camera.detector {
        Detector::Checkerboard { .. } => match &config.camera.descriptor {
            Descriptor::DetectorDefined => Ok(()),
            _ => Err(anyhow::anyhow!(
                "The checkerboard detector only supports a 'detector_defined' descriptor."
            )),
        },
        Detector::Markerboard {
            marker_dictionary, ..
        } => {
            SupportedMarkerDictionary::iter().find(|f| f.as_ref() == marker_dictionary).ok_or_else(||
                anyhow::anyhow!(
                    "The marker 'dictionary' is not one of the supported dictionary types. Provided dictionary: {}",
                    &marker_dictionary
                )
            )?;
            match &config.camera.descriptor {
                Descriptor::DetectorDefined => Ok(()),
                _ => Err(anyhow::anyhow!(
                    "The Markerboard detector only supports a 'detector_defined' descriptor."
                )),
            }
        }
        Detector::AprilGrid {
            marker_dictionary, ..
        } => {
            /// All supported dictionaries for AprilTags
            const APRILTAGS: [SupportedMarkerDictionary; 5] = [
                SupportedMarkerDictionary::Apriltag16h5,
                SupportedMarkerDictionary::Apriltag25h9,
                SupportedMarkerDictionary::Apriltag36h10,
                SupportedMarkerDictionary::Apriltag36h11,
                SupportedMarkerDictionary::ApriltagKalibr,
            ];
            APRILTAGS.iter().find(|f| f.as_ref() == marker_dictionary).ok_or_else(||
                anyhow::anyhow!(
                    "The marker 'dictionary' is not one of the supported dictionary types. AprilGrids \
                    require an AprilTag dictionary. Provided dictionary: {}",
                    &marker_dictionary
                )
            )?;
            match &config.camera.descriptor {
                Descriptor::DetectorDefined => Ok(()),
                _ => Err(anyhow::anyhow!(
                    "The AprilGrid detector only supports a 'detector_defined' descriptor."
                )),
            }
        }
        Detector::Markers {
            marker_dictionary, ..
        } => {
            SupportedMarkerDictionary::iter().find(|f| f.as_ref() == marker_dictionary).ok_or_else(||
                anyhow::anyhow!(
                    "The marker 'dictionary' is not one of the supported dictionary types. Provided dictionary: {}",
                    &marker_dictionary
                )
            )?;

            match &config.camera.descriptor {
                Descriptor::TargetList { .. } => Ok(()),
                _ => Err(anyhow::anyhow!(
                    "The Markers detector only supports a 'target_list' descriptor."
                )),
            }
        }
    }?;

    Ok(config)
}

#[cfg(test)]
mod tests {
    use super::*;
    use opencv::prelude::*;

    #[test]
    fn valid_checkerboard_is_ok() {
        read_object_space_config("fixtures/detector_checkerboard.json").unwrap();
    }

    #[test]
    fn valid_markerboard_is_ok() {
        read_object_space_config("fixtures/detector_markerboard.json").unwrap();
    }

    #[test]
    fn valid_markers_is_ok() {
        read_object_space_config("fixtures/detector_markers.json").unwrap();
    }

    #[test]
    fn valid_aprilgrid_is_ok() {
        read_object_space_config("fixtures/detector_aprilgrid.json").unwrap();
    }

    #[test]
    fn invalid_json_does_not_parse() {
        read_object_space_config("fixtures/detector_invalid.json").unwrap_err();
    }

    #[test]
    fn file_that_does_not_exist_is_err() {
        read_object_space_config("fixtures/i-do-not-exist.png").unwrap_err();
    }

    #[test]
    fn aruco_april_corner_order() {
        let dicts_and_bits = [
            (
                opencv::aruco::get_predefined_dictionary(
                    opencv::aruco::PREDEFINED_DICTIONARY_NAME::DICT_6X6_250,
                )
                .unwrap(),
                1,
            ),
            (
                opencv::aruco::get_predefined_dictionary(
                    opencv::aruco::PREDEFINED_DICTIONARY_NAME::DICT_APRILTAG_36h11,
                )
                .unwrap(),
                2,
            ),
        ];

        let corners = dicts_and_bits
            .iter()
            .map(|(dict, bits)| {
                // Build a mat which holds just a marker
                let marker_mat = {
                    let mut marker_mat = opencv::core::Mat::new_rows_cols_with_default(
                        480,
                        480,
                        opencv::core::CV_8UC1,
                        opencv::core::Scalar::all(255.0),
                    )
                    .unwrap();
                    opencv::aruco::draw_marker(dict, 0, 480, &mut marker_mat, *bits).unwrap();
                    marker_mat
                };

                // Matte the marker on a white field with a 100px border so the detector
                // can run correctly
                let matte_mat = opencv::core::Mat::new_rows_cols_with_default(
                    680,
                    680,
                    opencv::core::CV_8UC1,
                    opencv::core::Scalar::all(255.0),
                )
                .unwrap();
                marker_mat
                    .copy_to(
                        &mut opencv::core::Mat::roi(
                            &matte_mat,
                            opencv::core::Rect::new(100, 100, 480, 480),
                        )
                        .unwrap(),
                    )
                    .unwrap();

                let mut marker_corners =
                    opencv::core::Vector::<opencv::types::VectorOfPoint2f>::new();
                let mut marker_ids = opencv::core::Vector::<i32>::new();
                let mut rejected_points =
                    opencv::core::Vector::<opencv::types::VectorOfPoint2f>::new();
                let mut params = opencv::aruco::DetectorParameters::create().unwrap();
                params.set_corner_refinement_method(opencv::aruco::CORNER_REFINE_SUBPIX);
                params.set_marker_border_bits(*bits);
                opencv::aruco::detect_markers(
                    &matte_mat,
                    dict,
                    &mut marker_corners,
                    &mut marker_ids,
                    &params,
                    &mut rejected_points,
                    &opencv::core::no_array().unwrap(),
                    &opencv::core::no_array().unwrap(),
                )
                .unwrap();
                assert_eq!(marker_corners.len(), 1);

                let corners_flt = marker_corners.get(0).unwrap();
                corners_flt
                    .iter()
                    .map(|f| nalgebra::vector![f.x as i32, f.y as i32])
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();

        // Aruco
        for corners in corners.iter().cloned() {
            let min_x = corners
                .iter()
                .map(|c| c.x)
                .min_by(|a, b| a.partial_cmp(b).unwrap())
                .unwrap();
            let max_x = corners
                .iter()
                .map(|c| c.x)
                .max_by(|a, b| a.partial_cmp(b).unwrap())
                .unwrap();
            let min_y = corners
                .iter()
                .map(|c| c.y)
                .min_by(|a, b| a.partial_cmp(b).unwrap())
                .unwrap();
            let max_y = corners
                .iter()
                .map(|c| c.y)
                .max_by(|a, b| a.partial_cmp(b).unwrap())
                .unwrap();
            assert_eq!(corners[0].x, min_x);
            assert_eq!(corners[0].y, min_y);
            assert_eq!(corners[1].x, max_x);
            assert_eq!(corners[1].y, min_y);
            assert_eq!(corners[2].x, max_x);
            assert_eq!(corners[2].y, max_y);
            assert_eq!(corners[3].x, min_x);
            assert_eq!(corners[3].y, max_y);
        }
    }
}
