// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! Defines `Component`, an abstraction for distinct sources of data.
//!
//! They run the gamut from:
//!
//! - physical hardware: camera, IMU
//! - virtual hardware: clocks, depth derived from active stereo
//!
//! Each component posesses a UUID and a root UUID. The self-UUID serves as a unique identifier
//! (hence the name). A root UUID is assigned per-device, and all related components share that
//! same root UUID. The component that acts as the root for a device is arbitrary.  For example, a
//! stereo depth system might assign its left IR camera as "root". All other components that make
//! up the system would be related to the left IR camera through their root UUIDs.
//!
//! This UUID relational scheme allows for the easy formation of Plexes, or relational graphs for
//! all components in space and time.

use crate::{
    base::intrinsics::{CameraIntrinsics, LidarIntrinsics},
    prelude::Sameness,
};
use derive_new::new;
use nalgebra as na;
use serde::{Deserialize, Serialize};
use std::cmp::PartialEq;
use tangviz_basic_macros::{PlexComponent, Sameness};
use uuid::Uuid;

/// Trait denoting the interfaces that need to be implemented by all component types.
///
/// The easiest way to implement this is to use `#[derive(PlexComponent)]`.
///
/// Note that this trait specifically incorporates all the common elements of component types, and
/// should be interpreted as the minimum subset of functionality that a type must adhere to be a
/// plex component.
pub trait PlexComponent {
    /// Gets the component's UUID.
    fn uuid(&self) -> &Uuid;

    /// Gets the UUID of the root component for the device that this component comes from.
    fn root_uuid(&self) -> &Uuid;

    /// Gets the (not-necessarily-unique) name of the component.
    fn name(&self) -> &str;
}

/// Type used for representing a standard full-frame camera.
#[derive(Clone, Debug, Deserialize, PartialEq, new, Serialize, PlexComponent, Sameness)]
pub struct Camera {
    /// The UUID for this particular component.
    uuid: Uuid,

    /// The UUID of the root component for this component's parent device.
    root_uuid: Uuid,

    /// A user-friendly name for the specific component.
    ///
    /// Does not have to be unique.
    name: String,

    /// The intrinsics that characterize this camera.
    pub intrinsics: CameraIntrinsics,

    /// Covariance matrix for the camera's intrinsics.
    ///
    /// # Order
    ///
    /// To interpret this covariance matrix properly, it is necessary to understand the order of the
    /// elements. We roughly try to order the columns and rows according to:
    ///
    /// 1. `f`
    /// 2. `cx`
    /// 3. `cy`
    /// 4. Distortions - the number of entries will depend on the number of values encoded in the
    ///    model.
    /// 5. Affinity - the number of entries will depend on the number of values encoded in the
    ///    model.
    ///
    /// NOTE: `width` and `height` are not estimated values, so they never have entries in the
    /// covariance matrix.
    ///
    pub covariance: na::DMatrix<f64>,

    /// The size of a pixel.
    ///
    /// This may be in metric space, or, if this is unavailable, may be 1 (i.e. units of pixels are
    /// required). This can sometimes be helpful when comparing cameras, as e.g. N-pixels of
    /// reprojection error isn't necessarily congruent or comparable between two models of camera.
    pub pixel_pitch: f64,
}

/// Type used for representing a LiDAR.
#[derive(Clone, Debug, Deserialize, PartialEq, new, Serialize, PlexComponent, Sameness)]
pub struct Lidar {
    /// The UUID for this particular component.
    uuid: Uuid,

    /// The UUID of the root component for this component's parent device.
    root_uuid: Uuid,

    /// A user-friendly name for the specific component.
    ///
    /// Does not have to be unique.
    name: String,

    /// The intrinsics that characterize this LiDAR.
    pub intrinsics: LidarIntrinsics,

    /// Covariance matrix for the LiDAR's intrinsics.
    ///
    /// # Order
    ///
    /// To interpret this covariance matrix properly, it is necessary to understand the order of the
    /// elements. We roughly try to order the columns and rows according to:
    ///
    /// 1. Range
    /// 2. Azimuth
    /// 3. Altitude
    ///
    /// In that exact order.
    pub covariance: na::DMatrix<f64>,
}

/// Type used for representing an anchor, which is a physical point in space.
/// Anchors are only spatial; any time constraints assigned to them are disregarded
/// (though there is no requirement against it)
#[derive(Clone, Debug, Deserialize, PartialEq, new, Serialize, PlexComponent, Sameness)]
pub struct Anchor {
    /// The UUID for this particular component.
    uuid: Uuid,

    /// The UUID of the root component for this component's parent device.
    root_uuid: Uuid,

    /// A user-friendly name for the specific component.
    ///
    /// Does not have to be unique.
    name: String,
}

/// Type enumerating the various kinds of components that can be stored within a plex.
#[derive(Clone, Debug, Deserialize, Serialize, PartialEq, Sameness)]
#[serde(rename_all = "snake_case")]
pub enum Component {
    /// Variant of component describing a full-frame camera.
    Camera(Camera),

    /// Variant of component describing a LiDAR.
    Lidar(Lidar),

    /// Variant of component describing an anchor (physical point in space)
    Anchor(Anchor),
}

impl Component {
    /// Gets a reference to the component's root UUID, which is the UUID of the reference
    /// component on a given device.
    pub fn root_uuid(&self) -> &Uuid {
        match self {
            Component::Camera(c) => c.root_uuid(),
            Component::Lidar(l) => l.root_uuid(),
            Component::Anchor(a) => a.root_uuid(),
        }
    }

    /// Gets a reference to the component's UUID, which is unique per component.
    pub fn uuid(&self) -> &Uuid {
        match self {
            Component::Camera(c) => c.uuid(),
            Component::Lidar(l) => l.uuid(),
            Component::Anchor(a) => a.uuid(),
        }
    }

    /// Gets a reference to the user-friendly name of the component.
    pub fn name(&self) -> &str {
        match self {
            Component::Camera(c) => c.name(),
            Component::Lidar(l) => l.name(),
            Component::Anchor(a) => a.name(),
        }
    }

    /// If the variant is a camera, return the camera. Returns `None` otherwise.
    pub fn as_camera(&self) -> Option<&Camera> {
        match self {
            Component::Camera(c) => Some(c),
            _ => None,
        }
    }

    /// If the variant is a mutable camera, return the camera. Returns `None` otherwise.
    pub fn as_camera_mut(&mut self) -> Option<&mut Camera> {
        match self {
            Component::Camera(c) => Some(c),
            _ => None,
        }
    }

    /// If the variant is a LiDAR, return the LiDAR. Returns `None` otherwise.
    pub fn as_lidar(&self) -> Option<&Lidar> {
        match self {
            Component::Lidar(l) => Some(l),
            _ => None,
        }
    }

    /// If the variant is a mutable LiDAR, return the LiDAR. Returns `None` otherwise.
    pub fn as_lidar_mut(&mut self) -> Option<&mut Lidar> {
        match self {
            Component::Lidar(l) => Some(l),
            _ => None,
        }
    }

    /// If the variant is an Anchor, return the Anchor. Returns `None` otherwise.
    pub fn as_anchor(&self) -> Option<&Anchor> {
        match self {
            Component::Anchor(a) => Some(a),
            _ => None,
        }
    }

    /// If the variant is a mutable Anchor, return the Anchor. Returns `None` otherwise.
    pub fn as_anchor_mut(&mut self) -> Option<&mut Anchor> {
        match self {
            Component::Anchor(a) => Some(a),
            _ => None,
        }
    }
}

/// Module containing proptest-specific "arbitrary" functions, which provide arbitrary / randomized
/// data for property-based testing.
#[cfg(any(test, feature = "proptest-support"))]
pub mod arbitrary {
    use super::*;
    use crate::prelude::Encodable;
    use nalgebra as na;
    use proptest::prelude::*;

    impl Arbitrary for Component {
        type Strategy = BoxedStrategy<Self>;
        type Parameters = (
            <Camera as Arbitrary>::Parameters,
            <Lidar as Arbitrary>::Parameters,
            <Anchor as Arbitrary>::Parameters,
        );

        fn arbitrary_with(args: Self::Parameters) -> Self::Strategy {
            let (camera_args, lidar_args, anchor_args) = args;
            prop_oneof![
                any_with::<Camera>(camera_args).prop_map(Component::Camera),
                any_with::<Lidar>(lidar_args).prop_map(Component::Lidar),
                any_with::<Anchor>(anchor_args).prop_map(Component::Anchor),
            ]
            .boxed()
        }
    }

    impl Arbitrary for Camera {
        type Strategy = BoxedStrategy<Self>;
        type Parameters = (
            <String as Arbitrary>::Parameters,
            <CameraIntrinsics as Arbitrary>::Parameters,
        );

        fn arbitrary_with(args: Self::Parameters) -> Self::Strategy {
            let (name_args, intrinsics_args) = args;

            let name_strategy = any_with::<String>(name_args);
            let intrinsics_strategy = any_with::<CameraIntrinsics>(intrinsics_args);
            let values_strategy = proptest::collection::vec(
                proptest::num::f64::POSITIVE | proptest::num::f64::ZERO,
                proptest::collection::size_range(1000..=1000),
            );
            let pixel_pitch = proptest::num::f64::POSITIVE;

            (
                name_strategy,
                intrinsics_strategy,
                values_strategy,
                pixel_pitch,
            )
                .prop_map(|(name, intrinsics, values, pixel_pitch)| {
                    let uuid = Uuid::new_v4();

                    let num_params = intrinsics.encoded_size();
                    let slice = &values[0..num_params.pow(2)];
                    let covariance = na::DMatrixSlice::from_slice(slice, num_params, num_params)
                        .symmetric_part();

                    Camera::new(uuid, uuid, name, intrinsics, covariance, pixel_pitch)
                })
                .boxed()
        }
    }

    impl Arbitrary for Lidar {
        type Strategy = BoxedStrategy<Self>;
        type Parameters = (
            <String as Arbitrary>::Parameters,
            <LidarIntrinsics as Arbitrary>::Parameters,
        );

        fn arbitrary_with(args: Self::Parameters) -> Self::Strategy {
            let (name_args, intrinsics_args) = args;

            let name_strategy = any_with::<String>(name_args);
            let intrinsics_strategy = any_with::<LidarIntrinsics>(intrinsics_args);
            let values_strategy = proptest::collection::vec(
                proptest::num::f64::POSITIVE | proptest::num::f64::ZERO,
                proptest::collection::size_range(1000..=1000),
            );

            (name_strategy, intrinsics_strategy, values_strategy)
                .prop_map(|(name, intrinsics, values)| {
                    let uuid = Uuid::new_v4();

                    let num_params = intrinsics.encoded_size();
                    let slice = &values[0..num_params.pow(2)];
                    let covariance = na::DMatrixSlice::from_slice(slice, num_params, num_params)
                        .symmetric_part();

                    Lidar::new(uuid, uuid, name, intrinsics, covariance)
                })
                .boxed()
        }
    }

    impl Arbitrary for Anchor {
        type Strategy = BoxedStrategy<Self>;
        type Parameters = <String as Arbitrary>::Parameters;

        fn arbitrary_with(args: Self::Parameters) -> Self::Strategy {
            any_with::<String>(args)
                .prop_map(|name| {
                    let uuid = Uuid::new_v4();
                    Anchor::new(uuid, uuid, name)
                })
                .boxed()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        base::intrinsics::{AffinityModel, CameraIntrinsics, DistortionModel, ProjectionModel},
        prelude::*,
    };
    use proptest::prelude::*;

    #[test]
    fn component_constructor() {
        let uuid = Uuid::new_v4();
        let intrinsics = CameraIntrinsics {
            projection: ProjectionModel::Pinhole {
                f: 300.0,
                cx: 320.0,
                cy: 240.0,
            },
            distortion: DistortionModel::NoDistortion,
            affinity: AffinityModel::NoAffinity,
            width: 640,
            height: 480,
        };

        let name = String::from("Camera 1");

        let camera_one = Component::Camera(Camera::new(
            uuid,
            uuid,
            name.clone(),
            intrinsics,
            na::DMatrix::zeros(3, 3),
            1.0,
        ));
        assert_eq!(camera_one.uuid(), camera_one.root_uuid());
        assert_eq!(camera_one.name(), name);
    }

    proptest! {
        #[test]
        fn component_ser_de_is_reflective(component in any::<Component>()) {
            let serialized = serde_json::to_string(&component).unwrap();
            let deserialized = serde_json::from_str(&serialized).unwrap();

            prop_assert!(component.is_same(&deserialized));
        }
    }
}
