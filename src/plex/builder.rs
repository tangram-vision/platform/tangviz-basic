// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------

//! Builder type for constructing plexes.

use super::{
    component::Component,
    plex_type::{Plex, PlexGraph},
    relation::{Relation, SpatialConstraint, TemporalConstraint},
};
use anyhow::Result;
use chrono::Utc;
use petgraph::graph::{DefaultIx, Graph, NodeIndex};
use std::collections::HashMap;
use thiserror::Error;
use uuid::Uuid;

//#region Type declarations

/// Type describing errors that can happen when constructing a plex.
#[derive(Error, Debug, PartialEq)]
pub enum PlexConstructionError {
    /// A component does not exist within the builder even though a UUID to that component was
    /// provided.
    #[error("The component with UUID {0} does not exist in the plex.")]
    ComponentDoesNotExist(Uuid),

    /// A component with the specified UUID was added to the builder but a component with that UUID
    /// already exists within the builder.
    #[error("The component with UUID {0} already exists.")]
    ComponentAlreadyExists(Uuid),

    /// The constraint being added already exists within the builder.
    #[error("The constraint between {0} and {1} already exists.")]
    ConstraintAlreadyExists(Uuid, Uuid),

    /// The constraint being added is a self-constraint, which is not allowed.
    #[error("First and second UUIDs between a constraint must not alias.")]
    ConstraintMustBeBetweenDistinctComponents,
}

/// Builder type for constructing plexes.
#[derive(Debug, Clone)]
pub struct PlexBuilder {
    /// Graph structure that defines the core part of the plex.
    graph: PlexGraph,

    /// Map from UUID -> Node index.
    ///
    /// Node indices change in the graph, but the UUIDs of the nodes (components) themselves do
    /// not.
    node_from_uuid: HashMap<Uuid, NodeIndex<DefaultIx>>,
}

//#endregion

//#region PlexBuilder impl

impl Default for PlexBuilder {
    fn default() -> Self {
        PlexBuilder::new()
    }
}

impl From<Plex> for PlexBuilder {
    fn from(plex: Plex) -> Self {
        let Plex {
            graph,
            node_from_uuid,
            ..
        } = plex;

        Self {
            graph,
            node_from_uuid,
        }
    }
}

impl PlexBuilder {
    /// Constructs a new (empty) plex builder.
    pub fn new() -> Self {
        PlexBuilder {
            graph: Graph::new_undirected(),
            node_from_uuid: HashMap::new(),
        }
    }

    /// Predicate to check if a component with `this_uuid` is already contained within the builder.
    fn contains_uuid(&mut self, this_uuid: Uuid) -> bool {
        self.node_from_uuid.contains_key(&this_uuid)
    }

    /// Add a component to a plex after validating its uniqueness
    ///
    /// # Errors
    ///
    /// Returns [`PlexConstructionError::ComponentAlreadyExists`] if a component with the same UUID
    /// has already been added to the builder.
    pub fn add_component(
        &mut self,
        component: Component,
    ) -> Result<&mut Self, PlexConstructionError> {
        let this_uuid = *component.uuid();
        if self.contains_uuid(this_uuid) {
            Err(PlexConstructionError::ComponentAlreadyExists(this_uuid))
        } else {
            let node_idx = self.graph.add_node(component);
            self.node_from_uuid.insert(this_uuid, node_idx);
            Ok(self)
        }
    }

    /// Removes a component from the plex, if it exists.
    pub fn remove_component(&mut self, uuid: &Uuid) -> &mut Self {
        if let Some(node) = self.node_from_uuid.get(uuid) {
            self.graph.remove_node(*node);
            self.rebuild_node_uuid();
        }
        self
    }

    /// Rebuilds the uuid-to-node look-up table. This must be done whenever a node
    /// is removed.
    fn rebuild_node_uuid(&mut self) {
        self.node_from_uuid.clear();
        for ni in self.graph.node_indices() {
            let node = &self.graph[ni];
            self.node_from_uuid.insert(*node.uuid(), ni);
        }
    }

    /// Adds a spatial constraint to the plex, if one has not already been added.
    ///
    /// # Errors
    ///
    /// Returns [`PlexConstructionError::ComponentDoesNotExist`] if the UUIDs in the constraint do
    /// not correspond to any components that have previously been added to the builder.
    ///
    /// Returns [`PlexConstructionError::ConstraintAlreadyExists`] if a spatial relation between the
    /// provided UUIDs has already been added to the builder.
    ///
    /// Returns [`PlexConstructionError::ConstraintMustBeBetweenDistinctComponents`] if the first and
    /// second UUID provided are the same (i.e. they alias one another). UUIDs passed into this
    /// function must be distinct!
    pub fn add_spatial_constraint(
        &mut self,
        constraint: SpatialConstraint,
    ) -> Result<&mut Self, PlexConstructionError> {
        let node_from = self.node_from_uuid.get(&constraint.from).ok_or(
            PlexConstructionError::ComponentDoesNotExist(constraint.from),
        )?;

        let node_to = self
            .node_from_uuid
            .get(&constraint.to)
            .ok_or(PlexConstructionError::ComponentDoesNotExist(constraint.to))?;

        if constraint.from == constraint.to {
            return Err(PlexConstructionError::ConstraintMustBeBetweenDistinctComponents);
        }

        if let Some(edge_idx) = self.graph.find_edge(*node_from, *node_to) {
            // Safe to unwrap here since we just got the edge_idx from the graph itself and know it
            // exists.
            match self.graph.remove_edge(edge_idx).unwrap() {
                Relation::Time(time) => {
                    self.graph.add_edge(
                        *node_from,
                        *node_to,
                        Relation::SpaceAndTime {
                            space: constraint,
                            time,
                        },
                    );
                    Ok(self)
                }
                relation => {
                    // Yes, this is dumb but petgraph otherwise will not give us the relation as an
                    // owned type so we need to remove and immediately add it back.
                    self.graph.add_edge(*node_from, *node_to, relation);
                    Err(PlexConstructionError::ConstraintAlreadyExists(
                        constraint.from,
                        constraint.to,
                    ))
                }
            }
        } else {
            self.graph
                .add_edge(*node_from, *node_to, Relation::Space(constraint));
            Ok(self)
        }
    }

    /// Removes a spatial constraint between two components in the plex, if it exists.
    ///
    /// The ordering of the first and second components do not matter, as the builder will find the
    /// appropriate constraint. In other words, knowing the to/from order of the constraint isn't
    /// necessary.
    pub fn remove_spatial_constraint(
        &mut self,
        first_component: &Uuid,
        second_component: &Uuid,
    ) -> &mut Self {
        let node_1 = self.node_from_uuid.get(first_component);
        let node_2 = self.node_from_uuid.get(second_component);

        if node_1.is_none() || node_2.is_none() {
            return self;
        }

        let node_1 = node_1.unwrap();
        let node_2 = node_2.unwrap();

        if let Some(edge) = self.graph.find_edge(*node_1, *node_2) {
            if let Some(relation) = self.graph.remove_edge(edge) {
                match relation {
                    Relation::Space(_) => (),
                    Relation::Time(time) | Relation::SpaceAndTime { time, .. } => {
                        self.graph.add_edge(*node_1, *node_2, Relation::Time(time));
                    }
                }
            }
        }
        self
    }

    /// Adds a temporal constraint to the plex.
    ///
    /// # Errors
    ///
    /// Returns [`PlexConstructionError::ComponentDoesNotExist`] if the UUIDs in the constraint do
    /// not correspond to any components that have previously been added to the builder.
    ///
    /// Returns [`PlexConstructionError::ConstraintAlreadyExists`] if a temporal relation between the
    /// provided UUIDs has already been added to the builder.
    ///
    /// Returns [`PlexConstructionError::ConstraintMustBeBetweenDistinctComponents`] if the first and
    /// second UUID provided are the same (i.e. they alias one another). UUIDs passed into this
    /// function must be distinct!
    pub fn add_temporal_constraint(
        &mut self,
        constraint: TemporalConstraint,
    ) -> Result<&mut Self, PlexConstructionError> {
        let node_from = self.node_from_uuid.get(&constraint.from).ok_or(
            PlexConstructionError::ComponentDoesNotExist(constraint.from),
        )?;

        let node_to = self
            .node_from_uuid
            .get(&constraint.to)
            .ok_or(PlexConstructionError::ComponentDoesNotExist(constraint.to))?;

        if constraint.from == constraint.to {
            return Err(PlexConstructionError::ConstraintMustBeBetweenDistinctComponents);
        }

        if let Some(edge_idx) = self.graph.find_edge(*node_from, *node_to) {
            // Safe to unwrap here since we just got the edge_idx from the graph itself and know it
            // exists.
            match self.graph.remove_edge(edge_idx).unwrap() {
                Relation::Space(space) => {
                    self.graph.add_edge(
                        *node_from,
                        *node_to,
                        Relation::SpaceAndTime {
                            space,
                            time: constraint,
                        },
                    );
                    Ok(self)
                }
                relation => {
                    // Yes, this is dumb but petgraph otherwise will not give us the relation as an
                    // owned type so we need to remove and immediately add it back.
                    self.graph.add_edge(*node_from, *node_to, relation);
                    Err(PlexConstructionError::ConstraintAlreadyExists(
                        constraint.from,
                        constraint.to,
                    ))
                }
            }
        } else {
            self.graph
                .add_edge(*node_from, *node_to, Relation::Time(constraint));
            Ok(self)
        }
    }

    /// Removes a temporal constraint between two components in the plex, if it exists.
    ///
    /// The ordering of the first and second components do not matter, as the builder will find the
    /// appropriate constraint. In other words, knowing the to/from order of the constraint isn't
    /// necessary.
    pub fn remove_temporal_constraint(
        &mut self,
        first_component: &Uuid,
        second_component: &Uuid,
    ) -> &mut Self {
        let node_1 = self.node_from_uuid.get(first_component);
        let node_2 = self.node_from_uuid.get(second_component);

        if node_1.is_none() || node_2.is_none() {
            return self;
        }

        let node_1 = node_1.unwrap();
        let node_2 = node_2.unwrap();

        if let Some(edge) = self.graph.find_edge(*node_1, *node_2) {
            if let Some(relation) = self.graph.remove_edge(edge) {
                match relation {
                    Relation::Time(_) => (),
                    Relation::Space(space) | Relation::SpaceAndTime { space, .. } => {
                        self.graph
                            .add_edge(*node_1, *node_2, Relation::Space(space));
                    }
                }
            }
        }
        self
    }

    /// Add a complete plex to this builder. Useful when adding pre-defined component groups e.g. a
    /// RealSense.
    ///
    /// Note: rhs can be a consumable PlexBuilder, or another Plex
    ///
    pub fn join<T: Into<Self>>(&mut self, rhs: T) -> Result<&mut Self, PlexConstructionError> {
        let builder_to_join: PlexBuilder = rhs.into();

        // There is an inherent assumption in this UUID check that the Plex being joined to Self has also
        // been validated by a PlexBuilder.
        for (uuid, _) in builder_to_join.node_from_uuid.iter().by_ref() {
            if self.contains_uuid(*uuid) {
                return Err(PlexConstructionError::ComponentAlreadyExists(*uuid));
            }
        }

        // Add all components and constraints, now that we've confirmed they're unique.
        let (nodes, edges) = builder_to_join.graph.into_nodes_edges();
        for n in nodes {
            let this_uuid = *n.weight.uuid();
            let node_idx = self.graph.add_node(n.weight);
            self.node_from_uuid.insert(this_uuid, node_idx);
        }
        for e in edges {
            self.graph.add_edge(e.source(), e.target(), e.weight);
        }

        Ok(self)
    }

    /// Consume this builder object and produce a plex.
    pub fn build(self) -> Plex {
        self.build_with_parts(Uuid::new_v4(), Utc::now().timestamp_nanos())
    }

    /// Consume this builder object and produce a plex with the given UUID and creation timestamp.
    pub fn build_with_parts(self, uuid: Uuid, creation_timestamp: i64) -> Plex {
        let PlexBuilder {
            graph,
            node_from_uuid,
        } = self;

        Plex {
            graph,
            node_from_uuid,
            uuid,
            creation_timestamp,
        }
    }
}

//#endregion

//#region Tests

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        base::{
            extrinsics::{ExtrinsicCovariance, Extrinsics},
            intrinsics::{AffinityModel, CameraIntrinsics, DistortionModel, ProjectionModel},
        },
        plex::{
            component::{Camera, Component, PlexComponent},
            relation::{Synchronization, TemporalConstraint},
        },
        sameness::Sameness,
    };
    use approx::assert_relative_eq;
    use nalgebra::{DMatrix, Isometry3, SMatrix};

    fn mock_intrinsics() -> CameraIntrinsics {
        CameraIntrinsics {
            projection: ProjectionModel::Pinhole {
                f: 300.0,
                cx: 320.0,
                cy: 240.0,
            },
            distortion: DistortionModel::NoDistortion,
            affinity: AffinityModel::NoAffinity,
            width: 640,
            height: 480,
        }
    }

    #[test]
    fn adding_same_component_twice_fails() {
        let camera_one = Component::Camera(Camera::new(
            Uuid::new_v4(),
            Uuid::new_v4(),
            String::from("cam"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let mut builder = PlexBuilder::new();
        builder
            .add_component(camera_one.clone())
            .expect("First addition of component failed.");

        let res = builder.add_component(camera_one);

        assert!(res.is_err());

        match res {
            Err(PlexConstructionError::ComponentAlreadyExists(_)) => (),
            _ => unreachable!(),
        }
    }

    #[test]
    fn add_spatial_constraint_failures() {
        let camera_one = Component::Camera(Camera::new(
            Uuid::new_v4(),
            Uuid::new_v4(),
            String::from("Camera 1"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let camera_two = Component::Camera(Camera::new(
            Uuid::new_v4(),
            *camera_one.root_uuid(),
            String::from("Camera 2"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let space = SpatialConstraint {
            extrinsics: Extrinsics::from(Isometry3::identity()),
            covariance: ExtrinsicCovariance::from(SMatrix::zeros()),
            from: *camera_one.uuid(),
            to: *camera_two.uuid(),
        };

        let mut builder = PlexBuilder::new();
        let res = builder
            .add_component(camera_one.clone())
            .expect("Failed to add first camera to builder.")
            .add_component(camera_two)
            .expect("Failed to add second camera to builder.")
            .add_spatial_constraint(space.clone())
            .expect("First addition of spatial constraint failed.")
            .add_spatial_constraint(space);

        assert!(res.is_err());

        match res {
            Err(PlexConstructionError::ConstraintAlreadyExists(_, _)) => (),
            _ => unreachable!(),
        }

        let space = SpatialConstraint {
            extrinsics: Extrinsics::from(Isometry3::identity()),
            covariance: ExtrinsicCovariance::from(SMatrix::zeros()),
            from: *camera_one.uuid(),
            to: *camera_one.uuid(),
        };

        // Now, we test if adding a relation where both UUIDs are the same fails
        let res = builder.add_spatial_constraint(space);

        assert!(res.is_err());

        match res {
            Err(PlexConstructionError::ConstraintMustBeBetweenDistinctComponents) => (),
            _ => unreachable!(),
        }

        let space = SpatialConstraint {
            extrinsics: Extrinsics::from(Isometry3::identity()),
            covariance: ExtrinsicCovariance::from(SMatrix::zeros()),
            from: *camera_one.uuid(),
            to: Uuid::new_v4(),
        };

        // This time, testing that passing in am unknown UUID results in a component not found
        // failure.
        let res = builder.add_spatial_constraint(space);

        assert!(res.is_err());

        match res {
            Err(PlexConstructionError::ComponentDoesNotExist(_)) => (),
            _ => unreachable!(),
        }
    }

    #[test]
    fn add_temporal_constraint_failures() {
        let camera_one = Component::Camera(Camera::new(
            Uuid::new_v4(),
            Uuid::new_v4(),
            String::from("Camera 1"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let camera_two = Component::Camera(Camera::new(
            Uuid::new_v4(),
            *camera_one.root_uuid(),
            String::from("Camera 2"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let time = TemporalConstraint {
            synchronization: Synchronization { offset: 0, skew: 0 },
            resolution: 0,
            from: *camera_one.uuid(),
            to: *camera_two.uuid(),
        };

        let mut builder = PlexBuilder::new();
        let res = builder
            .add_component(camera_one.clone())
            .expect("Failed to add first camera to builder.")
            .add_component(camera_two)
            .expect("Failed to add second camera to builder.")
            .add_temporal_constraint(time.clone())
            .expect("First addition of relation failed.")
            .add_temporal_constraint(time);

        assert!(res.is_err());

        match res {
            Err(PlexConstructionError::ConstraintAlreadyExists(_, _)) => (),
            _ => unreachable!(),
        }

        let time = TemporalConstraint {
            synchronization: Synchronization { offset: 0, skew: 0 },
            resolution: 0,
            from: *camera_one.uuid(),
            to: *camera_one.uuid(),
        };

        // Now, we test if adding a relation where both UUIDs are the same fails
        let res = builder.add_temporal_constraint(time);

        assert!(res.is_err());

        match res {
            Err(PlexConstructionError::ConstraintMustBeBetweenDistinctComponents) => (),
            _ => unreachable!(),
        }

        let time = TemporalConstraint {
            synchronization: Synchronization { offset: 0, skew: 0 },
            resolution: 0,
            from: *camera_one.uuid(),
            to: Uuid::new_v4(),
        };

        // This time, testing that passing in am unknown UUID results in a component not found
        // failure.
        let res = builder.add_temporal_constraint(time);

        assert!(res.is_err());

        match res {
            Err(PlexConstructionError::ComponentDoesNotExist(_)) => (),
            _ => unreachable!(),
        }
    }

    #[test]
    fn removing_component_removes_it_from_final_plex() {
        let camera_uuid = Uuid::new_v4();

        let camera = Component::Camera(Camera::new(
            camera_uuid,
            camera_uuid,
            String::from("Camera 1"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let mut builder = PlexBuilder::new();

        builder
            .add_component(camera)
            .expect("Failed to add camera to builder.")
            .remove_component(&camera_uuid);

        let plex = builder.build();

        assert!(plex.components().is_empty());
    }

    #[test]
    fn removing_component_removes_all_constraints_as_well() {
        let camera_one = Component::Camera(Camera::new(
            Uuid::new_v4(),
            Uuid::new_v4(),
            String::from("Camera 1"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let camera_two = Component::Camera(Camera::new(
            Uuid::new_v4(),
            *camera_one.root_uuid(),
            String::from("Camera 2"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let time = TemporalConstraint {
            synchronization: Synchronization { offset: 0, skew: 0 },
            resolution: 0,
            from: *camera_one.uuid(),
            to: *camera_two.uuid(),
        };

        let mut builder = PlexBuilder::new();
        builder
            .add_component(camera_one.clone())
            .expect("Failed to add first camera to builder.")
            .add_component(camera_two)
            .expect("Failed to add second camera to builder.")
            .add_temporal_constraint(time)
            .expect("Failed to add temporal constraint to builder.")
            .remove_component(camera_one.uuid());

        let plex = builder.build();

        assert!(plex.relations().is_empty());
    }

    #[test]
    fn removing_spatial_constraint_does_not_remove_temporal_constraint() {
        let camera_one = Component::Camera(Camera::new(
            Uuid::new_v4(),
            Uuid::new_v4(),
            String::from("Camera 1"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let camera_two = Component::Camera(Camera::new(
            Uuid::new_v4(),
            *camera_one.root_uuid(),
            String::from("Camera 2"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let space = SpatialConstraint {
            extrinsics: Extrinsics::from(Isometry3::identity()),
            covariance: ExtrinsicCovariance::from(SMatrix::zeros()),
            from: *camera_one.uuid(),
            to: *camera_two.uuid(),
        };

        let time = TemporalConstraint {
            synchronization: Synchronization { offset: 0, skew: 0 },
            resolution: 0,
            from: *camera_one.uuid(),
            to: *camera_two.uuid(),
        };

        let mut builder = PlexBuilder::new();
        builder
            .add_component(camera_one.clone())
            .expect("Failed to add first camera to builder.")
            .add_component(camera_two.clone())
            .expect("Failed to add second camera to builder.")
            .add_spatial_constraint(space)
            .expect("Failed to add spatial constraint to builder.")
            .add_temporal_constraint(time.clone())
            .expect("Failed to add temporal constraint to builder.")
            .remove_spatial_constraint(camera_one.uuid(), camera_two.uuid());

        let plex = builder.build();

        assert!(!plex.relations().is_empty());

        let plex_time = plex
            .get_temporal_constraint(camera_two.uuid(), camera_one.uuid())
            .unwrap();

        assert_eq!(plex_time.synchronization.skew, time.synchronization.skew);
        assert_eq!(
            plex_time.synchronization.offset,
            time.synchronization.offset
        );
        assert_eq!(plex_time.resolution, time.resolution);
    }

    #[test]
    fn removing_temporal_constraint_does_not_remove_spatial_constraint() {
        let camera_one = Component::Camera(Camera::new(
            Uuid::new_v4(),
            Uuid::new_v4(),
            String::from("Camera 1"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let camera_two = Component::Camera(Camera::new(
            Uuid::new_v4(),
            *camera_one.root_uuid(),
            String::from("Camera 2"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let space = SpatialConstraint {
            extrinsics: Extrinsics::from(Isometry3::identity()),
            covariance: ExtrinsicCovariance::from(SMatrix::zeros()),
            from: *camera_one.uuid(),
            to: *camera_two.uuid(),
        };

        let time = TemporalConstraint {
            synchronization: Synchronization { offset: 0, skew: 0 },
            resolution: 0,
            from: *camera_one.uuid(),
            to: *camera_two.uuid(),
        };

        let mut builder = PlexBuilder::new();
        builder
            .add_component(camera_one.clone())
            .expect("Failed to add first camera to builder.")
            .add_component(camera_two.clone())
            .expect("Failed to add second camera to builder.")
            .add_spatial_constraint(space.clone())
            .expect("Failed to add spatial constraint to builder.")
            .add_temporal_constraint(time)
            .expect("Failed to add temporal constraint to builder.")
            .remove_temporal_constraint(camera_one.uuid(), camera_two.uuid());

        let plex = builder.build();

        assert!(!plex.relations().is_empty());

        let plex_space = plex
            .get_spatial_constraint(camera_two.uuid(), camera_one.uuid())
            .unwrap();

        assert_relative_eq!(
            plex_space.extrinsics.to_isometry(),
            space.extrinsics.to_isometry(),
            epsilon = f64::EPSILON
        );

        assert_relative_eq!(
            plex_space.covariance.to_se3(),
            space.covariance.to_se3(),
            epsilon = f64::EPSILON
        );
    }

    #[test]
    fn removing_constraint_actually_removes_it_from_the_final_plex() {
        let camera_one = Component::Camera(Camera::new(
            Uuid::new_v4(),
            Uuid::new_v4(),
            String::from("Camera 1"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let camera_two = Component::Camera(Camera::new(
            Uuid::new_v4(),
            *camera_one.root_uuid(),
            String::from("Camera 2"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let space = SpatialConstraint {
            extrinsics: Extrinsics::from(Isometry3::identity()),
            covariance: ExtrinsicCovariance::from(SMatrix::zeros()),
            from: *camera_one.uuid(),
            to: *camera_two.uuid(),
        };

        let time = TemporalConstraint {
            synchronization: Synchronization { offset: 0, skew: 0 },
            resolution: 0,
            from: *camera_one.uuid(),
            to: *camera_two.uuid(),
        };

        let mut builder = PlexBuilder::new();
        builder
            .add_component(camera_one.clone())
            .expect("Failed to add first camera to builder.")
            .add_component(camera_two.clone())
            .expect("Failed to add second camera to builder.")
            .add_spatial_constraint(space)
            .expect("Failed to add spatial constraint to builder.")
            .remove_spatial_constraint(camera_one.uuid(), camera_two.uuid())
            .add_temporal_constraint(time)
            .expect("Failed to add temporal constraint to builder.")
            .remove_temporal_constraint(camera_one.uuid(), camera_two.uuid());

        let plex = builder.build();

        assert!(plex.relations().is_empty());
    }

    #[test]
    fn plex_is_homomorphic_when_converted_to_builder_and_back() {
        let camera_one = Component::Camera(Camera::new(
            Uuid::new_v4(),
            Uuid::new_v4(),
            String::from("Camera 1"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let camera_two = Component::Camera(Camera::new(
            Uuid::new_v4(),
            *camera_one.root_uuid(),
            String::from("Camera 2"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let space = SpatialConstraint {
            extrinsics: Extrinsics::from(Isometry3::identity()),
            covariance: ExtrinsicCovariance::from(SMatrix::zeros()),
            from: *camera_one.uuid(),
            to: *camera_two.uuid(),
        };

        let mut builder = PlexBuilder::new();
        builder
            .add_component(camera_one.clone())
            .expect("Failed to add first camera to builder.")
            .add_component(camera_two.clone())
            .expect("Failed to add second camera to builder.")
            .add_spatial_constraint(space)
            .expect("Failed to add spatial constraint to builder.");

        let plex = builder.build();

        let builder_from_plex = PlexBuilder::from(plex.clone());

        let homomorphic_plex = builder_from_plex.build();
        // Based on how the builder works, we can't guarantee that the timestamp and UUID are
        // exactly the same (without doing build_with_parts where we trivially set them to be the
        // same). But we do want to guarantee that the new plex has the same components and
        // relations from an API perspective.

        let plex_components = plex.components();
        let homomorphic_components = homomorphic_plex.components();

        let plex_c1 = plex_components
            .iter()
            .find(|c| c.uuid() == camera_one.uuid())
            .unwrap()
            .as_camera()
            .unwrap();

        let homomorphic_c1 = homomorphic_components
            .iter()
            .find(|c| c.uuid() == camera_one.uuid())
            .unwrap()
            .as_camera()
            .unwrap();

        assert_eq!(plex_c1.root_uuid(), homomorphic_c1.root_uuid());
        assert_eq!(plex_c1.name(), homomorphic_c1.name());

        assert!(plex_c1.intrinsics.eq(&homomorphic_c1.intrinsics));

        assert_relative_eq!(
            plex_c1.covariance,
            homomorphic_c1.covariance,
            epsilon = f64::EPSILON
        );

        let plex_c2 = plex_components
            .iter()
            .find(|c| c.uuid() == camera_two.uuid())
            .unwrap()
            .as_camera()
            .unwrap();

        let homomorphic_c2 = homomorphic_components
            .iter()
            .find(|c| c.uuid() == camera_two.uuid())
            .unwrap()
            .as_camera()
            .unwrap();

        assert_eq!(plex_c2.root_uuid(), homomorphic_c2.root_uuid());
        assert_eq!(plex_c2.name(), homomorphic_c2.name());

        assert!(plex_c2.intrinsics.eq(&homomorphic_c2.intrinsics));

        assert_relative_eq!(
            plex_c2.covariance,
            homomorphic_c2.covariance,
            epsilon = f64::EPSILON
        );

        assert_eq!(plex.components().len(), 2);
        assert_eq!(homomorphic_plex.components().len(), 2);

        let plex_constraint = plex
            .get_spatial_constraint(camera_two.uuid(), camera_one.uuid())
            .unwrap();

        let homomorphic_constraint = homomorphic_plex
            .get_spatial_constraint(camera_two.uuid(), camera_one.uuid())
            .unwrap();

        assert_relative_eq!(
            plex_constraint.extrinsics.to_isometry(),
            homomorphic_constraint.extrinsics.to_isometry(),
            epsilon = f64::EPSILON
        );

        assert_relative_eq!(
            plex_constraint.covariance.to_se3(),
            homomorphic_constraint.covariance.to_se3(),
            epsilon = f64::EPSILON
        );

        assert!(plex
            .get_temporal_constraint(camera_two.uuid(), camera_one.uuid())
            .is_none());
        assert!(homomorphic_plex
            .get_temporal_constraint(camera_two.uuid(), camera_one.uuid())
            .is_none());

        assert_eq!(plex.relations().len(), 1);
        assert_eq!(homomorphic_plex.relations().len(), 1);
    }

    #[test]
    fn component_remove_and_add() {
        let camera_one = Component::Camera(Camera::new(
            Uuid::new_v4(),
            Uuid::new_v4(),
            String::from("Camera 1"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let camera_two = Component::Camera(Camera::new(
            Uuid::new_v4(),
            *camera_one.root_uuid(),
            String::from("Camera 2"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let mut pb = PlexBuilder::new();
        pb.add_component(camera_one.clone())
            .unwrap()
            .add_component(camera_two.clone())
            .unwrap()
            .remove_component(camera_one.uuid())
            .add_component(camera_one)
            .unwrap()
            .remove_component(camera_two.uuid());

        let plex = pb.build();
        let mut has_c1 = false;
        let mut has_c2 = false;
        for c in plex.components() {
            match c.as_camera().unwrap().name() {
                "Camera 1" => {
                    has_c1 = true;
                }
                "Camera 2" => {
                    has_c2 = true;
                }
                _ => {}
            };
        }
        assert!(has_c1);
        assert!(!has_c2);
    }

    #[test]
    fn joining_subplex_to_plex_with_same_component_is_error() {
        let camera_one = Component::Camera(Camera::new(
            Uuid::new_v4(),
            Uuid::new_v4(),
            String::from("Camera 1"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let mut builder = PlexBuilder::new();
        builder
            .add_component(camera_one.clone())
            .expect("Failed to add first camera to builder.");
        let plex = builder.build();

        let mut builder_two = PlexBuilder::new();
        builder_two
            .add_component(camera_one)
            .expect("Failed to add first camera to builder.");
        assert!(builder_two.join(plex).is_err());
    }

    #[test]
    fn joining_subplex_to_same_plex_twice_is_error() {
        let camera_one = Component::Camera(Camera::new(
            Uuid::new_v4(),
            Uuid::new_v4(),
            String::from("Camera 1"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let mut builder = PlexBuilder::new();
        builder
            .add_component(camera_one)
            .expect("Failed to add first camera to builder.");
        let plex = builder.build();

        let mut builder_two = PlexBuilder::new();
        builder_two
            .join(plex.clone())
            .expect("Failed to add plex to builder.");
        assert!(builder_two.join(plex).is_err());
    }

    #[test]
    fn joining_empty_builder_to_empty_builder_is_empty() {
        let mut empty_plex_builder = PlexBuilder::new();
        empty_plex_builder.join(PlexBuilder::new()).unwrap();
        let plex = empty_plex_builder.build();
        assert_eq!(plex.components().len(), 0);
        assert_eq!(plex.relations().len(), 0);
    }

    #[test]
    fn joining_subplex_to_empty_builder_creates_same_plex() {
        let camera_one = Component::Camera(Camera::new(
            Uuid::new_v4(),
            Uuid::new_v4(),
            String::from("Camera 1"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let camera_two = Component::Camera(Camera::new(
            Uuid::new_v4(),
            *camera_one.root_uuid(),
            String::from("Camera 2"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let space = SpatialConstraint {
            extrinsics: Extrinsics::from(Isometry3::identity()),
            covariance: ExtrinsicCovariance::from(SMatrix::zeros()),
            from: *camera_one.uuid(),
            to: *camera_two.uuid(),
        };

        let time = TemporalConstraint {
            synchronization: Synchronization { offset: 0, skew: 0 },
            resolution: 0,
            from: *camera_one.uuid(),
            to: *camera_two.uuid(),
        };

        let mut builder = PlexBuilder::new();
        builder
            .add_component(camera_one)
            .expect("Failed to add first camera to builder.")
            .add_component(camera_two)
            .expect("Failed to add second camera to builder.")
            .add_spatial_constraint(space)
            .expect("Failed to add spatial constraint to builder.")
            .add_temporal_constraint(time)
            .expect("Failed to add temporal constraint to builder.");

        // For use later
        let mut builder_rhs = builder.clone();
        let plex = builder.build();

        // Add our plex to an empty builder.
        let mut builder_lhs = PlexBuilder::new();
        builder_lhs
            .join(plex.clone())
            .expect("Failed to add subplex to builder.");
        let plex_lhs = builder_lhs.build();

        // Add an empty builder to our plex (reciprocal test)
        builder_rhs
            .join(PlexBuilder::new())
            .expect("Failed to add empty builder to builder.");
        let plex_rhs = builder_rhs.build();

        // This test assures that there is a 1:1 matching relationship between every
        // component/relation from the first plex into the new second plex which it joined.
        //
        // NOTE: These plexes won't be exactly the same; the plex metadata itself (UUID, creation
        // timestamp) for the plex will be different. Instead of plex.is_same(), we must compare all
        // components and constraints for sameness.
        for c in plex.components().iter() {
            // 1-1 correspondence for all components in orig. plex and [empty.join(plex)]
            assert_eq!(
                plex_lhs
                    .components()
                    .iter()
                    .filter(|c_two| c_two.is_same(c))
                    .count(),
                1
            );
            // 1-1 correspondence for all components in [plex.join(empty)] and orig. plex
            assert_eq!(
                plex_rhs
                    .components()
                    .iter()
                    .filter(|c_two| c_two.is_same(c))
                    .count(),
                1
            );
        }
        for r in plex.relations().iter() {
            // 1-1 correspondence for all relations in orig. plex and [empty.join(plex)]
            assert_eq!(
                plex_lhs
                    .relations()
                    .iter()
                    .filter(|r_two| r_two.is_same(r))
                    .count(),
                1
            );
            // 1-1 correspondence for all components in [plex.join(empty)] and orig. plex
            assert_eq!(
                plex_rhs
                    .relations()
                    .iter()
                    .filter(|r_two| r_two.is_same(r))
                    .count(),
                1
            );
        }
    }

    // Comparing moved float data, nothing checked is the result of a calculation
    #[allow(clippy::float_cmp)]
    #[test]
    fn plex_calibration_example() {
        let camera_one = Component::Camera(Camera::new(
            Uuid::new_v4(),
            Uuid::new_v4(),
            String::from("Camera 1"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let camera_two = Component::Camera(Camera::new(
            Uuid::new_v4(),
            *camera_one.root_uuid(),
            String::from("Camera 2"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let space = SpatialConstraint {
            extrinsics: Extrinsics::from(Isometry3::translation(0.0, 0.0, 0.1)),
            covariance: ExtrinsicCovariance::from(SMatrix::identity()),
            from: *camera_one.uuid(),
            to: *camera_two.uuid(),
        };
        let temp = TemporalConstraint {
            synchronization: Synchronization { offset: 0, skew: 1 },
            resolution: 1000,
            from: *camera_one.uuid(),
            to: *camera_two.uuid(),
        };

        let mut pb = PlexBuilder::new();
        pb.add_component(camera_one.clone()).unwrap();
        pb.add_component(camera_two.clone()).unwrap();
        pb.add_spatial_constraint(space.clone()).unwrap();
        pb.add_temporal_constraint(temp.clone()).unwrap();

        let uncalibrated_plex = pb.build();

        // Cache the relations since they'll get removed
        let mut relations = uncalibrated_plex
            .relations()
            .iter()
            .map(|r| (*r).clone())
            .collect::<Vec<_>>();

        let mut calib_plex_buider: PlexBuilder = uncalibrated_plex.into();

        // Recreate camera one with new intrinsics
        let mut calibrated_intrinsics_one = mock_intrinsics();
        match &mut calibrated_intrinsics_one.projection {
            ProjectionModel::Pinhole { f, cx, cy } => {
                *f *= 1.1;
                *cx *= 1.1;
                *cy *= 1.1;
            }
        }
        calib_plex_buider.remove_component(camera_one.uuid());
        let mut calib_camera_one = camera_one.clone();
        calib_camera_one.as_camera_mut().unwrap().intrinsics = calibrated_intrinsics_one.clone();

        calib_plex_buider.add_component(calib_camera_one).unwrap();

        // Recreate camera two with new intrinsics
        let mut calibrated_intrinsics_two = mock_intrinsics();
        match &mut calibrated_intrinsics_two.projection {
            ProjectionModel::Pinhole { f, cx, cy } => {
                *f *= 0.99;
                *cx *= 0.99;
                *cy *= 0.99;
            }
        }
        calib_plex_buider.remove_component(camera_two.uuid());
        let mut calib_camera_two = camera_two.clone();
        calib_camera_two.as_camera_mut().unwrap().intrinsics = calibrated_intrinsics_two.clone();
        calib_plex_buider.add_component(calib_camera_two).unwrap();

        let calib_transform = Extrinsics::from(Isometry3::translation(1e-4, -1e-4, 0.999));
        for mut r in relations.iter_mut() {
            let maybe_space = match &mut r {
                Relation::Space(s) => Some(s),
                Relation::Time(_) => None,
                Relation::SpaceAndTime { space, time: _t } => Some(space),
            };
            maybe_space.unwrap().extrinsics = calib_transform.clone();
        }

        for r in relations.iter() {
            match r {
                Relation::Space(s) => {
                    calib_plex_buider.add_spatial_constraint(s.clone()).unwrap();
                }
                Relation::Time(t) => {
                    calib_plex_buider
                        .add_temporal_constraint(t.clone())
                        .unwrap();
                }
                Relation::SpaceAndTime { space, time } => {
                    calib_plex_buider
                        .add_spatial_constraint(space.clone())
                        .unwrap();
                    calib_plex_buider
                        .add_temporal_constraint(time.clone())
                        .unwrap();
                }
            };
        }

        let calib_plex = calib_plex_buider.build();
        for c in calib_plex.components().iter() {
            let cam = c.as_camera().unwrap();

            let tgt_cam;
            let tgt_intr;
            if cam.name() == "Camera 1" {
                tgt_cam = camera_one.as_camera().unwrap();
                tgt_intr = calibrated_intrinsics_one.clone();
            } else if cam.name() == "Camera 2" {
                tgt_cam = camera_two.as_camera().unwrap();
                tgt_intr = calibrated_intrinsics_two.clone();
            } else {
                panic!("We got a new camera somehow?")
            }

            assert_eq!(cam.uuid(), tgt_cam.uuid());
            assert_eq!(cam.root_uuid(), tgt_cam.root_uuid());
            assert_eq!(cam.name(), tgt_cam.name());
            assert_eq!(cam.intrinsics, tgt_intr);
            assert_eq!(cam.covariance, tgt_cam.covariance); // we didn't adjust cov in this case
            assert_eq!(cam.pixel_pitch, tgt_cam.pixel_pitch);
        }

        assert_eq!(calib_plex.relations().len(), 1);
        for r in calib_plex.relations().iter() {
            let (maybe_space, maybe_time) = match r {
                Relation::Space(s) => (Some(s), None),
                Relation::Time(t) => (None, Some(t)),
                Relation::SpaceAndTime { space, time } => (Some(space), Some(time)),
            };

            // We expect one space
            let new_space = maybe_space.unwrap();
            // ... and one time
            let new_temp = maybe_time.unwrap();
            assert_eq!(*new_temp, temp);
            assert_eq!(new_space.from, space.from);
            assert_eq!(new_space.to, space.to);
            assert_eq!(new_space.covariance.to_se3(), space.covariance.to_se3()); // we didn't adjust cov in this case
            assert_eq!(new_space.extrinsics.to_se3(), calib_transform.to_se3());
        }
    }
}

//#endregion
