// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! Defines `Plex`, the relational graphs connecting all components through space and time.
//!
//! They are undirected, acyclic graphs with Components as nodes and Relations
//! as edges. They serve to connect sensor suites in three distinct ways:
//!
//! - Modeling: Component-based. A plex is modeled _iff_ a complete model
//!   descriptor exists for every component in the graph.
//! - Registration: Relation-based. A plex is registered _iff_ for every
//!   pair of components m and n in the graph, there exists a unique series of
//!   spatial constraints that connects the pair.
//! - Synchronization: Relation-based. A plex is synchronized _iff_ for
//!   every pair of components m and n in the graph, there exists a unique
//!   series of temporal constraints that connects the pair.
//!
//! A plex is *calibrated* iff it is fully modeled, registered, and synchronized.
//! See [the documentation](https://www.notion.so/tangramvision/Plexes-5dedf69dd9da40978f428ea544f6634c)
//! for more.

use super::{
    builder::{PlexBuilder, PlexConstructionError},
    component::Component,
    relation::{Relation, SpatialConstraint, Synchronization, TemporalConstraint},
};
use crate::base::extrinsics::{ExtrinsicCovariance, Extrinsics};
use crate::prelude::Sameness;
use anyhow::Result;
use nalgebra::{Isometry3, Matrix6};
use petgraph::{
    algo::all_simple_paths,
    graph::{DefaultIx, NodeIndex, UnGraph},
};
use serde::{ser::*, Deserialize, Deserializer, Serialize, Serializer};
use std::{collections::HashMap, fs::read_to_string, path::Path};
use uuid::Uuid;

//#region Type declarations

/// Type alias for the underlying plex's graph type.
///
/// Mostly used for the sake of brevity.
pub(super) type PlexGraph = UnGraph<Component, Relation>;

/// A Plex is a undirected, acyclic graph describing the spatial and temporal constraints of a
/// sensor system.
///
/// NOTE: Plexes are immutable by design! If a plex is invalid, it cannot be modified. Another plex
/// must be created and used in its place.
#[derive(Debug, Clone)]
pub struct Plex {
    /// Our graph structure.
    pub(super) graph: PlexGraph,

    /// Map from UUID to node. Node indices change, but UUIDs do not.
    pub(super) node_from_uuid: HashMap<Uuid, NodeIndex<DefaultIx>>,

    /// UUID unique to this Plex. Used when reading/writing stored data.
    pub(super) uuid: Uuid,

    /// First valid timestamp in this plex's existence.
    pub(super) creation_timestamp: i64,
}

//#endregion

/// Read a plex in from a JSON file at the given path.
pub fn read_plex<P>(plex_path: P) -> Result<Plex>
where
    P: AsRef<Path>,
{
    let valid_plex = serde_json::from_str::<Plex>(&read_to_string(plex_path)?)?;
    Ok(valid_plex)
}

//#region Plex impl

impl Serialize for Plex {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut s = serializer.serialize_struct("Plex", 5)?;

        s.serialize_field("uuid", &self.uuid)?;
        s.serialize_field("creation_timestamp", &self.creation_timestamp)?;
        s.serialize_field("components", &self.components())?;

        let mut spatial_constraints = Vec::new();
        let mut temporal_constraints = Vec::new();

        for relation in self.relations() {
            match relation {
                Relation::Space(s) => spatial_constraints.push(s),
                Relation::Time(t) => temporal_constraints.push(t),
                Relation::SpaceAndTime { space, time } => {
                    spatial_constraints.push(space);
                    temporal_constraints.push(time);
                }
            }
        }

        s.serialize_field("spatial_constraints", &spatial_constraints)?;
        s.serialize_field("temporal_constraints", &temporal_constraints)?;
        s.end()
    }
}

impl<'de> Deserialize<'de> for Plex {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        /// Intermediate type for visiting the field keys of a plex when serialized.
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "snake_case")]
        enum PlexFields {
            /// The unique identifier of the plex
            Uuid,
            /// The timestamp at which this plex was created
            CreationTimestamp,
            /// Serialized components of the plex
            Components,
            /// Serialized spatial constraints of the plex
            SpatialConstraints,
            /// Serialized temporal constraints of the plex
            TemporalConstraints,
        }

        /// Plex field keys as static strs
        const FIELDS: [&str; 5] = [
            "uuid",
            "creation_timestamp",
            "components",
            "spatial_constraints",
            "temporal_constraints",
        ];

        /// An intermediate type used solely for implementing a custom visitor, which produces a
        /// Plex (custom struct) from a map of key <-> value pairs.
        struct PlexVisitor;

        impl<'de> serde::de::Visitor<'de> for PlexVisitor {
            type Value = Plex;

            fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
                formatter.write_str("struct Plex")
            }

            fn visit_map<V>(self, mut map: V) -> std::result::Result<Plex, V::Error>
            where
                V: serde::de::MapAccess<'de>,
            {
                let mut uuid = None;
                let mut creation_timestamp = None;
                let mut components = Vec::new();
                let mut spatial_constraints = Vec::new();
                let mut temporal_constraints = Vec::new();

                while let Some(key) = map.next_key()? {
                    match key {
                        PlexFields::Uuid => {
                            if uuid.is_some() {
                                return Err(serde::de::Error::duplicate_field("uuid"));
                            }
                            uuid = Some(map.next_value()?);
                        }

                        PlexFields::CreationTimestamp => {
                            if creation_timestamp.is_some() {
                                return Err(serde::de::Error::duplicate_field(
                                    "creation_timestamp",
                                ));
                            }
                            creation_timestamp = Some(map.next_value()?);
                        }

                        PlexFields::Components => {
                            if !components.is_empty() {
                                return Err(serde::de::Error::duplicate_field("components"));
                            }
                            components.append(&mut map.next_value()?);
                        }

                        PlexFields::SpatialConstraints => {
                            if !spatial_constraints.is_empty() {
                                return Err(serde::de::Error::duplicate_field(
                                    "spatial_constraints",
                                ));
                            }
                            spatial_constraints.append(&mut map.next_value()?);
                        }

                        PlexFields::TemporalConstraints => {
                            if !temporal_constraints.is_empty() {
                                return Err(serde::de::Error::duplicate_field(
                                    "temporal_constraints",
                                ));
                            }
                            temporal_constraints.append(&mut map.next_value()?);
                        }
                    }
                }

                let uuid = uuid.ok_or_else(|| serde::de::Error::missing_field("uuid"))?;
                let creation_timestamp = creation_timestamp
                    .ok_or_else(|| serde::de::Error::missing_field("creation_timestamp"))?;

                let mut builder = PlexBuilder::new();

                for component in components {
                    builder.add_component(component).map_err(|e| {
                        let uuid = match e {
                            PlexConstructionError::ComponentAlreadyExists(uuid) => uuid,
                            _ => unreachable!(),
                        };
                        let field = format!("duplicate field `component.uuid ({})`", uuid);

                        serde::de::Error::custom(field)
                    })?;
                }

                for sc in spatial_constraints {
                    builder.add_spatial_constraint(sc).map_err(|e| {
                        let field = match e {
                            PlexConstructionError::ConstraintAlreadyExists(from, to) => {
                                format!(
                                    "duplicate field `spatial_constraints.from ({}) and .to ({})`",
                                    from, to
                                )
                            }

                            PlexConstructionError::ComponentDoesNotExist(uuid) => {
                                format!("missing field `component with uuid ({})`", uuid)
                            }

                            PlexConstructionError::ConstraintMustBeBetweenDistinctComponents => {
                                String::from("spatial constraint `from` and `to` are same UUID")
                            }

                            _ => unreachable!(),
                        };

                        serde::de::Error::custom(field)
                    })?;
                }

                for tc in temporal_constraints {
                    builder.add_temporal_constraint(tc).map_err(|e| {
                        let field = match e {
                            PlexConstructionError::ConstraintAlreadyExists(from, to) => {
                                format!(
                                    "duplicate field `temporal_constraints.from ({}) and .to ({})`",
                                    from, to
                                )
                            }

                            PlexConstructionError::ComponentDoesNotExist(uuid) => {
                                format!("missing field `component with uuid ({})`", uuid)
                            }

                            PlexConstructionError::ConstraintMustBeBetweenDistinctComponents => {
                                String::from("temporal constraint `from` and `to` are same UUID")
                            }

                            _ => unreachable!(),
                        };

                        serde::de::Error::custom(field)
                    })?;
                }

                Ok(builder.build_with_parts(uuid, creation_timestamp))
            }
        }

        deserializer.deserialize_struct("Plex", &FIELDS, PlexVisitor)
    }
}

impl Sameness for Plex {
    fn is_same(&self, rhs: &Self) -> bool {
        if !self.uuid().is_same(&rhs.uuid())
            || !self.creation_timestamp().is_same(&rhs.creation_timestamp())
        {
            return false;
        }

        // WARNING: Checking components and relations is kind of spotty?
        //
        // We do the naive thing here and do an O(n^2) search for a component / relation that
        // `is_same` as one in the other plex.
        //
        // This is a bit fragile, but should demonstrate that the serialization is reflective,
        // at least. Note that these tests do not preclude users from serializing a plex that
        // would break this check (i.e. pass this test, but still be invalid). However, that
        // would only happen at the conceptual level, because the plex builder type from Rivet
        // rejects duplicate components or relations.
        //
        // If that ever changes, this code is invalid!!!

        let left_components = self.components();
        let right_components = rhs.components();

        if left_components.len() != right_components.len() {
            return false;
        }

        for left in left_components {
            let mut found_same = false;

            for right in right_components.iter() {
                if left.is_same(right) {
                    found_same = true;
                    break;
                }
            }

            if !found_same {
                return false;
            }
        }

        let left_relations = self.relations();
        let right_relations = rhs.relations();

        if left_relations.len() != right_relations.len() {
            return false;
        }

        for left in left_relations {
            let mut found_same = false;

            for right in right_relations.iter() {
                if left.is_same(right) {
                    found_same = true;
                    break;
                }
            }

            if !found_same {
                return false;
            }
        }

        true
    }
}

impl Plex {
    /// The UUID of the Plex.
    pub fn uuid(&self) -> Uuid {
        self.uuid
    }

    /// The creation timestamp of the Plex, in nanoseconds from epoch.
    pub fn creation_timestamp(&self) -> i64 {
        self.creation_timestamp
    }

    /// Get an existing component by its UUID, if it exists within the Plex.
    pub fn get_component(&self, uuid: &Uuid) -> Option<&Component> {
        let node_idx = self.node_from_uuid.get(uuid)?;
        self.graph.node_weight(*node_idx)
    }

    /// Retrieve all existing components in the plex
    pub fn components(&self) -> Vec<&Component> {
        let node_array = self.graph.raw_nodes();
        node_array.iter().map(|x| &x.weight).collect()
    }

    /// Predicate for checking if a component with a provided UUID exists within the Plex.
    pub fn contains_component(&self, uuid: &Uuid) -> bool {
        self.node_from_uuid.contains_key(uuid)
    }

    /// Finds the first component in the plex that satisfies the provided predicate.
    ///
    /// Equivalent to doing `plex.components().find(pred)`.
    pub fn find_component<P>(&self, pred: P) -> Option<&Component>
    where
        P: Fn(&Component) -> bool,
    {
        self.graph.raw_nodes().iter().find_map(|node| {
            if pred(&node.weight) {
                Some(&node.weight)
            } else {
                None
            }
        })
    }

    /// Gets the spatial constraint between two components (`component` from `from`) with the
    /// minimum covariance from the Plex.
    ///
    /// This searches through all possible simple paths in the Plex that connect the two
    /// components, and picks the spatial constraint propagated along that path with the minimum
    /// covariance.
    ///
    /// Returns `None` iff there exists no path (of spatial constraints) connecting the two
    /// components in the Plex.
    pub fn get_spatial_constraint(
        &self,
        component: &Uuid,
        from: &Uuid,
    ) -> Option<SpatialConstraint> {
        let node_to = self.node_from_uuid.get(component)?;
        let node_from = self.node_from_uuid.get(from)?;

        // Early exit if the nodes exist and they are the same UUID.
        if component == from {
            return Some(SpatialConstraint {
                extrinsics: Extrinsics::from(&Isometry3::identity()),
                covariance: ExtrinsicCovariance::new(Matrix6::zeros()),
                from: *from,
                to: *component,
            });
        }

        // Minimum Covariance Determinant.
        //
        // The determinant of a covariance matrix is a relatively robust way to compare two
        // covariance matrices for the smallest average variance across the entire set, as
        // comparing values in a matrix directly may lead to controversial sorting.
        //
        // NOTE: determinants are not generally applicable in this way, unless applied to
        // covariance matrices. This is because covariance matrices are symmetric & positive
        // semi-definite matrices.
        let mut mcd = f64::INFINITY;

        let mut constraint = None;

        for path in all_simple_paths::<Vec<NodeIndex<DefaultIx>>, &PlexGraph>(
            &self.graph,
            *node_from,
            *node_to,
            0,
            None,
        ) {
            let mut path_clone = path.iter();

            // Consume the first item so that we can offset the vector by 1.
            let _ = path_clone.next();

            let mut all_constraints_in_path =
                path.iter().zip(path_clone).map(|(node_prev, node_next)| {
                    let edge_idx = self.graph.find_edge(*node_next, *node_prev)?;
                    match self.graph.edge_weight(edge_idx)? {
                        Relation::Space(s) => Some(s),
                        Relation::SpaceAndTime { space, .. } => Some(space),
                        Relation::Time(_) => None,
                    }
                });

            // It is imperative that we start the seed with the correct from/to orientation.
            //
            // Thus, if the from / to are not already aligned, then the extrinsics isometry needs
            // to be inverted, and we need to swap the to and from on the constraint.
            //
            // Likewise, notice that we do not pass `Extrinsics` here, but rather just the
            // isometry. The isometry is a more convenient form for composing many extrinsics.  The
            // lack of conversions to / from Extrinsics and Isometry3 also helps reduce any
            // accumulated floating point error in the final extrinsic (however, we're using f64
            // to represent extrinsics, so this should be vastly less than 5e-6 (1 arc-second) even
            // if we didn't. This may not hold if f32 is used).
            let seed = {
                let constraint = all_constraints_in_path.next().flatten().unwrap();

                if constraint.from == *from {
                    Some((
                        constraint.extrinsics.to_isometry(),
                        constraint.covariance.clone(),
                        constraint.to,
                    ))
                } else {
                    Some((
                        constraint.extrinsics.inverse().to_isometry(),
                        constraint.covariance.clone(),
                        constraint.from,
                    ))
                }
            };

            let constraint_for_path = all_constraints_in_path.fold(seed, |k, c| {
                let (isometry, cov, to) = k?;
                let space_c = c?;

                let new_cov = space_c.covariance.compose(&space_c.extrinsics, &cov);

                let (new_isometry, new_to) = if space_c.from == to {
                    (space_c.extrinsics.to_isometry() * isometry, space_c.to)
                } else {
                    (
                        space_c.extrinsics.to_isometry().inv_mul(&isometry),
                        space_c.from,
                    )
                };

                Some((new_isometry, new_cov, new_to))
            });

            if let Some((isometry, covariance, to)) = constraint_for_path {
                let det = covariance.to_se3().determinant();

                if det < mcd {
                    mcd = det;
                    constraint = Some(SpatialConstraint {
                        extrinsics: Extrinsics::from(isometry),
                        covariance,
                        from: *from,
                        to,
                    });
                }
            }
        }

        constraint
    }

    /// Gets the temporal constraint between two components in the plex, if one exists.
    ///
    /// Returns `None` iff there exists no path (of temporal constraints) connecting the two
    /// components in the Plex.
    pub fn get_temporal_constraint(
        &self,
        component: &Uuid,
        from: &Uuid,
    ) -> Option<TemporalConstraint> {
        let node_to = self.node_from_uuid.get(component)?;
        let node_from = self.node_from_uuid.get(from)?;

        // Early exit if the nodes exist and they are the same UUID.
        if component == from {
            return Some(TemporalConstraint {
                synchronization: Synchronization { offset: 0, skew: 0 },
                resolution: 0,
                from: *from,
                to: *component,
            });
        }

        let mut min_resolution = u64::MAX;
        let mut constraint = None;

        for path in all_simple_paths::<Vec<NodeIndex<DefaultIx>>, &PlexGraph>(
            &self.graph,
            *node_from,
            *node_to,
            0,
            None,
        ) {
            let mut path_clone = path.iter();

            // Consume the first item so that we can offset the vector by 1.
            let _ = path_clone.next();

            let mut all_constraints_in_path =
                path.iter().zip(path_clone).map(|(node_prev, node_next)| {
                    let edge_idx = self.graph.find_edge(*node_next, *node_prev)?;
                    match self.graph.edge_weight(edge_idx)? {
                        Relation::Space(_) => None,
                        Relation::SpaceAndTime { time, .. } => Some(time),
                        Relation::Time(t) => Some(t),
                    }
                });

            // It is imperative that we start the seed with the correct from/to orientation.
            let seed = {
                let constraint = all_constraints_in_path.next().flatten()?;

                if constraint.from == *from {
                    Some(constraint.clone())
                } else {
                    Some(constraint.inverse())
                }
            };

            let constraint_for_path = all_constraints_in_path.fold(seed, |current, next| {
                let current = current?;
                let next = next?;

                current
                    .compose(next)
                    .or_else(|| current.compose(&next.inverse()))
            });

            match constraint_for_path {
                Some(c) if c.resolution <= min_resolution => {
                    min_resolution = c.resolution;
                    constraint = Some(c);
                }
                _ => (),
            }
        }
        constraint
    }

    /// Retrieve all existing relations in the plex.
    pub fn relations(&self) -> Vec<&Relation> {
        self.graph
            .raw_edges()
            .iter()
            .map(|edge| &edge.weight)
            .collect()
    }
}
//#endregion

//#region Tests

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        base::intrinsics::{AffinityModel, CameraIntrinsics, DistortionModel, ProjectionModel},
        plex::{
            builder::PlexBuilder,
            component::{Camera, Component, Anchor},
            relation::SpatialConstraint,
        },
        prelude::*,
    };
    use approx::{assert_relative_eq, assert_relative_ne};
    use nalgebra::{DMatrix, SMatrix, Translation3, UnitQuaternion, Vector3};

    /// Epsilon for approximate floating comparisons
    const EPSILON: f64 = 1e-12;

    fn mock_intrinsics() -> CameraIntrinsics {
        CameraIntrinsics {
            projection: ProjectionModel::Pinhole {
                f: 300.0,
                cx: 320.0,
                cy: 240.0,
            },
            distortion: DistortionModel::BrownConrady {
                k1: 0.0,
                k2: 0.0,
                k3: 0.0,
                p1: 0.0,
                p2: 0.0,
            },
            affinity: AffinityModel::NoAffinity,
            width: 640,
            height: 480,
        }
    }

    #[test]
    fn getting_spatial_constraint_from_known_relation() {
        let uuid_one = Uuid::new_v4();

        let camera_one = Component::Camera(Camera::new(
            uuid_one,
            uuid_one,
            String::from("Camera 1"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let camera_two = Component::Camera(Camera::new(
            Uuid::new_v4(),
            uuid_one,
            String::from("Camera 2"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let constraint = SpatialConstraint {
            extrinsics: Extrinsics::from(Isometry3::identity()),
            covariance: ExtrinsicCovariance::from(SMatrix::repeat(999.0)),
            from: *camera_one.uuid(),
            to: *camera_two.uuid(),
        };

        let mut builder = PlexBuilder::new();
        builder
            .add_component(camera_one.clone())
            .expect("Could not add first component to PlexBuilder.")
            .add_component(camera_two.clone())
            .expect("Could not add second component to PlexBuilder.")
            .add_spatial_constraint(constraint.clone())
            .expect("Could not add relation to PlexBuilder.");

        let plex = builder.build();

        assert!(plex.contains_component(camera_one.uuid()));
        assert!(plex.contains_component(camera_two.uuid()));

        let constraint_from_plex = plex
            .get_spatial_constraint(camera_two.uuid(), camera_one.uuid())
            .expect("Could not find spatial constraint from plex!");

        assert_eq!(constraint_from_plex.from, *camera_one.uuid());
        assert_eq!(constraint_from_plex.to, *camera_two.uuid());

        assert_relative_eq!(
            constraint_from_plex.extrinsics.to_se3(),
            constraint.extrinsics.to_se3(),
            epsilon = EPSILON,
        );

        assert_relative_eq!(
            constraint_from_plex.covariance.to_se3(),
            constraint.covariance.to_se3(),
            epsilon = EPSILON,
        );

        let inverse_constraint = plex
            .get_spatial_constraint(camera_one.uuid(), camera_two.uuid())
            .expect("Could not find spatial constraint from plex!");

        assert_eq!(inverse_constraint.from, *camera_two.uuid());
        assert_eq!(inverse_constraint.to, *camera_one.uuid());

        assert_relative_eq!(
            inverse_constraint.extrinsics.to_se3(),
            constraint.extrinsics.inverse().to_se3(),
            epsilon = EPSILON,
        );

        assert_relative_eq!(
            inverse_constraint.covariance.to_se3(),
            constraint.covariance.to_se3(),
            epsilon = EPSILON,
        );
    }

    #[test]
    fn getting_spatial_constraint_picks_min_covariance() {
        let root_uuid = Uuid::new_v4();

        let camera_a = Component::Camera(Camera::new(
            root_uuid,
            root_uuid,
            String::from("A"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let camera_b = Component::Camera(Camera::new(
            Uuid::new_v4(),
            root_uuid,
            String::from("B"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let camera_c = Component::Camera(Camera::new(
            Uuid::new_v4(),
            root_uuid,
            String::from("C"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let constraint_b_from_a = SpatialConstraint {
            extrinsics: Extrinsics::from(Isometry3::identity()),
            covariance: ExtrinsicCovariance::from(SMatrix::identity()),
            from: *camera_a.uuid(),
            to: *camera_b.uuid(),
        };

        let constraint_b_from_c = SpatialConstraint {
            extrinsics: Extrinsics::from(Isometry3::identity()),
            covariance: ExtrinsicCovariance::from(SMatrix::identity()),
            from: *camera_c.uuid(),
            to: *camera_b.uuid(),
        };

        let constraint_a_from_c = SpatialConstraint {
            extrinsics: Extrinsics::from(Isometry3::identity()),
            covariance: ExtrinsicCovariance::from(30.0 * SMatrix::identity()),
            from: *camera_c.uuid(),
            to: *camera_a.uuid(),
        };

        let mut builder = PlexBuilder::new();
        builder
            .add_component(camera_a.clone())
            .expect("Could not add A to PlexBuilder.")
            .add_component(camera_b)
            .expect("Could not add B to PlexBuilder.")
            .add_component(camera_c.clone())
            .expect("Could not add C to PlexBuilder.")
            .add_spatial_constraint(constraint_b_from_a)
            .expect("Could not add relation B from A to PlexBuilder.")
            .add_spatial_constraint(constraint_b_from_c)
            .expect("Could not add relation B from C to PlexBuilder.")
            .add_spatial_constraint(constraint_a_from_c.clone())
            .expect("Could not add relation A from C to PlexBuilder.");

        let plex = builder.build();

        // We attempt to get C from A. There should be two paths to it:
        //
        // 1. C <- A
        // 2. C <- B <- A
        //
        // `constraint_a_from_c` has very large covariance (orders of magnitude higher), so we
        // should expect that the second path is the one we get, and that the covariance we get
        // back is not equal to the covariance in `constraint_a_from_c`.
        //
        let constraint_from_plex = plex
            .get_spatial_constraint(camera_c.uuid(), camera_a.uuid())
            .expect("No spatial constraint found for C from A!");

        assert_eq!(constraint_from_plex.from, *camera_a.uuid());
        assert_eq!(constraint_from_plex.to, *camera_c.uuid());

        assert_relative_ne!(
            constraint_from_plex.covariance.to_se3(),
            constraint_a_from_c.covariance.to_se3(),
            epsilon = EPSILON,
        );
    }

    #[test]
    fn getting_spatial_constraint_produces_correct_extrinsic_and_cov() {
        let root_uuid = Uuid::new_v4();

        let camera_a = Component::Camera(Camera::new(
            root_uuid,
            root_uuid,
            String::from("A"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let camera_b = Component::Camera(Camera::new(
            Uuid::new_v4(),
            root_uuid,
            String::from("B"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let camera_c = Component::Camera(Camera::new(
            Uuid::new_v4(),
            root_uuid,
            String::from("C"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let b_from_a = Extrinsics::from(Isometry3::from_parts(
            Translation3::new(1212.5, 300.0, 233.3),
            UnitQuaternion::from_scaled_axis(Vector3::y() * 0.33 * std::f64::consts::PI),
        ));
        let constraint_b_from_a = SpatialConstraint {
            extrinsics: b_from_a,
            covariance: ExtrinsicCovariance::from(SMatrix::zeros()),
            from: *camera_a.uuid(),
            to: *camera_b.uuid(),
        };

        let c_from_b = Extrinsics::from(Isometry3::from_parts(
            Translation3::new(-230.5, 39.0, -0.3),
            UnitQuaternion::from_scaled_axis(Vector3::x() * -0.44 * std::f64::consts::PI),
        ));
        let constraint_c_from_b = SpatialConstraint {
            extrinsics: c_from_b,
            covariance: ExtrinsicCovariance::from(SMatrix::zeros()),
            from: *camera_b.uuid(),
            to: *camera_c.uuid(),
        };

        let mut builder = PlexBuilder::new();
        builder
            .add_component(camera_a.clone())
            .expect("Could not add A to PlexBuilder.")
            .add_component(camera_b)
            .expect("Could not add B to PlexBuilder.")
            .add_component(camera_c.clone())
            .expect("Could not add C to PlexBuilder.")
            .add_spatial_constraint(constraint_b_from_a.clone())
            .expect("Could not add relation B from A to PlexBuilder.")
            .add_spatial_constraint(constraint_c_from_b.clone())
            .expect("Could not add relation C from B to PlexBuilder.");

        let plex = builder.build();

        let constraint_from_plex = plex
            .get_spatial_constraint(camera_c.uuid(), camera_a.uuid())
            .expect("No spatial constraint found for C from A");

        assert_eq!(constraint_from_plex.from, *camera_a.uuid());
        assert_eq!(constraint_from_plex.to, *camera_c.uuid());

        let c_from_a = constraint_from_plex.extrinsics;

        let c_from_b_from_a = constraint_c_from_b
            .extrinsics
            .compose(&constraint_b_from_a.extrinsics);

        assert_relative_eq!(
            c_from_a.to_se3(),
            c_from_b_from_a.to_se3(),
            epsilon = EPSILON
        );

        let inverse_constraint_from_plex = plex
            .get_spatial_constraint(camera_a.uuid(), camera_c.uuid())
            .expect("No spatial constraint found for A from C");

        assert_eq!(inverse_constraint_from_plex.from, *camera_c.uuid());
        assert_eq!(inverse_constraint_from_plex.to, *camera_a.uuid());

        let a_from_c = inverse_constraint_from_plex.extrinsics;

        let a_from_b_from_c = constraint_b_from_a
            .extrinsics
            .inverse()
            .compose(&constraint_c_from_b.extrinsics.inverse());

        assert_relative_eq!(
            a_from_c.to_se3(),
            a_from_b_from_c.to_se3(),
            epsilon = EPSILON
        );
    }

    #[test]
    fn find_component_with_name() {
        let root_uuid = Uuid::new_v4();

        let camera_a = Component::Camera(Camera::new(
            root_uuid,
            root_uuid,
            String::from("A"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let uuid_b = Uuid::new_v4();
        let camera_b = Component::Camera(Camera::new(
            uuid_b,
            root_uuid,
            String::from("B"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let uuid_c = Uuid::new_v4();
        let camera_c = Component::Camera(Camera::new(
            uuid_c,
            root_uuid,
            String::from("C"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let mut builder = PlexBuilder::new();

        builder
            .add_component(camera_a.clone())
            .unwrap()
            .add_component(camera_b.clone())
            .unwrap()
            .add_component(camera_c)
            .unwrap();

        let plex = builder.build();

        let cam_a_from_plex = plex.find_component(|c| c.name() == "A");

        assert!(cam_a_from_plex.is_some());
        assert_eq!(&camera_a, cam_a_from_plex.unwrap());

        let cam_b_from_plex = plex.find_component(|c| *c.uuid() == uuid_b);

        assert!(cam_b_from_plex.is_some());
        assert_eq!(&camera_b, cam_b_from_plex.unwrap());

        let does_not_exist = plex.find_component(|c| *c.uuid() == Uuid::new_v4());

        assert!(does_not_exist.is_none());
    }

    #[test]
    fn empty_plex_ser_de_json_is_reflective() {
        let builder = PlexBuilder::new();
        let plex = builder.build();

        assert_eq!(plex.components().len(), 0);
        assert_eq!(plex.relations().len(), 0);

        let json_string = serde_json::to_string(&plex).expect("Could not serialize plex to JSON");

        let plex_from_json = serde_json::from_str::<Plex>(&json_string)
            .expect("Could not deserialize plex from JSON.");

        assert_eq!(plex.uuid(), plex_from_json.uuid());
        assert_eq!(
            plex.creation_timestamp(),
            plex_from_json.creation_timestamp()
        );

        assert_eq!(plex.components().len(), plex_from_json.components().len());
        assert_eq!(plex.relations().len(), plex_from_json.relations().len());
    }

    #[test]
    fn nonempty_plex_ser_de_json_is_reflective() {
        let root_uuid = Uuid::new_v4();

        let camera_a = Component::Camera(Camera::new(
            root_uuid,
            root_uuid,
            String::from("A"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let camera_b = Component::Camera(Camera::new(
            Uuid::new_v4(),
            root_uuid,
            String::from("B"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let camera_c = Component::Camera(Camera::new(
            Uuid::new_v4(),
            root_uuid,
            String::from("C"),
            mock_intrinsics(),
            DMatrix::zeros(3, 3),
            1.0,
        ));

        let anchor_a = Component::Anchor(Anchor::new(
            Uuid::new_v4(),
            root_uuid,
            String::from("AnchorA"),
        ));

        let constraint_b_from_a = SpatialConstraint {
            extrinsics: Extrinsics::from(Isometry3::from_parts(
                Translation3::new(1212.5, 300.0, 233.3),
                UnitQuaternion::from_scaled_axis(Vector3::y() * 0.33 * std::f64::consts::PI),
            )),
            covariance: ExtrinsicCovariance::from(SMatrix::zeros()),
            from: *camera_a.uuid(),
            to: *camera_b.uuid(),
        };

        let constraint_c_from_b = SpatialConstraint {
            extrinsics: Extrinsics::from(Isometry3::from_parts(
                Translation3::new(-230.5, 39.0, -0.3),
                UnitQuaternion::from_scaled_axis(Vector3::x() * -0.44 * std::f64::consts::PI),
            )),
            covariance: ExtrinsicCovariance::from(SMatrix::zeros()),
            from: *camera_b.uuid(),
            to: *camera_c.uuid(),
        };

        let constraint_a_from_c = TemporalConstraint {
            synchronization: Synchronization {
                skew: 0,
                offset: 1234,
            },
            resolution: 200,
            from: *camera_c.uuid(),
            to: *camera_a.uuid(),
        };

        let constraint_anchor_from_a = SpatialConstraint {
            extrinsics: Extrinsics::from(Isometry3::from_parts(
                Translation3::new(-230.5, 39.0, -0.3),
                UnitQuaternion::from_scaled_axis(Vector3::x() * -0.44 * std::f64::consts::PI),
            )),
            covariance: ExtrinsicCovariance::from(SMatrix::zeros()),
            from: *camera_a.uuid(),
            to: *anchor_a.uuid(),
        };        

        let mut builder = PlexBuilder::new();

        builder
            .add_component(camera_a)
            .expect("Could not add camera A component")
            .add_component(camera_b)
            .expect("Could not add caemra B component")
            .add_component(camera_c)
            .expect("Could not add camera C component")
            .add_component(anchor_a)
            .expect("Could not add anchor A component")
            .add_spatial_constraint(constraint_b_from_a)
            .expect("Could not add B from A spatial constraint")
            .add_spatial_constraint(constraint_c_from_b)
            .expect("Could not add C from B spatial constraint")
            .add_spatial_constraint(constraint_anchor_from_a)
            .expect("Could not add Anchor A from Camera A spatial constraint")
            .add_temporal_constraint(constraint_a_from_c)
            .expect("Could not add A from C temporal constraint");

        let plex = builder.build();

        let json_string = serde_json::to_string(&plex).expect("Could not serialize plex to JSON.");

        let plex_from_json = serde_json::from_str::<Plex>(&json_string)
            .expect("Could not deserialize plex from JSON.");

        assert!(plex.is_same(&plex_from_json));
    }
}

//#endregion
