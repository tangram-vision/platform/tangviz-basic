// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------

//! Provides `Relation`, the different temporal and spatial constraints possible between components.
//!
//! Components are connected through Space, through Time, or through a combination of the two
//! (conveniently called `SpaceAndTime`). These connections and their variations comprehensively
//! describe the connections that matter in a Plex.

use crate::{
    base::extrinsics::{ExtrinsicCovariance, Extrinsics},
    prelude::Sameness,
};
use serde::{Deserialize, Serialize};
use tangviz_basic_macros::Sameness;
use uuid::Uuid;

/// One second in units of nanoseconds.
const ONE_SECOND_IN_NANOS: i64 = 1_000_000_000;

/// A type describing how to relate timestamps between two different clocks, C1 and C2.
///
/// Uses the terminology from [this
/// paper](https://www.iol.unh.edu/sites/default/files/knowledgebase/1588/clock_synchronization_terminology.pdf).
/// This effectively is a type describing the following relationship between two clocks:
///
/// ```text
/// time_c2 = (1_000_000_000 + skew) * time_c1 + offset
/// ```
///
/// where the relation is a C2 &larr; C1 (C2 from C1) temporal constraint. Note that the above
/// assumes every unit is in integer nanoseconds.
///
/// This tells us how to relate two timestamps to be in the same temporal frame-of-reference.
///
/// Synchronization behaves according to the following model:
///
///   y = (1 + a) x + b
///
///   - y: time_to       -- unit: ns
///   - a: skew          -- unit: unitless
///   - x: time_from     -- unit: ns
///   - b: offset        -- unit: ns
///
/// Due to the use of integer mathematics, the application of this formula in code is not
/// intuitive.  We don't use floating point numbers in an effort to preserve precision. Therefore,
/// we convert everything into integer space.
///
/// Since we're dealing in nanoseconds, we have chosen a factor of 1e9 to perform this
/// transformation, because it preserves integer operations to the nanosecond. Anything less than a
/// nanosecond will be truncated.
///
/// Applying this transformation to our synchronization formula produces the following equation:
///
/// ```text
/// y = 1e9 [(1 + a) x + b]
///     -------------------
///            1e9
/// ```
///
/// Which simplifies (a little) into:
///
/// ```text
/// y = (1e9 + 1e9 * a) x + b
///     -----------------
///           1e9
/// ```
///
/// Given this, you'll see a few non-intuitive considerations in our code:
///
/// - The `skew` term is actually stored as `1e9 * a` in our struct for ease of compute.
/// - We must add and divide by a factor of 1e9 in order to maintain correct units during
/// composition.
///
/// It's all for the precision.
#[derive(Clone, Debug, Deserialize, Serialize, PartialEq, Sameness)]
pub struct Synchronization {
    /// The epoch offset between two clocks.
    ///
    /// In units of integer nanoseconds.
    pub offset: i64,

    /// The scale offset between two clocks.
    ///
    /// See the [`Synchronization`] type description for how we represent this unitless factor.
    pub skew: i64,
}

impl Synchronization {
    /// Get the inverse synchronization relationship.
    ///
    /// This is equivalent to swapping C1 and C2 (i.e. `from` and `to`).
    pub(crate) fn inverse(&self) -> Self {
        let scale = ONE_SECOND_IN_NANOS + self.skew;

        let offset = -(self.offset * ONE_SECOND_IN_NANOS) / scale;
        let skew = (ONE_SECOND_IN_NANOS.pow(2) / scale) - ONE_SECOND_IN_NANOS;

        Self { offset, skew }
    }

    /// Compose two synchronizations together, producing a new synchronization.
    ///
    /// This is equivalent to having both the C3 &larr; C2 and C2 &larr; C1 synchronizations, and
    /// thereafter deriving C3 &larr; C1. If we think of this algebraically, then we might get:
    ///
    /// ```text
    /// time_c3 = (1_000_000_000 + skew_c3_c2) * time_c2 + offset_c3_c2
    /// time_c2 = (1_000_000_000 + skew_c2_c1) * time_c1 + offset_c2_c1
    /// ```
    ///
    /// By then substituting the second equation into the first, we get:
    ///
    /// ```text
    /// time_c3 = (1_000_000_000 + skew_c3_c2)
    ///     * ((1_000_000_000 + skew_c2_c1) * time_c1 + offset_c2_c1)
    ///     + offset_c3_c2
    /// ```
    ///
    /// From here, the combined skews and offsets are computed.
    ///
    /// # Examples
    ///
    /// This is formulated such that you may call it as follows:
    ///
    /// ```ignore
    /// # use tangviz_basic::plex::Synchronization;
    /// # fn composition(c3_from_c2: Synchronization, c2_from_c1: Synchronization) {
    /// let c3_from_c1 = c3_from_c2.compose(c2_from_c1);
    /// # }
    /// ```
    pub(crate) fn compose(&self, rhs: &Self) -> Self {
        let offset = ((ONE_SECOND_IN_NANOS + self.skew) * rhs.offset) + self.offset;

        let sab = (self.skew * rhs.skew) / ONE_SECOND_IN_NANOS;
        let skew = rhs.skew + self.skew + sab;

        Self { offset, skew }
    }

    /// Applies the synchronization factors to a timestamp in nanoseconds.
    pub fn apply(&self, timestamp_nanos: i64) -> i64 {
        (ONE_SECOND_IN_NANOS + self.skew) * timestamp_nanos / ONE_SECOND_IN_NANOS + self.offset
    }
}

/// Type describing a temporal (synchronization) constraint between two components.
///
/// `from` and `to` serve to indicate the direction that any synchronization should be applied
/// between the two components.
///
/// If you're wondering how to apply the synchronization strategy, consider:
///
/// ```text
/// clock_to = fn(strategy, clock_from);
/// ```
///
/// Which is the direction that this constraint should be applied in order to correctly synchronize
/// the `to` and `from` component clocks.
#[derive(Clone, Debug, Deserialize, Serialize, PartialEq, Sameness)]
pub struct TemporalConstraint {
    /// The strategy to achieve known synchronization between these two components in the plex.
    pub synchronization: Synchronization,

    /// The resolution to which synchronization should be applied.
    ///
    /// Can also colloquially describe the confidence - a smaller resolution is a higher confidence
    /// check during synchronization.
    ///
    /// # Examples
    ///
    /// If you have a camera at 30FPS, you would probably describe the resolution as 16ms, or
    /// 16_000_000 ns. Resolution is the magnitude of the half-interval span of time that you would
    /// expect to match observations. However, this would more or less approximate "nearest
    /// neighbour" synchronization where you just pick the closest frame in your resolution. This
    /// is because you would be setting your resolution according to your frame-rate, and not
    /// according to any real knowledge of how close the clocks actually are.
    ///
    /// If you're measuring synchronization (as opposed to just querying it or making it up), you
    /// can probably determine roughly what the resolution is independent of the frame-rate, which
    /// is strictly better in most cases. This gives you an idea of how different two clocks
    /// actually are, and should provide more confidence in the final matching of observations.
    ///
    /// NOTE: In most cases you should probably expect that resolution doesn't get worse than the
    /// half interval related to the frame-rate of your device. In such cases, you probably have a
    /// broken clock.
    pub resolution: u64,

    /// The UUID of the component that the synchronization strategy must be applied to.
    pub from: Uuid,

    /// The UUID of the component whose clock we synchronize into by applying our synchronization
    /// strategy (to the `from` component).
    pub to: Uuid,
}

impl TemporalConstraint {
    /// Produces the inverse temporal strategy (i.e. swapping from and to).
    pub fn inverse(&self) -> Self {
        Self {
            synchronization: self.synchronization.inverse(),
            resolution: self.resolution,
            from: self.to,
            to: self.from,
        }
    }

    /// Composes two temporal constraints together iff `self.from == rhs.to`.
    ///
    /// # Examples
    ///
    /// ```
    /// # use tangviz_basic::plex::TemporalConstraint;
    /// # fn composition(c3_from_c2: TemporalConstraint, c2_from_c1: TemporalConstraint) {
    /// let c3_from_c1 = c3_from_c2.compose(&c2_from_c1);
    /// # }
    /// ```
    pub fn compose(&self, rhs: &Self) -> Option<Self> {
        if self.from == rhs.to {
            Some(Self {
                synchronization: self.synchronization.compose(&rhs.synchronization),
                resolution: self.resolution.max(rhs.resolution),
                from: rhs.from,
                to: self.to,
            })
        } else {
            None
        }
    }
}

/// Type describing a spatial (extrinsic) constraint between two components.
///
/// `from` and `to` serve to indicate the coordinate reference of the extrinsics.
///
/// Extrinsics are recorded in the frame of the "`to` from `from`" frame. Example: if `to` relates
/// to a depth camera component, and `from` relates to a color camera component, then the
/// extrinsics are "depth from color" extrinsics. See [the
/// documentation](https://www.notion.so/tangramvision/Plexes-5dedf69dd9da40978f428ea544f6634c) for
/// more.
#[derive(Clone, Debug, Deserialize, Serialize, Sameness)]
pub struct SpatialConstraint {
    /// Extrinsics between components.
    pub extrinsics: Extrinsics,

    /// Covariance matrix of the extrinsics.
    ///
    /// Matrix shape and ordering will depend on the representation. See the relevant `to_XXX`
    /// functions associated with this type for more information.
    pub covariance: ExtrinsicCovariance,

    /// The UUID of the component that extrinsics must be applied to.
    pub from: Uuid,

    /// The UUID of the component whose coordinate frame we transform into when applying the
    /// extrinsics (to the coordinate frame of the `from` component).
    pub to: Uuid,
}

impl SpatialConstraint {
    /// Produces the inverse spatial constraint (i.e. swapping from and to).
    pub fn inverse(&self) -> Self {
        Self {
            extrinsics: self.extrinsics.inverse(),
            covariance: self.covariance.clone(),
            from: self.to,
            to: self.from,
        }
    }
}

/// Relation is an "edge" in our plex graph, and describes how two sensors
/// relate in time and space.
#[derive(Clone, Debug, Sameness)]
pub enum Relation {
    /// The relation in the plex comprises _only_ a spatial constraint.
    Space(SpatialConstraint),

    /// The relation in the plex comprises _only_ a temporal constraint.
    Time(TemporalConstraint),

    /// The relation in the plex comprises a spatial and temporal constraint.
    SpaceAndTime {
        /// The spatial constraint.
        space: SpatialConstraint,

        /// The temporal constraint.
        time: TemporalConstraint,
    },
}

/// Module containing proptest-specific "arbitrary" functions, which provide arbitrary / randomized
/// data for property-based testing.
#[cfg(any(test, feature = "proptest-support"))]
pub mod arbitrary {
    use super::*;
    use proptest::prelude::*;

    impl Arbitrary for SpatialConstraint {
        type Strategy = BoxedStrategy<Self>;
        type Parameters = ();

        fn arbitrary_with(_args: Self::Parameters) -> Self::Strategy {
            any::<(Extrinsics, ExtrinsicCovariance)>()
                .prop_map(|(extrinsics, covariance)| Self {
                    extrinsics,
                    covariance,
                    from: Uuid::new_v4(),
                    to: Uuid::new_v4(),
                })
                .boxed()
        }
    }

    impl Arbitrary for Synchronization {
        type Strategy = BoxedStrategy<Self>;
        type Parameters = ();

        fn arbitrary_with(_args: Self::Parameters) -> Self::Strategy {
            any::<(i64, i64)>()
                .prop_map(|(offset, skew)| Self { offset, skew })
                .boxed()
        }
    }

    impl Arbitrary for TemporalConstraint {
        type Strategy = BoxedStrategy<Self>;
        type Parameters = ();

        fn arbitrary_with(_args: Self::Parameters) -> Self::Strategy {
            any::<(Synchronization, u64)>()
                .prop_map(|(synchronization, resolution)| Self {
                    synchronization,
                    resolution,
                    from: Uuid::new_v4(),
                    to: Uuid::new_v4(),
                })
                .boxed()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::prelude::*;
    use proptest::prelude::*;

    proptest! {
        #[test]
        fn sync_inverse_does_integer_division_correctly(
            // Any value is fine except for a skew equal to 1 second.
            //
            // Technically anything approaching this is also wrong, but having a combined skew
            // (1+s) that equates to zero would mean we have an infinite number of ticks in one
            // clock relative to another. This is a bit preposterous conceptually, and should not
            // be possible. We discount it here because such a case would cause a division by zero.
            //
            // In actual code this might be seen as a huge problem -- what if someone inputs a
            // temporal constraint with a skew of -1e9? Well, in such a scenario, we do have to
            // worry about it from a singularity perspective, but the relationship being described
            // would be nonsensical, so ther's little that can be done to avoid it. We could
            // mandate that these values all be positive, but negative skews are valid (e.g. if you
            // have (1+s) == 0.99998 or something).
            skew in (-100000000i32..100000000).prop_filter(
                "Total skew (1 + v) of zero is nonsensical.",
                |&v| v != -1_000_000_000
            ),
            offset in any::<i32>()
        ) {
            // It may seem strange that we use i32 above, but then i64 is the representation we use
            // for nanoseconds internally. The reason is that we expect that generally our
            // constraints won't support hundreds of years of difference between components (I
            // mean, why are you using plexes and nanoseconds to sync something that is on the
            // scale of years?).
            //
            // Because of this assumption, we are using i32 from proptest merely to make sure that
            // our sync values fit within a `f64`, since that is not guaranteed for any arbitrary
            // `i64`.
            //
            // Of course, this means that our system won't handle skews and offsets that approach
            // `i64::MAX`, but that was going to happen anyways. If we do want to support numbers
            // in that range, we probably need to represent everything as `i128`, which just kicks
            // the can farther down the road, since we have the same problem there. Of course,
            // proper BigInts could solve this, but Rust isn't a language where we have first class
            // support for BigInts and I'm not convinced we need them when discussion clock
            // differences (absolute time would be different, but we aren't doing that here, and
            // that's what the chrono crate is for).
            //
            // Anyways, the test doesn't cover any possible value but should be valid for most
            // values. The math should be correct here, but we may not be able to guarantee that at
            // the limits of our integer range.

            let sync = Synchronization { skew: skew.into(), offset: offset.into() };

            // Conversion to f64 is perfect for i32 (0 ULP). Division is 1/2 ULP
            let skew_as_floating = f64::from(skew) / 1e9;
            let skew_as_floating_as_i64 = unsafe {
                // Incurs 1/2 ULP for a total of 1 ULP
                (skew_as_floating * 1e9).round().to_int_unchecked::<i64>()
            };

            // We have 1 ULP error total from down-then-upscaling. 1 ULP at i32::MAX (as f64) is
            // 2.38e-7. This means it's that nearly all values of `skew` will roundtrip perfectly.
            // If a number's decimal portion is very close to 0.5 and the error happens to push it
            // above or below then we could be off by at most 1. In practice, all values (yes, I
            // tested all of them) of i32 will undergo this conversion perfectly, but that's not
            // necessarily guarenteed. It's a consequence of the structure of floats.
            prop_assert!((sync.skew - skew_as_floating_as_i64).abs() <= 1);

            let offset_as_floating = f64::from(offset) / 1e9;
            let offset_as_floating_as_i64 = unsafe {
                (offset_as_floating * 1e9).round().to_int_unchecked::<i64>()
            };

            // Same as with skew
            prop_assert!((sync.offset - offset_as_floating_as_i64).abs() <= 1);

            let inv = sync.inverse();

            let inv_skew_as_floating = (1.0 / (1.0 + skew_as_floating)) - 1.0;
            let inv_skew_as_floating_as_i64 = unsafe {
                (inv_skew_as_floating * 1e9)
                    .round()
                    .to_int_unchecked::<i64>()
            };
            prop_assert!((inv.skew - inv_skew_as_floating_as_i64).abs() <= 2);

            let inv_offset_as_floating = -offset_as_floating / (1.0 + skew_as_floating);
            let inv_offset_as_floating_as_i64 = unsafe {
                (inv_offset_as_floating * 1e9)
                    .round()
                    .to_int_unchecked::<i64>()
            };

            prop_assert!((inv.offset - inv_offset_as_floating_as_i64).abs() <= 2);
        }

        #[test]
        fn spatial_constraint_ser_de_is_reflective(constraint in any::<SpatialConstraint>()) {
            let serialized = serde_json::to_string(&constraint).unwrap();
            let deserialized = serde_json::from_str(&serialized).unwrap();

            prop_assert!(constraint.is_same(&deserialized));
        }

        #[test]
        fn temporal_constraint_ser_de_is_reflective(constraint in any::<TemporalConstraint>()) {
            let serialized = serde_json::to_string(&constraint).unwrap();
            let deserialized = serde_json::from_str(&serialized).unwrap();

            prop_assert!(constraint.is_same(&deserialized));
        }
    }
}
