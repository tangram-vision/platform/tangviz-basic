# Examples

Look at the source code of the examples to find helpful comments and pointers about how to get the
most out of the Tangram Vision SDK. Run any of the examples here using `cargo`:

```bash
$ cargo run --example <demo name>
```

## SDK Basics

- [build_your_own_plex](build_your_own_plex.rs): Demonstrates how to use the Plex APIs in the SDK to
  create a Plex to represent your multi-sensor system.

## Calibration

- [gen_objspace_markers](gen_objspace_markers.rs): This example shows how to generate an
  appropriate object-space definition from a grid pattern of fiducial markers, i.e. AprilTags or
  ArUco. Given the nature of such grids, it is necessary to take into account the layout of the
  grid, in addition to metric spacing. This example does this via a passed-in image file of the
  marker grid.

  See the `fixtures` folder in the top level of this crate to find examples of object space files.
