// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! Intel RealSense 415 plex creation. Reference:
//!
//! https://www.intel.com/content/dam/support/us/en/documents/emerging-technologies/intel-realsense-technology/Intel-RealSense-D400-Series-Datasheet.pdf
//! https://www.gophotonics.com/products/cmos-image-sensors/omnivision-technologies/21-118-ov2740

use anyhow::Result;
use conv::prelude::*;
use tangviz_basic::{
    base::{
        AffinityModel, CameraIntrinsics, DistortionModel, ExtrinsicCovariance, Extrinsics,
        ProjectionModel,
    },
    nalgebra::{DMatrix, DVector, Isometry3, Matrix6, Translation3, UnitQuaternion, Vector6},
    plex::{
        Camera, Component, PlexBuilder, SpatialConstraint, Synchronization, TemporalConstraint,
    },
};
use uuid::Uuid;

/// Output a Camera component based on known input parameters. This is meant to be a "best guess" at
/// the correct intrinsics of the Camera. Results should be calibrated before use.
fn create_default_bc_camera(
    width: usize,
    height: usize,
    pixel_pitch: f64,
    diag_fov_deg: f64,
    name: String,
    camera_uuid: Uuid,
    root_uuid: Uuid,
) -> Result<Component> {
    let camera_intrinsics = {
        let cx = (width / 2).value_as::<f64>()?;
        let cy = (height / 2).value_as::<f64>()?;

        // This is approximately field of view of the camera from corner-to-corner
        // of the camera, in radians.x
        let field_of_view = diag_fov_deg.to_radians();

        let half_diagonal_px = (cx * cx + cy * cy).sqrt();

        // This is a rough approximation of the focal length based purely on the
        // field-of-view of the lens.
        let f = half_diagonal_px / (field_of_view / 2.0).tan();

        let projection = ProjectionModel::Pinhole { f, cx, cy };
        let distortion = DistortionModel::BrownConrady {
            k1: 0.0,
            k2: 0.0,
            k3: 0.0,
            p1: 0.0,
            p2: 0.0,
        };
        let affinity = AffinityModel::NoAffinity;

        CameraIntrinsics {
            projection,
            distortion,
            affinity,
            width,
            height,
        }
    };

    let camera_covariance = DMatrix::<f64>::from_diagonal(&DVector::from_iterator(
        8,
        vec![
            500.0, // standard deviation of f
            100.0, // standard deviation of cx
            100.0, // standard deviation of cy
            1.0,   // standard deviation of k1
            1.0,   // standard deviation of k2
            1.0,   // standard deviation of k3
            1.0,   // standard deviation of p1
            1.0,   // standard deviation of p2
        ]
        .into_iter()
        .map(|x: f64| x.powi(2)), // Variance is std. deviation squared
    ));
    Ok(Component::Camera(Camera::new(
        camera_uuid,
        root_uuid,
        name,
        camera_intrinsics,
        camera_covariance,
        pixel_pitch,
    )))
}

fn main() -> Result<()> {
    let mut builder = PlexBuilder::new();

    // ~~~~~~~~~~~~
    // Left IR: OV2740
    // ~~~~~~~~~~~~
    let ir_width = 848_usize;
    let ir_height = 480_usize;
    let ir_diag_fov_deg = 77_f64;
    let ir_pixel_pitch = 1.4e-6; // 1.4 microns per pixel
    let left_ir_uuid = Uuid::new_v4();

    let left_ir_cam = create_default_bc_camera(
        ir_width,
        ir_height,
        ir_pixel_pitch,
        ir_diag_fov_deg,
        String::from("left_ir"),
        left_ir_uuid,
        left_ir_uuid, // root is itself
    )?;

    // ~~~~~~~~~~~~
    // Right IR: OV2740
    // ~~~~~~~~~~~~
    let right_ir_uuid = Uuid::new_v4();
    let right_ir_cam = create_default_bc_camera(
        ir_width,
        ir_height,
        ir_pixel_pitch,
        ir_diag_fov_deg,
        String::from("right_ir"),
        right_ir_uuid,
        left_ir_uuid, // root is left IR
    )?;

    // ~~~~~~~~~~~~
    // Color: OV2740!
    // ~~~~~~~~~~~~
    let color_width = 1280_usize;
    let color_height = 720_usize;
    let color_diag_fov_deg = 77_f64;
    let color_pixel_pitch = 1.4e-6; // 1.4 microns per pixel
    let color_uuid = Uuid::new_v4();

    let color_cam = create_default_bc_camera(
        color_width,
        color_height,
        color_pixel_pitch,
        color_diag_fov_deg,
        String::from("color"),
        color_uuid,
        left_ir_uuid, // root is left IR
    )?;

    // ~~~~~~~~~~~~
    // Spatial Constraints
    // ~~~~~~~~~~~~
    let std_devs = Extrinsics::from(Isometry3::from_parts(
        Translation3::new(0.01, 0.01, 0.01),
        UnitQuaternion::from_euler_angles(
            15_f64.to_radians(),
            15_f64.to_radians(),
            15_f64.to_radians(),
        ),
    ))
    .to_se3();
    let extrinsic_covariance = ExtrinsicCovariance::from(Matrix6::from_diagonal(
        &Vector6::from_iterator(std_devs.into_iter().map(|v| v.powi(2))),
    ));

    // Spatial constraint: Left IR from Right IR
    // Left IR is origin
    let ir_left_from_ir_right = Extrinsics::from(Isometry3::from_parts(
        Translation3::new(0.05, 0.0, 0.0),
        UnitQuaternion::identity(),
    ));
    let left_from_right_constraint = SpatialConstraint {
        extrinsics: ir_left_from_ir_right,
        covariance: extrinsic_covariance.clone(),
        from: right_ir_uuid,
        to: left_ir_uuid, // origin
    };

    let ir_left_from_color = Extrinsics::from(Isometry3::from_parts(
        Translation3::new(-0.015, 0.0, 0.0),
        UnitQuaternion::identity(),
    ));
    let left_from_color_constraint = SpatialConstraint {
        extrinsics: ir_left_from_color,
        covariance: extrinsic_covariance,
        from: color_uuid,
        to: left_ir_uuid, // origin
    };

    let time_left_from_right = TemporalConstraint {
        synchronization: Synchronization { offset: 0, skew: 0 },
        // resolution of 5ms (5e6 ns)
        // This means that any observation from the left IR may be synced to 
        // a right IR observation if they are within +/-5ms delta
        resolution: 5000000,
        from: right_ir_uuid,
        to: left_ir_uuid,
    };

    let time_left_from_color = TemporalConstraint {
        synchronization: Synchronization { offset: 0, skew: 0 },
        // resolution of 5ms (5e6 ns)
        // This means that any observation from the left IR may be synced to 
        // a right IR observation if they are within +/-5ms delta
        resolution: 5000000,
        from: color_uuid,
        to: left_ir_uuid,
    };

    // Put all of our pieces together
    builder.add_component(left_ir_cam)?;
    builder.add_component(right_ir_cam)?;
    builder.add_component(color_cam)?;
    builder.add_spatial_constraint(left_from_right_constraint)?;
    builder.add_spatial_constraint(left_from_color_constraint)?;
    builder.add_temporal_constraint(time_left_from_right)?;
    builder.add_temporal_constraint(time_left_from_color)?;

    let plex = builder.build();
    let plex_json = serde_json::to_string(&plex)?;

    std::fs::write("realsense_415_plex.json", plex_json).expect("Unable to write file");

    Ok(())
}
