// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! Sample program to demonstrate how to make your own Plex using the Tangram Vision SDK.
//!

use anyhow::Result;
use conv::prelude::*;
use tangviz_basic::{
    base::{
        AffinityModel, CameraIntrinsics, DistortionModel, ExtrinsicCovariance, Extrinsics,
        ProjectionModel,
    },
    nalgebra::{DMatrix, DVector, Isometry3, Matrix6, Translation3, UnitQuaternion, Vector6},
    plex::{Camera, Component, PlexBuilder, SpatialConstraint},
};
use uuid::Uuid;

/// Constants used in this example.
///
/// Feel free to change these to accommodate your own custom plex.
mod constants {
    /// The width (number of columns) of the image format of our cameras, in pixels.
    pub const WIDTH: usize = 1280;

    /// The height (number of rows) of the image format of our cameras, in pixels.
    pub const HEIGHT: usize = 720;

    /// The field of view of our zeroeth camera, in degrees.
    pub const CAMERA_0_FIELD_OF_VIEW: f64 = 120.0;

    /// The field of view of our first camera, in degrees.
    pub const CAMERA_1_FIELD_OF_VIEW: f64 = 70.0;

    /// The topic name for camera 0.
    pub const CAMERA_0_TOPIC_NAME: &str = "cam0/image_raw";

    /// The topic name for camera 1.
    pub const CAMERA_1_TOPIC_NAME: &str = "cam1/image_raw";

    /// The size of a pixel in units-per-pixel.
    ///
    /// While we predominantly keep the system in units of pixels, it isn't necessarily 100%
    /// correct to compare errors between multiple cameras pixel-to-pixel, as different cameras
    /// might have different pixel sizes. This is not used during calibration, but can be useful
    /// for interpreting and comparing results between two cameras. Usually will be in units of
    /// metres or millimetres.
    ///
    /// Example: if you had pixels that were 3μm, you might set this to 3e-6 (metres-per-pixel) or
    /// 3e-3 (millimetres-per-pixel). This field is meant to be purely semantic, so units are not
    /// restricted.
    ///
    /// **NOTE**: If you're unsure, set this to 1.0 (pixels-per-pixel).
    pub const PIXEL_PITCH: f64 = 1.0;

    /// Standard deviation of translations, in metres.
    pub const TRANSLATION_STD_DEV: f64 = 15.0;

    /// Standard deviation of rotations, in degrees.
    pub const ROTATION_STD_DEV: f64 = 15.0;
}

fn main() -> Result<()> {
    let mut builder = PlexBuilder::new();

    // ~~~~~~~~~~~~
    // Camera 0 Intrinsics & Covariance
    // ~~~~~~~~~~~~

    let camera0_intrinsics = {
        // cx and cy are approximately in the centre of the image.
        let cx = (constants::WIDTH / 2).value_as::<f64>()?;
        let cy = (constants::HEIGHT / 2).value_as::<f64>()?;

        // This is approximately field of view of the camera from corner-to-corner
        // of the camera, in radians.
        let field_of_view = (constants::CAMERA_0_FIELD_OF_VIEW).to_radians();

        let radius = (cx * cx + cy * cy).sqrt();

        // This is a rough approximation of the focal length based purely on the
        // field-of-view of the lens.
        let f = radius / (field_of_view / 2.0).tan();

        let distortion = DistortionModel::KannalaBrandt {
            k1: 0.0,
            k2: 0.0,
            k3: 0.0,
            k4: 0.0,
        };

        let affinity = AffinityModel::Scale { a1: 0.0 };

        CameraIntrinsics {
            projection: ProjectionModel::Pinhole { f, cx, cy },
            distortion,
            affinity,
            width: constants::WIDTH,
            height: constants::HEIGHT,
        }
    };

    let camera0_covariance = DMatrix::<f64>::from_diagonal(&DVector::from_iterator(
        8,
        vec![
            100.0, // standard deviation of f
            25.0,  // standard deviation of cx
            25.0,  // standard deviation of cy
            1.0,   // standard deviation of k1
            1.0,   // standard deviation of k2
            1.0,   // standard deviation of k3
            1.0,   // standard deviation of k4
            0.5,   // standard deviation of a1
        ]
        .into_iter()
        .map(|x: f64| x.powi(2)), // Variance is std. deviation squared
    ));

    let camera0_uuid = Uuid::new_v4();

    let camera0 = Component::Camera(Camera::new(
        camera0_uuid,
        camera0_uuid,
        constants::CAMERA_0_TOPIC_NAME.to_string(),
        camera0_intrinsics,
        camera0_covariance,
        constants::PIXEL_PITCH,
    ));

    // ~~~~~~~~~~~~
    // Camera 1 Intrinsics & Covariance
    // ~~~~~~~~~~~~

    let camera1_intrinsics = {
        // cx and cy are approximately in the centre of the image.
        let cx = (constants::WIDTH / 2).value_as::<f64>()?;
        let cy = (constants::HEIGHT / 2).value_as::<f64>()?;

        // This is approximately field of view of the camera from corner-to-corner
        // of the camera, in radians.
        let field_of_view = (constants::CAMERA_1_FIELD_OF_VIEW).to_radians();

        let radius = (cx * cx + cy * cy).sqrt();

        // This is a rough approximation of the focal length based purely on the
        // field-of-view of the lens.
        let f = radius / (field_of_view / 2.0).tan();

        let distortion = DistortionModel::BrownConrady {
            k1: 0.0,
            k2: 0.0,
            k3: 0.0,
            p1: 0.0,
            p2: 0.0,
        };

        let affinity = AffinityModel::Scale { a1: 0.0 };

        CameraIntrinsics {
            projection: ProjectionModel::Pinhole { f, cx, cy },
            distortion,
            affinity,
            width: constants::WIDTH,
            height: constants::HEIGHT,
        }
    };

    let camera1_covariance = DMatrix::<f64>::from_diagonal(&DVector::from_iterator(
        9,
        vec![
            100.0, // standard deviation of f
            25.0,  // standard deviation of cx
            25.0,  // standard deviation of cy
            1.0,   // standard deviation of k1
            1.0,   // standard deviation of k2
            1.0,   // standard deviation of k3
            1.0,   // standard deviation of p1
            1.0,   // standard deviation of p2
            0.5,   // standard deviation of a1
        ]
        .into_iter()
        .map(|x: f64| x.powi(2)), // Variance is std. deviation squared
    ));

    let camera1_uuid = Uuid::new_v4();

    let camera1 = Component::Camera(Camera::new(
        camera1_uuid,
        camera0_uuid,
        constants::CAMERA_1_TOPIC_NAME.to_string(),
        camera1_intrinsics,
        camera1_covariance,
        constants::PIXEL_PITCH,
    ));

    builder.add_component(camera0)?.add_component(camera1)?;

    // ~~~~~~~~~~~~
    // Camera 0 <-> Camera 1 Spatial Constraint
    // ~~~~~~~~~~~~
    //
    // Here we are defining the camera-1-from-camera-0 extrinsic.

    let cam1_from_cam0 = Extrinsics::from(Isometry3::from_parts(
        Translation3::new(0.2, 0.0, -0.2),
        UnitQuaternion::identity(),
    ));

    // Angular standard deviation in radians.
    let d_theta = (constants::ROTATION_STD_DEV).to_radians();

    let std_devs = Extrinsics::from(Isometry3::from_parts(
        // Assume all our translations are ±0.010m (±1cm)
        Translation3::new(
            constants::TRANSLATION_STD_DEV, // X
            constants::TRANSLATION_STD_DEV, // Y
            constants::TRANSLATION_STD_DEV, // Z
        ),
        // And all our rotations are good to ±15°
        UnitQuaternion::from_euler_angles(d_theta, d_theta, d_theta),
    ))
    .to_se3();

    let extrinsic_covariance = ExtrinsicCovariance::from(Matrix6::from_diagonal(
        &Vector6::from_iterator(std_devs.into_iter().map(|v| v.powi(2))),
    ));

    let spatial_constraint = SpatialConstraint {
        extrinsics: cam1_from_cam0,
        covariance: extrinsic_covariance,
        from: camera0_uuid,
        to: camera1_uuid,
    };

    builder.add_spatial_constraint(spatial_constraint)?;

    let _plex = builder.build();

    Ok(())
}
