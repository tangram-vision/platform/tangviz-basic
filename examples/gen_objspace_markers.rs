// Copyright (c) 2022 Tangram Robotics Inc. - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// ----------------------------
//! Example that generates an object space file from an image of markers
//!
//! Run `cargo run -- -h` to see all arguments and options.

use anyhow::Result;
use getopts::Options;
use opencv::{
    aruco::{DetectorParameters, Dictionary, PREDEFINED_DICTIONARY_NAME},
    core::{Ptr, Vector},
    prelude::*,
};
use serde_json::json;
use std::{collections::HashMap, env, path::PathBuf, str::FromStr};
use strum::IntoEnumIterator;
use tangviz_basic::base::object_space::{
    Descriptor, Detector, DetectorDescriptor, ObjectSpaceConfig, SupportedMarkerDictionary, Target,
};

/// Somewhere to hold the arguments that were passed to this example.
struct ProgramArgs {
    /// The path of the target image file we are interpreting as object space
    path: PathBuf,
    /// The length of the edge of a marker
    marker_length: f64,
    /// The dictionary used to generate the markers in the target image
    marker_dictionary: String,
}

/// OpenCV Detector objects
struct OpenCVDetector {
    /// The initialized dictionary of marker codes
    dict: Ptr<Dictionary>,
    /// The detection parameters used
    params: Ptr<DetectorParameters>,
}

/// Derive the detector configuration that we should use to parse the target image file
fn derive_opencv_detector(identifier: &str) -> Result<OpenCVDetector> {
    // Most of our detectors will just need general params. However, a few (mostly AprilTags) will
    // need modifiers.
    let mut params = DetectorParameters::create()?;

    // Set our corner refinement method.
    params.set_corner_refinement_method(opencv::aruco::CORNER_REFINE_SUBPIX);

    // If we are using Kalibr targets, make sure we have the right border bits set
    if let Ok(SupportedMarkerDictionary::ApriltagKalibr) =
        SupportedMarkerDictionary::from_str(identifier)
    {
        params.set_marker_border_bits(2);
    }

    // Create our mapping of dictionaries.
    let dictionary = match SupportedMarkerDictionary::from_str(identifier) {
        Ok(SupportedMarkerDictionary::Aruco4x4_50) => PREDEFINED_DICTIONARY_NAME::DICT_4X4_50,
        Ok(SupportedMarkerDictionary::Aruco4x4_100) => PREDEFINED_DICTIONARY_NAME::DICT_4X4_100,
        Ok(SupportedMarkerDictionary::Aruco4x4_250) => PREDEFINED_DICTIONARY_NAME::DICT_4X4_250,
        Ok(SupportedMarkerDictionary::Aruco4x4_1000) => PREDEFINED_DICTIONARY_NAME::DICT_4X4_1000,
        Ok(SupportedMarkerDictionary::Aruco5x5_50) => PREDEFINED_DICTIONARY_NAME::DICT_5X5_50,
        Ok(SupportedMarkerDictionary::Aruco5x5_100) => PREDEFINED_DICTIONARY_NAME::DICT_5X5_100,
        Ok(SupportedMarkerDictionary::Aruco5x5_250) => PREDEFINED_DICTIONARY_NAME::DICT_5X5_250,
        Ok(SupportedMarkerDictionary::Aruco5x5_1000) => PREDEFINED_DICTIONARY_NAME::DICT_5X5_1000,
        Ok(SupportedMarkerDictionary::Aruco6x6_50) => PREDEFINED_DICTIONARY_NAME::DICT_6X6_50,
        Ok(SupportedMarkerDictionary::Aruco6x6_100) => PREDEFINED_DICTIONARY_NAME::DICT_6X6_100,
        Ok(SupportedMarkerDictionary::Aruco6x6_250) => PREDEFINED_DICTIONARY_NAME::DICT_6X6_250,
        Ok(SupportedMarkerDictionary::Aruco6x6_1000) => PREDEFINED_DICTIONARY_NAME::DICT_6X6_1000,
        Ok(SupportedMarkerDictionary::Aruco7x7_50) => PREDEFINED_DICTIONARY_NAME::DICT_7X7_50,
        Ok(SupportedMarkerDictionary::Aruco7x7_100) => PREDEFINED_DICTIONARY_NAME::DICT_7X7_100,
        Ok(SupportedMarkerDictionary::Aruco7x7_250) => PREDEFINED_DICTIONARY_NAME::DICT_7X7_250,
        Ok(SupportedMarkerDictionary::Aruco7x7_1000) => PREDEFINED_DICTIONARY_NAME::DICT_7X7_1000,
        Ok(SupportedMarkerDictionary::ArucoOriginal) => {
            PREDEFINED_DICTIONARY_NAME::DICT_ARUCO_ORIGINAL
        }
        Ok(SupportedMarkerDictionary::Apriltag16h5) => {
            PREDEFINED_DICTIONARY_NAME::DICT_APRILTAG_16h5
        }
        // 5x5 bits, minimum hamming distance between any two codes = 9, 35 codes
        Ok(SupportedMarkerDictionary::Apriltag25h9) => {
            PREDEFINED_DICTIONARY_NAME::DICT_APRILTAG_25h9
        }
        // 6X6 bits, minimum hamming distance between any two codes = 10, 2320 codes
        Ok(SupportedMarkerDictionary::Apriltag36h10) => {
            PREDEFINED_DICTIONARY_NAME::DICT_APRILTAG_36h10
        }
        // 6x6 bits, minimum hamming distance between any two codes = 11, 587 codes
        Ok(SupportedMarkerDictionary::Apriltag36h11) => {
            PREDEFINED_DICTIONARY_NAME::DICT_APRILTAG_36h11
        }
        // 6x6 bits, minimum hamming distance between any two codes = 11, 587 codes. Border width of 2.
        // This is the main differentiator between Kalibr and other board types.
        Ok(SupportedMarkerDictionary::ApriltagKalibr) => {
            PREDEFINED_DICTIONARY_NAME::DICT_APRILTAG_36h11
        }
        Err(_) => {
            let dictionary_options = {
                SupportedMarkerDictionary::iter().fold(
                    String::from(
                        "\nERROR: The provided dictionary is not valid. Possible options: \n",
                    ),
                    |acc, option| acc + "- " + &option.to_string() + "\n",
                )
            };
            eprintln!("{}", dictionary_options);
            std::process::exit(1);
        }
    };

    let dict = opencv::aruco::get_predefined_dictionary(dictionary)?;

    Ok(OpenCVDetector { dict, params })
}

/// Parses all command line arguments for the provided image path
fn read_args() -> ProgramArgs {
    let args: Vec<String> = env::args().collect();
    let mut opts = Options::new();

    // Required arguments
    opts.reqopt(
        "t",
        "target",
        "[REQ] The location of the target image file.",
        "<path>",
    );
    opts.reqopt(
        "l",
        "marker_length",
        "[REQ] The length in meters of a single marker edge.",
        "<float>",
    );
    opts.optflag("h", "help", "[OPT] Print this help message");

    let dictionary_options = {
        SupportedMarkerDictionary::iter().fold(
            String::from(
                "[OPT] The marker dictionary from which this grid was derived. The program \
         will attempt to deduce the right grid type if an argument is not \
         supplied. Possible options: \n",
            ),
            |acc, option| acc + "- " + &option.to_string() + "\n",
        )
    };

    // Optional options
    opts.optopt("d", "marker_dictionary", &dictionary_options, "");

    let usage = opts.usage(&opts.short_usage(env!("CARGO_PKG_NAME")));
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(_) => {
            eprintln!("\nERROR: Missing required arguments.\n{}", usage);
            std::process::exit(1);
        }
    };

    // Option h: print out all available flags
    if matches.opt_present("h") {
        println!("\n{}", usage);
        std::process::exit(0);
    }

    let img_path = matches.opt_str("target").unwrap();
    let img_path_buf = match PathBuf::from_str(&img_path) {
        Ok(p) => p,
        Err(e) => {
            eprintln!("|-- Error finding image: {}", e);
            std::process::exit(1);
        }
    };
    let marker_length = f64::from_str(&matches.opt_str("marker_length").unwrap()).unwrap();

    let marker_dictionary = match matches.opt_str("marker_dictionary") {
        Some(f) => f,
        None => String::from(""),
    };

    ProgramArgs {
        path: img_path_buf,
        marker_length,
        marker_dictionary,
    }
}

fn main() -> Result<()> {
    // Get our argument from getopts
    let markers = read_args();

    // Parse our image
    let marker_image = image::open(markers.path)?.into_luma8();

    // Copy to cv::mat
    let marker_mat = {
        let mut m = opencv::core::Mat::new_rows_cols_with_default(
            marker_image.height() as i32,
            marker_image.width() as i32,
            opencv::core::CV_8U,
            opencv::core::Scalar::default(),
        )?;

        for (col, row, val) in marker_image.enumerate_pixels() {
            *m.at_2d_mut::<u8>(row as i32, col as i32).unwrap() = val[0];
        }

        m
    };

    // If the dictionary is provided to us, use that.
    //
    // If we were not provided a dictionary type, identify the type by cycling through and
    // determining what detector dictionary gets the most detections.
    //
    // This is not necessarily the right thing to do; there is a possiblity of misdetection, low
    // detection count, or a missing dictionary. However, for the purposes of this example it should
    // be sufficient.
    let possible_dicts = {
        if !markers.marker_dictionary.is_empty() {
            vec![markers.marker_dictionary.as_str()]
        } else {
            SupportedMarkerDictionary::iter()
                .map(|option| option.into())
                .collect()
        }
    };

    let (derived_marker_dict, (marker_corners, marker_ids)) = {
        let mut detections_from_dict = HashMap::new();
        for dict in possible_dicts {
            let detector = derive_opencv_detector(dict)?;
            let mut marker_corners = Vector::<opencv::types::VectorOfPoint2f>::new();
            let mut marker_ids = opencv::core::Vector::<i32>::new();
            let mut rejected_points = opencv::core::Vector::<opencv::types::VectorOfPoint2f>::new();
            opencv::aruco::detect_markers(
                &marker_mat,
                &detector.dict,
                &mut marker_corners,
                &mut marker_ids,
                &detector.params,
                &mut rejected_points,
                &opencv::core::no_array().unwrap(),
                &opencv::core::no_array().unwrap(),
            )?;
            detections_from_dict.insert(dict, (marker_corners.to_vec(), marker_ids.to_vec()));
        }

        // Find the dictionary with the most detections
        detections_from_dict
            .into_iter()
            .max_by(|(_, (_, m_id1)), (_, (_, m_id2))| m_id1.len().cmp(&m_id2.len()))
            .unwrap()
    };

    // Derive the pixel dimensionality of our grid. This will be a scaling factor on object space.
    let marker_length_in_pixels =
        (marker_corners[0].to_vec()[0] - marker_corners[0].to_vec()[1]).norm();
    let scale = markers.marker_length / marker_length_in_pixels;

    // Create our object space hierarchy
    let mut target_list_vec = Vec::<Target>::new();
    for (corners, id) in marker_corners.iter().zip(marker_ids.iter()) {
        for (corner_id, corner) in corners.iter().enumerate() {
            target_list_vec.push(Target {
                id: *id as usize * 4 + corner_id,
                coordinates: [corner.x as f64 * scale, corner.y as f64 * scale, 0.0],
                // This represents a standard deviation of 0.1 units in each metric
                // direction [x, y, z].
                variances: [0.01, 0.01, 0.01],
            });
        }
    }

    let object_space_config = ObjectSpaceConfig {
        camera: DetectorDescriptor {
            detector: Detector::Markers {
                marker_length: markers.marker_length,
                marker_dictionary: String::from(derived_marker_dict),
            },
            descriptor: Descriptor::TargetList {
                targets: target_list_vec,
            },
        },
    };

    // Write to stdout
    let json = json!(&object_space_config);
    println!("{}", json);

    Ok(())
}
